﻿/*
 * Created by SharpDevelop.
 * User: Sinbo
 * Date: 2010/6/23
 * Time: 16:50
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    /// <summary>
    /// Description of API.
    /// </summary>
    public class API
    {
        private static API _Instance;
        public static API Instance { get { if (_Instance == null) { _Instance = new API(); } return _Instance; } }
        private API() { }

        #region 留言信息
        /// 获取最新的信息
        /// </summary>
        /// <param name="object_id"></param>
        /// <returns></returns>
        public MessageVO[] message(int length)
        {
            MessageModel mesageModel = new MessageModel();
            return mesageModel.Select(0, length, string.Format("status='1'"));
        }

        public int count(string object_id)
        {
            MessageModel mesageModel = new MessageModel();
            string where = string.Format("object_id='{0}' and status='1'", object_id);
            return mesageModel.Count(where);
        }

        /// <summary>
        /// 获取置首留言
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public MessageVO[] rank(int length)
        {
            MessageModel mesageModel = new MessageModel();
            return mesageModel.Select(0, length, "rank='1'");
        }
        #endregion

        #region 获取文章
        /// <summary>
        /// 获取分类下文章
        /// </summary>
        /// <param name="category_id">分类编号</param>
        /// <param name="length">条数</param>
        /// <returns></returns>
        public ArticleVO[] category(string category_id, int length)
        {
			ArticleVO[] article= this.articleModel.Select(0, length, string.Format("categorys like '%,{0},%'", category_id), "`pubtime` desc");
			return article;
        }

        /// <summary>
        /// 获取标签下文章
        /// </summary>
        /// <param name="tag_id">标签编号</param>
        /// <param name="length">条数</param>
        /// <returns></returns>
        public ArticleVO[] tag(string tag_id, int length)
        {
            return this.articleModel.Select(0, length, string.Format("tags like '%,{0},%'", tag_id), "`pubtime` desc");
        }

        /// <summary>
        /// 根据标签和分类同时获取文章
        /// </summary>
        /// <param name="category_id"></param>
        /// <param name="tag_id"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public ArticleVO[] complex(string category_id, string tag_id, int length)
        {
            return this.articleModel.Select(0, length, string.Format("categorys like '%,{0},%' and tags like '%,{0},%'", category_id, tag_id), "`pubtime` desc");
        }
        #endregion

        #region 每周热点
        /// <summary>
        /// 每周热点
        /// </summary>
        /// <returns></returns>
        public ArticleVO[] week(int length)
        {
            int time = Jabinfo.Help.Date.ToDate(DateTime.Now.AddDays(-7));
            return this.articleModel.Select(0, length, string.Format("pubtime<{0}", time), "`reads` desc");
        }

        //分类下周热点
        public ArticleVO[] weekByCategory(string relate_id, int length)
        {
            int time = Jabinfo.Help.Date.ToDate(DateTime.Now.AddDays(-7));
            return this.articleModel.Select(0, length, string.Format("pubtime<{0} and categorys like '%,{1},%'", time, relate_id), "`reads` desc");
        }

        //分类下周热点
        public ArticleVO[] weekByTag(string relate_id, int length)
        {
            int time = Jabinfo.Help.Date.ToDate(DateTime.Now.AddDays(-7));
            return this.articleModel.Select(0, length, string.Format("pubtime<{0} and tags like '%,{1},%'", time, relate_id), "`reads` desc");
        }
        #endregion

        #region 获取一个页面
        /// <summary>
        /// 获取一组页面
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public PageVO[] page(string jtype)
        {
            PageModel pageModel = new PageModel();
            return pageModel.Select(jtype);
        }

        public PageVO pageVO(string page_id)
        {
            return new PageVO(page_id);
        }
        #endregion

        #region 获取友情链接
        public LinksVO[] link(string type, int length)
        {
            LinksModel linksModel = new LinksModel();
            return linksModel.Select(0, length, string.Format("category='{0}'", type));
        }

        public LinksVO[] link(int length)
        {
            LinksModel linksModel = new LinksModel();
            return linksModel.Select(0, length, string.Empty);
        }
        #endregion

        #region 获取文章内容
        public ArticleVO article(string article_id)
        {
            ArticleVO articleVO = new ArticleVO(article_id);
            articleVO.Summary = new Article_detailVO(article_id).Content;
            return articleVO;    
        }
        #endregion


        #region 分类下拉框
        private CategoryVO[] group(string parent_id)
        {
            CategoryModel categoryModel = new CategoryModel();
            return categoryModel.Select(parent_id);
        }
        public string Option(string parent_id, int i)
        {
            StringBuilder stringBuilder = new StringBuilder(1500);
            CategoryVO[] categorys = this.group(parent_id);
            if (categorys == null)
                return string.Empty;
            i = i + 1;
            foreach (CategoryVO category in categorys)
            {
                stringBuilder.AppendFormat("<option value=\"{0}\">", category.Category_id);
                for (int j = 1; j < i; j++)
                {
                    stringBuilder.Append("&nbsp;&nbsp;&nbsp;");
                }
                stringBuilder.AppendFormat("{0}.{1}</option>", i, category.Title);
                if (category.Childen == "1")
                {
                    stringBuilder.Append(Option(category.Category_id, i));
                }
            }
            return stringBuilder.ToString();
        }
        #endregion

        #region Model
        private ArticleModel _Model = null;
        private ArticleModel articleModel
        {
            get
            {
                if (_Model == null)
                    _Model = new ArticleModel();
                return _Model;
            }
        }
        #endregion
    }
}
