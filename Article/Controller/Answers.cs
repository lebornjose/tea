﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:26
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Answers : Sinbo.Controller
    {
        public Answers()
        {
            this.Access = Right.Administrator;
        }

        public void home(string question_id,string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string where=string.Format("question_id='{0}'",question_id);
            JabinfoView view = new JabinfoView("article_answers_home", this.JabinfoContext);
            view.Variable["answersList"] =answersModel.Select(index, 30, where);
			  view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["question_id"] = question_id;
            view.Variable["total"] = answersModel.Count(where);
            view.Render();
        }

        public void add(string question_id)
        {
            if (this.IsPost)
            {
                answersModel.Insert(this.Post);
                Output("#","提交成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_answers_add", this.JabinfoContext);
            view.Variable["question_id"]=question_id;
            view.Render();
        }

        public void edit(string answers_id)
        {
            if (answers_id == null)
            {
                answersModel.Update(this.Post);
                Output("#","修改成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_answers_edit", this.JabinfoContext);
			view.Variable["answers"] = new AnswersVO(answers_id);
            view.Render();
        }

        public void remove(string answers_id)
        {
            answersModel.Delete(answers_id);
        }

        private AnswersModel _answersModel = null;
        private AnswersModel answersModel
        {
            get
            {
                if (_answersModel == null)
                    _answersModel = new AnswersModel();
                return _answersModel;
            }
        }
    }
}