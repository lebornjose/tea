/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Web;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Article
{
    class Article : Sinbo.Controller
    {
        public Article()
        {
            this.Access = Right.Administrator;
        }

        #region 文章列表
        public void home(string start)
        {
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            int size = 30;
            JabinfoView view = new JabinfoView("article_article_home", this.JabinfoContext);
            view.Variable["articleList"] = articleModel.Select(index, size, string.Empty, "article_id desc");
            view.Variable["index"] = index;
            view.Variable["size"] = size;
            view.Variable["start"] = index * size;
            view.Variable["category_id"] = "";
            view.Variable["total"] = articleModel.Count();
            view.Variable["articletype"] = Jabinfo.Help.Config.Get("article.jtype");
            view.Render();
        }

        public void index(string category_id, string start)
        {
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            int size = 30;
            string where = string.Format("categorys like '%,{0},%'", category_id);
            JabinfoView view = new JabinfoView("article_article_index", this.JabinfoContext);
            view.Variable["articleList"] = articleModel.Select(index, size, where, "article_id desc");
            view.Variable["index"] = index;
            view.Variable["size"] = size;
            view.Variable["start"] = index * size;
            view.Variable["total"] = articleModel.Count(where);
            view.Variable["category_id"] = category_id;
            view.Variable["category"] = new CategoryVO(category_id);
            view.Variable["articletype"] = Jabinfo.Help.Config.Get("article.jtype");
            view.Render();
        }
        #endregion

        #region	添加文章
        public void add(string category_id)
        {
            if (this.IsPost)
            {
                this.Post["article_id"] = Help.Basic.JabId;
                this.Post["pubtime"] = Jabinfo.Help.Date.StringToDate(this.Post["pubtime"]).ToString();
                //记录图片
                if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
                {
                    Jabinfo.Help.Image.Save(this.Post["article_id"], this.Files["image"]);
                    JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
                    foreach (string key in sizes.Keys)
                    {
                        string[] size = sizes[key].Split('x');
                        Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["article_id"], key), this.Post["article_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
                    }
                    this.Post["attach"] = "1";//有预览图
                }
                else
                {
                    this.Post["attach"] = "0";
                }
                if (this.Files["afile"] != null && this.Files["afile"].ContentLength > 10)
                {
                    string fileName = this.Files["afile"].FileName;
                    this.Post["model"] = fileName.Substring(fileName.LastIndexOf('.') + 1);
                    Jabinfo.Help.Upload.Save(this.Post["article_id"] + "_file", this.Files["afile"], this.Post["model"]);
                }
                if (string.IsNullOrEmpty(this.Post["summary"]))
                {
                    string content = Jabinfo.Help.Formate.HtmlClear(this.Post["content"]);
                    if (content.Length < 200)
                        this.Post["summary"] = content;
                    else
                        this.Post["summary"] = content.Substring(0, 200);
                }
                if (this.Post["category_id"] != string.Empty)
                    this.Post["categorys"] = string.Format(",{0},", this.Post["category_id"]);
                this.Post["uid"] = this.Uid;
                articleModel.Insert(this.Post);
                Article_detailModel detailModel = new Article_detailModel();
                detailModel.Insert(this.Post);
                if (this.Post["category_id"] == string.Empty)
                    this.Jump(string.Format("article/article/category/{0}", this.Post["article_id"]), "添加成功,请设置文章分类");
                else
                    this.Jump(string.Format("article/article/edit/{0}", this.Post["article_id"]), "添加成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_article_add", this.JabinfoContext);
	    view.Variable["city"] = Jabinfo.Help.Config.Get("schoolrc.city");
            view.Variable["category_id"] = category_id;
            view.Variable["now"] = Jabinfo.Help.Date.Now;
            view.Render();
        }
        #endregion

        #region 设置文字评论状态
        /// <summary>
        /// 设置文字评论状态
        /// </summary>
        /// <param name="article"></param>
        public void set_post(string article_id)
        {
            ArticleVO article = new ArticleVO(article_id);
            if (article.Article_id != null)
            {
                JabinfoKeyValue data = new JabinfoKeyValue();
                data["article_id"] = article_id;
                if (article.Ispost == "1")
                    data["ispost"] = "0";
                else
                    data["ispost"] = "1";
                articleModel.Update(data);
                Jabinfo.Help.Basic.Clear("article_home_detail", article_id);//清除前台缓存
                Print(data["ispost"]);
            }
        }
        #endregion

        #region	编辑文章
        public void edit(string article_id)
        {
            Article_detailModel detailModel = new Article_detailModel();
            Article_detailVO detail = null;
            if (this.IsPost)
            {
                ArticleVO article = new ArticleVO(this.Post["article_id"]);
                detail = new Article_detailVO(this.Post["article_id"]);
                this.Post["pubtime"] = Jabinfo.Help.Date.StringToDate(this.Post["pubtime"]).ToString();
                if (this.Post["iscash"] == null && article.Iscash == "1")
                {
                    //记录分类
                    if (detail.Categorys != null)
                    {
                        string[] category_ids = detail.Categorys.Split(',');
                        JabinfoKeyValue category = new JabinfoKeyValue();
                        category["object_id"] = this.Post["article_id"];
                        category["jtype"] = "1";
                        foreach (string id in category_ids)
                        {
                            category["pubtime"] = this.Post["pubtime"];
                            category["relation_id"] = id;
                        }
                    }
                    //记录标签
                    if (detail.Tags != null)
                    {
                        string[] tag_ids = detail.Tags.Split(',');
                        JabinfoKeyValue tag = new JabinfoKeyValue();
                        tag["object_id"] = this.Post["article_id"];
                        tag["jtype"] = "5";
                        foreach (string id in tag_ids)
                        {
                            tag["pubtime"] = this.Post["pubtime"];
                            tag["relation_id"] = id;
                        }
                    }
                }
                //记录图片
                if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
                {
                    Jabinfo.Help.Image.Save(this.Post["article_id"], this.Files["image"]);
                    JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
                    foreach (string key in sizes.Keys)
                    {
                        string[] size = sizes[key].Split('x');
                        Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["article_id"], key), this.Post["article_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
                    }
                    this.Post["attach"] = "1";//有预览图
                }
                if (this.Files["afile"] != null && this.Files["afile"].ContentLength > 10)
                {
                    string fileName = this.Files["afile"].FileName;
                    this.Post["model"] = fileName.Substring(fileName.LastIndexOf('.') + 1);
                    Jabinfo.Help.Upload.Save(this.Post["article_id"] + "_file", this.Files["afile"], this.Post["model"]);
                }
                if (this.Post["iscash"] == null)
                    this.Post["iscash"] = "0";
                articleModel.Update(this.Post);
                this.Post["categorys"] = this.Post["category_id"];
                this.Post["tags"] = this.Post["tag_id"];
                detailModel.Update(this.Post);
                this.Jump(string.Format("article/article/edit/{0}", this.Post["article_id"]), "编辑成功");
                return;
            }
            detail = new Article_detailVO(article_id);
            detail.Content = Jabinfo.Help.Formate.HtmlEncode(detail.Content);
            JabinfoView view = new JabinfoView("article_article_edit", this.JabinfoContext);
            view.Variable["article"] = articleModel.Get(article_id);
			view.Variable["city"] = Jabinfo.Help.Config.Get("schoolrc.city");
            view.Variable["detail"] = detail;
            view.Render();
        }
        #endregion

        #region	文章分类
        /// <summary>
        /// 文章分类
        /// </summary>
        public void category(string article_id)
        {
            ArticleVO article = null;
            if (this.IsPost)
            {
                article = new ArticleVO(this.Post["article_id"]);
                JabinfoKeyValue data = new JabinfoKeyValue();
                data["article_id"] = this.Post["article_id"];
                data["tags"] = string.IsNullOrEmpty(this.Post["tag_id"]) ? "" : string.Format(",{0},", this.Post["tag_id"]);
                data["categorys"] = string.IsNullOrEmpty(this.Post["categorys"]) ? "" : string.Format(",{0},", this.Post["categorys"]);
                Article_detailModel detailModel  = new Article_detailModel();
                detailModel.Update(data);
                if (article.Iscash == "0" && !string.IsNullOrEmpty(data["categorys"]))
                {
                    string[] lt = data["categorys"].Split(',');
                    data["category_id"] = lt[lt.Length - 2];
                    this.articleModel.Update(data);
                }
                Output("#", "保存成功");
                return;
            }
            string[] categorys = new String[0];
            article = new ArticleVO(article_id);
            if (!string.IsNullOrEmpty(article.Categorys))
                categorys = article.Categorys.Split(',');

            TagsModel tagsModel = new TagsModel();
            string initTag = string.Empty;
            for (int i = 0; i < categorys.Length; i++)
            {
                TagsVO[] tags = tagsModel.Select(categorys[i]);
                if (tags != null)
                {
                    CategoryVO categoryVO = new CategoryVO(categorys[i]);
                    initTag = string.Format("{0}<li id=\"{1}\"><strong>{2}</strong>&nbsp;", initTag, categoryVO.Category_id, categoryVO.Title);
                    foreach (TagsVO tag in tags)
                    {
                        initTag = string.Format("{0}<input type=\"checkbox\" name=\"tag_id\" value=\"{1}\" />{2}", initTag, tag.Tags_id, tag.Title);
                    }
                    initTag = string.Format("{0}</li>", initTag);
                }
            }

            JabinfoView view = new JabinfoView("article_article_category", this.JabinfoContext);
            view.Variable["initTag"] = initTag;
            view.Variable["top_id"] = Jabinfo.Help.Config.Get("core.top", "article");
            view.Variable["article"] = new ArticleVO(article_id);
            view.Variable["detail"] = article;
            view.Variable["article_id"] = article_id;
            view.Render();
        }
        #endregion

        #region	把文章添加到推荐位

        public void recommend(string object_id)
        {
            PositionModel positionModel = new PositionModel();
            if (this.IsPost)
            {
                ArticleVO article = new ArticleVO(this.Post["object_id"]);
                if (article.Article_id != null)
                {
                    this.Post["advertise_id"] = Jabinfo.Help.Basic.JabId;
                    if (this.Post["jtype"] != "0" && this.Files["file"].ContentLength > 0)
                    {
                        Jabinfo.Help.Upload.Save(this.Post["advertise_id"], this.Files["file"], this.Post["jtype"]);
                        string file = Jabinfo.Help.Upload.Get(this.Post["advertise_id"], this.Post["jtype"]);
                        PositionVO position = new PositionVO(this.Post["position_id"]);
                        switch (this.Post["jtype"])
                        {
                            case "jpg":
                                this.Post["content"] = string.Format("<img src=\"{0}\" width=\"{1}\" height=\"{2}\" alt=\"{3}\" />", file, position.Width, position.Height, this.Post["title"]);
                                break;
                            case "swf":
                                this.Post["content"] = string.Format("");
                                break;
                        }
                    }
                    this.Post["title"] = article.Title;
                    this.Post["summary"] = article.Summary;
                    this.Post["target"] = string.Format("article/home/detail/{0}", this.Post["object_id"]);
                    AdvertiseModel advertiseModel = new AdvertiseModel();
                    if (advertiseModel.Insert(this.Post) == "")
                        this.Jump(this.Url.Site("article/article/home"), "添加失败，结束时间不能小于开始时间");
                    else
                        this.Jump(this.Url.Site("article/article/home"), "添加推荐位成功");
                }
                return;
            }
            JabinfoView view = new JabinfoView("article_article_recommend", this.JabinfoContext);
            view.Variable["object_id"] = object_id;
            view.Variable["positions"] = positionModel.Select();
            view.Render();
        }
        #endregion

        #region	文章搜索
        public void option()
        {
            ArticleVO[] articles = this.articleModel.Like(0, 20, this.Post["keyword"], "jtype<>'6'", string.Empty);
            if (articles != null)
            {
                foreach (ArticleVO article in articles)
                    Print(string.Format("<option value=\"{0}\">{1}</option>", article.Article_id, article.Title));
            }
            else
                Print("<option value=\"\">不存在该文章</option>");
        }

        public void search_option()
        {
			CategoryModel categoryModel = new CategoryModel();
            JabinfoView view = new JabinfoView("article_article_search_option", this.JabinfoContext);
            view.Variable["top_id"] = Jabinfo.Help.Config.Get("core.top", "article");
			view.Variable["categoryList"] = categoryModel.Select(0,0,string.Empty,string.Empty);
			view.Variable["city"] = Jabinfo.Help.Config.Get("schoolrc.city");
            view.Render();
        }

        public void search(string start)
        {
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string where = string.Empty;
            if (this.IsPost)
            {
                if (!string.IsNullOrEmpty(this.Post["caption"]))
                    where = string.Format("and caption='{0}'", this.Post["caption"]);

                if (!string.IsNullOrEmpty(this.Post["category_id"]))
                    where = string.Format("{0} and categorys like '%{1}%'", where, this.Post["category_id"]);

                if (!string.IsNullOrEmpty(this.Post["keyword"]))
                    where = string.Format("{0} and title like '%{1}%'", where, this.Post["keyword"]);

                if (where != string.Empty)
                    where = where.Substring(4);

                this.JabinfoContext.Session.Add("article_search", where);
            }
            else
            {
                where = this.JabinfoContext.Session.Get("article_search").ToString();
            }

            JabinfoView view = new JabinfoView("article_article_search", this.JabinfoContext);
            view.Variable["articleList"] = articleModel.Select(index, 30, where, "article_id desc");
            view.Variable["index"] = index;
            view.Variable["start"] = index * 30;
            view.Variable["size"] = 30;
            view.Variable["total"] = articleModel.Count(where);
            view.Variable["articletype"] = Jabinfo.Help.Config.Get("article.jtype");
            view.Render();
        }
        #endregion

        #region 文章回收站
        /// <summary>
        /// 将文章移入回收站
        /// </summary>
        /// <param name="article_id"></param>
        public void remove(string article_id)
        {
            ArticleVO article = new ArticleVO(article_id);
            //文章进入回收站
            Jabinfo.JabinfoLoger.DeleteVO(article.Title, article.Article_id, "article_del", this.Uid, article);
            articleModel.Delete(article_id);
            Output("删除成功！");
        }

        /// <summary>
        /// 回收站彻底删除
        /// </summary>
        /// <param name="article_id"></param>
        /// <param name="logid"></param>
        public void clear(string article_id, string logid)
        {
            Article_detailModel detailModel = new Article_detailModel();
            detailModel.Delete(article_id);
            //删除文章附件
            JabinfoLoger.Delete(logid);
        }

        /// <summary>
        /// 回收站列表
        /// </summary>
        /// <param name="start"></param>
        public void recycle(string start)
        {
            int index = 0;
            if (start != null)
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView("article_article_recycle", this.JabinfoContext);
            view.Variable["logs"] = JabinfoLoger.Select(index, 30, "logtype='article_del'");
            view.Variable["total"] = JabinfoLoger.Count("logtype='article_del'");
            view.Variable["size"] = 30;
            view.Variable["index"] = index;
            view.Render();
        }

        /// <summary>
        /// 从回收站恢复文章
        /// </summary>
        /// <param name="logid"></param>
        public void restore(string logid)
        {
            object result = JabinfoLoger.Restore(logid, typeof(ArticleVO));
            if (result == null)
                Output("恢复失败");
            else
                Output("#", "恢复成功");
        }

        #endregion

        #region	文章图片
        /// <summary>
        /// 文章图片
        /// </summary>
        /// <param name="article_id"></param>
        public void photo(string article_id)
        {
            AttachModel attachModel = new AttachModel();
            if (this.IsPost)
            {
                bool flag = false;
                if (this.Post["attach_id"] == null)
                {
                    flag = true;
                    this.Post["attach_id"] = Jabinfo.Help.Basic.JabId;
                }
                foreach (string name in this.Files.AllKeys)
                {
                    if (this.Files[name].ContentLength > 10)
                    {
                        string file_id = string.Format("{0}{1}", this.Post["attach_id"], name.Substring(name.IndexOf('_')));
                        Jabinfo.Help.Image.Save(file_id, this.Files[name]);
                    }
                }
                if (flag)
                    attachModel.Insert(this.Post);
                else
                    attachModel.Update(this.Post);
                this.Jump(string.Format("article/article/photo/{0}", this.Post["object_id"]), "保存成功");
                return;
            }

            JabinfoView view = new JabinfoView("article_article_photo", this.JabinfoContext);
            view.Variable["article_id"] = article_id;
            view.Variable["attachs"] = attachModel.Select(article_id);
            view.Render();
        }
        #endregion

        #region 专题文章管理
        public void photoremove(string attach_id)
        {
            AttachModel attachModel = new AttachModel();
            attachModel.Delete(attach_id);
        }

        public void special(string article_id)
        {
            SpecialModel specialModel = new SpecialModel();
            JabinfoView view = new JabinfoView("article_article_special", this.JabinfoContext);
            view.Variable["article_id"] = article_id;
            view.Variable["specails"] = specialModel.Select(article_id);
            view.Render();
        }

        public void specialadd(string article_id)
        {
            if (this.IsPost)
            {
                SpecialVO special = new SpecialVO();
                this.Post["special_id"] = Jabinfo.Help.Basic.JabId;
                special.Insert(this.Post);
                Output("#", "添加成功");
                return;
            }

            JabinfoView view = new JabinfoView("article_article_specialadd", this.JabinfoContext);
            view.Variable["article_id"] = article_id;
            view.Render();
        }

        public void specialremove(string article_id, string object_id)
        {
            SpecialModel specialModel = new SpecialModel();
            specialModel.Delete(article_id, object_id);
        }
        #endregion

        #region 自动更新图片大小
        public void auto(string article_id)
        {
            if (article_id == null)
                article_id = "0";
            ArticleModel articleModel = new ArticleModel();
            string[] result = articleModel.IDS(0, 5, string.Format("article_id>'{0}'", article_id), "article_id asc");
            if (result.Length == 0)
            {
                Print("更新完成");
                return;
            }
            Print("更新中...{0}", article_id);
            foreach (string id in result)
            {
                article_id = id;
                string file = Jabinfo.Help.Upload.PysPath(article_id, "jpg");
                if (file != null)
                {
                    JabinfoKeyValue presize = Jabinfo.Help.Config.Get("article.photosize");
                    foreach (string key in presize.Keys)
                    {
                        string[] size = presize[key].Split('x');
                        Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", article_id, key), article_id, Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
                    }
                }
            }
            this.Jump("article/article/auto/" + article_id);
        }
        #endregion

        #region Model
        private ArticleModel _Model = null;
        private ArticleModel articleModel
        {
            get
            {
                if (_Model == null)
                    _Model = new ArticleModel();
                return _Model;
            }
        }
        #endregion

    }
}