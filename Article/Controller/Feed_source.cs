﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-3-3 下06时27分09秒
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Article
{
    class Feed_source : Sinbo.Controller
    {
		#region	Constructor
        public Feed_source()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView("article_feed_source_home", this.JabinfoContext);
            view.Variable["feed_sourceList"] = feed_sourceModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = feed_sourceModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add(string category_id)
        {
            if (this.IsPost)
            {
                feed_sourceModel.Insert(this.Post);
                Output("#", "Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView("article_feed_source_add", this.JabinfoContext);
            view.Variable["category_id"] = category_id;
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string source_id)
        {
            if (this.IsPost)
            {
                feed_sourceModel.Update(this.Post);
                Output("#", "Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView("article_feed_source_edit", this.JabinfoContext);
			view.Variable["feed_source"] = new Feed_sourceVO(source_id);
            view.Render();
        }
		#endregion


        #region 查看文章源
        public void category(string category_id, string start)
        {
            int index = 0;
            int size = 30;
            if (!string.IsNullOrEmpty(start))
            {
                index = Convert.ToInt32(start);
            }
            string where = string.Format("category_id='{0}'", category_id);
            JabinfoView view = new JabinfoView("article_feed_source_category", this.JabinfoContext);
            view.Variable["feed_sourceList"] = feed_sourceModel.Select(0, size, where, "jindex desc, source_id desc");
            view.Variable["index"] = index;
            view.Variable["size"] = size;
            view.Variable["start"] = index * size;
            view.Variable["total"] = feed_sourceModel.Count(where);
            view.Variable["category"] = new CategoryVO(category_id);
            view.Render();
        }
        #endregion

		#region	remove
        public void remove(string source_id)
        {
            feed_sourceModel.Delete(source_id);
        }
		#endregion

		#region	_model
        private Feed_sourceModel _model = null;
        private Feed_sourceModel feed_sourceModel
        {
            get
            {
                if (_model == null)
                    _model = new Feed_sourceModel();
                return _model;
            }
        }
		#endregion
    }
}