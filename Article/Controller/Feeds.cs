﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-3-3 下06时28分07秒
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Article
{
    class Feeds : Sinbo.Controller
    {
		#region	Constructor
        public Feeds()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView("article_feeds_home", this.JabinfoContext);
            view.Variable["feedsList"] = feedsModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = feedsModel.Count(string.Empty);
            view.Render();
        }
		#endregion

        #region 查看种子

        public void source(string source_id, string start)
        {
            int index = 0;
            int size = 30;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string where = string.Format("source_id='{0}'", source_id);
            Feed_sourceVO feed_sourceVO = new Feed_sourceVO(source_id);
            CategoryVO categoryVO = new CategoryVO(feed_sourceVO.Category_id);
            JabinfoView view = new JabinfoView("article_feeds_source", this.JabinfoContext);
            view.Variable["feedsList"] = feedsModel.Select(index, size, where, string.Empty);
            view.Variable["index"] = index;
            view.Variable["size"] = size;
            view.Variable["start"] = index * size;
            view.Variable["total"] = feedsModel.Count(where);
            view.Variable["category"] = categoryVO;
            view.Variable["feed_source"] = feed_sourceVO;
            view.Render();
        }
        #endregion

        #region	add
        public void add()
        {
            if (this.IsPost)
            {
                feedsModel.Insert(this.Post);
				Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView("article_feeds_add", this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string feeds_id)
        {
            if (this.IsPost)
            {
                feedsModel.Update(this.Post);
                Output("#", "Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView("article_feeds_edit", this.JabinfoContext);
			view.Variable["feeds"] = new FeedsVO(feeds_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string feeds_id)
        {
            feedsModel.Delete(feeds_id);
        }
		#endregion

		#region	_model
        private FeedsModel _model = null;
        private FeedsModel feedsModel
        {
            get
            {
                if (_model == null)
                    _model = new FeedsModel();
                return _model;
            }
        }
		#endregion
    }
}