﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Links : Sinbo.Controller
    {
        public Links()
        {
            this.Access = Right.Administrator;
        }
        public void home(string start, string type)
        {
            int index=0;
            string where = string.Empty;
            bool istype = false;
            if(type!=null)
            {
                where=string.Format("category='{0}'",type);
                istype = true;
            }
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView("article_links_home", this.JabinfoContext);
            view.Variable["linksList"] = linksModel.Select(index,30,where);
            view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = linksModel.Count(where);
            view.Variable["category"]=Jabinfo.Help.Config.Get("enum.links");
            view.Variable["istype"] = istype;
            view.Render();
        }
        
        
        
        public void type(string start)
        {
          int index=0;
          if(!string.IsNullOrEmpty(start))
          	index = Convert.ToInt32(start);
        	JabinfoView view = new JabinfoView("article_links_type", this.JabinfoContext);
        	view.Variable["category"] = Jabinfo.Help.Config.Get("enum.links");
        	view.Render();
        }
        
        public void add()
        {
            if (this.IsPost)
            {
                linksModel.Insert(this.Post);
                Output("#","提交成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_links_add", this.JabinfoContext);
            view.Variable["category"]=Jabinfo.Help.Config.Get("enum.links");
            view.Render();
        }

        public void edit(string links_id)
        {
            if (links_id == null)
            {
                linksModel.Update(this.Post);
                Output("#","修改成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_links_edit", this.JabinfoContext);
			  view.Variable["links"] = new LinksVO(links_id);
			  view.Variable["category"] = Jabinfo.Help.Config.Get("enum.links");
            view.Render();
        }

        public void remove(string links_id)
        {
            linksModel.Delete(links_id);
        }

        private LinksModel _linksModel = null;
        private LinksModel linksModel
        {
            get
            {
                if (_linksModel == null)
                    _linksModel = new LinksModel();
                return _linksModel;
            }
        }
    }
}