﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Message : Sinbo.Controller
    {
        public Message()
        {
            this.Access = Right.Administrator;
        }

        #region 留言列表
        public void home(string mtype, string start)
        {
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string page = string.Format("/{0}", mtype);
            string where = string.IsNullOrEmpty(mtype)?"": string.Format("mtype='{0}'", mtype);
            string caption = string.Empty;
            string temtemplate = string.Empty;
            switch (mtype)
            {
                case "0":
                    caption = "留言";
                    temtemplate = "article_message_home";
                    break;
                case "1":
                    caption = "留言";
                    temtemplate = "article_message_home";
                    break;
                case "2":
                    caption = "产品留言";
                    temtemplate = "article_message_home";
                    break;
                case "4":
                    caption = "产品咨询";
                    temtemplate = "article_message_homequestion";
                    break;
            }
            JabinfoView view = new JabinfoView(temtemplate, this.JabinfoContext);
            view.Variable["messageList"] = messageModel.Select(index, 30, where);
            view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["mtype"] = mtype;
            view.Variable["caption"] = caption;
            view.Variable["total"] = messageModel.Count(where);
            view.Variable["status"] = Jabinfo.Help.Config.Get("enum.choice");
            view.Variable["page"] = page;
            view.Render();
        }
        #endregion

        #region 留言的增、删、改
        public void add(string mtype, string object_id)
        {
            if (this.IsPost)
            {
                this.Post["uid"] = this.Uid;
                this.Post["rank"] = "0";
                string obj = this.Post["object_id"];
                messageModel.Insert(this.Post);
                Output("#","提交成功");
                return;
            }

            JabinfoView view = new JabinfoView("article_message_add", this.JabinfoContext);
            view.Variable["mtype"] = mtype;
            view.Variable["object_id"] = object_id;
            view.Render();
        }

		#region 回答问题
		public void answer(string message_id)
		{
			 string reply=string.Empty;
			if (this.IsPost)
			{
				MessageVO messageVO = new MessageVO(this.Post["message_id"]);
				if (messageVO.Status == "0")
					reply =this.Post["reply"];
				else
					reply = messageVO.Reply + "," + this.Post["reply"];
				this.Post["reply"] = reply;
				this.Post["status"] = "1";
				messageModel.Update(this.Post);
				Output("#", "回答问题成功");
				return;
			}
			JabinfoView view = new JabinfoView("article_message_answer", this.JabinfoContext);
			view.Variable["message"] = new MessageVO(message_id);
			view.Render();
		}
		#endregion
		public void edit(string message_id)
        {
            if (message_id == null)
            {
                if (this.Post["rank"] == null)
                    this.Post["rank"] = "0";
                messageModel.Update(this.Post);
                MessageVO message = new MessageVO(this.Post["message_id"]);
                Output("#","修改成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_message_edit", this.JabinfoContext);
            view.Variable["message"] = new MessageVO(message_id);
            view.Render();
        }

        public void reply(string message_id)
        {
            if (message_id == null)
            {
                if (this.Post["rank"] == null)
                    this.Post["rank"] = "0";
                messageModel.Update(this.Post);
                Output("#","修改成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_message_replyquestion", this.JabinfoContext);
            view.Variable["message"] = new MessageVO(message_id);
            view.Render();
        }
        public void remove(string message_id)
        {
            MessageVO message = new MessageVO(message_id);
            messageModel.Delete(message_id);
        }
        #endregion

        #region 设置留言状态
        public void ispost(string message_id)
        {
            MessageVO message = new MessageVO(message_id);
            if (message.Message_id != null)
            {
                JabinfoKeyValue data = new JabinfoKeyValue();
                data["message_id"] = message_id;
                if (message.Status == "0")
                {
                    data["status"] = "1";
                }
                else
                    data["status"] = "0";
                messageModel.Update(data);
                Output("#","设置成功");
                return;
            }
            Print("#$设置失败");
            return;
        }
        #endregion

		private MessageModel _model = null;
        private MessageModel messageModel
        {
            get
            {
                if (_model == null)
                    _model = new MessageModel();
                return _model;
            }
        }
    }
}