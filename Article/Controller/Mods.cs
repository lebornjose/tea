/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2011-12-15 13:26:23
 */
using System;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
	class Mods : Sinbo.Controller
	{
		#region	Constructor
		public Mods()
		{
			this.Access = Right.Administrator;
		}
		#endregion

		#region	home
		public void home(string lan)
		{
			if (string.IsNullOrEmpty(lan))
				lan = "0";
			string where=string.Format("lan='{0}'",lan);
			int count = this.modsModel.Count(where);
			if (count == 0)
			{
				JabinfoKeyValue data = new JabinfoKeyValue();
				data["mods_id"] = Jabinfo.Help.Basic.JabId;
				data["title"] = "首页";
				data["target"] = "";
				data["jindex"] = "0";
				data["action"] = "";
				data["lan"] = lan;
				data["jtype"] = "";
				this.modsModel.Insert(data);
			}
			JabinfoView view = new JabinfoView("core_mods_home", this.JabinfoContext);
			view.Variable["modsList"] = modsModel.Select(0, 30, where, "jindex asc");
			view.Variable["lan"] = lan;
			view.Variable["type"] = Jabinfo.Help.Config.Get("core.mods");
			view.Render();
		}
		#endregion

		#region select
		public void select(string type)
		{
			JabinfoKeyValue top = Jabinfo.Help.Config.Get("core.top");
			switch (type)
			{
			case "0"://页面
				PageModel pageModel = new PageModel();
				PageVO[] pages = pageModel.Select(0, 30, null);
				Print("<select name=\"mods_id\">");
				if (pages != null)
					foreach (PageVO page in pages)
						Print(string.Format("<option value=\"{0}\">{1}</option>", page.Page_id, page.Title));
				Print("</select>");
				break;
			case "1"://分类
				CategoryModel categoryModel = new CategoryModel();
				CategoryVO[] categorys = categoryModel.Select(top["article"]);
				Print("<select name=\"mods_id\">");
				Print("<option value=\"\">请选择</option>");
				Print(API.Instance.Option("0", 0));
				Print("</select>");
				break;
			case "2"://标签
				TagsModel tagsModel = new TagsModel();
				TagsVO[] tags = tagsModel.Select(top["tag"]);
				Print("<select name=\"mods_id\">");
				if (tags != null)
					foreach (TagsVO tg in tags)
						Print(string.Format("<option value=\"{0}\">{1}</option>", tg.Tags_id, tg.Title));
				Print("</select>");
				break;
			case "9"://自定义
				Print("名称<input type=\"text\" name=\"title\" style=\"width:240px\" /><br />地址<input type=\"text\" style=\"width:240px\" name=\"target\" />");
				break;
			}
		}
		#endregion

		#region	add
		public void add(string lan)
		{
			if (this.IsPost)
			{
				switch (this.Post["jtype"])
				{
				case "0":
					PageVO pageVO = new PageVO(this.Post["mods_id"]);
					this.Post["title"] = pageVO.Title;
					this.Post["target"] = string.Format("article/home/page/{0}", pageVO.Page_id);
					this.Post["action"] = string.Format("article/page/edit/{0}", pageVO.Page_id);
					break;
				case "1":
					CategoryVO categoryVO = new CategoryVO(this.Post["mods_id"]);
					this.Post["title"] = categoryVO.Title;
					this.Post["target"] = string.Format("article/home/category/{0}", categoryVO.Category_id);
					this.Post["action"] = string.Format("article/article/index/{0}", categoryVO.Category_id);
					break;
				case "2":
					TagsVO tagsVO = new TagsVO(this.Post["mods_id"]);
					this.Post["title"] = tagsVO.Title;
					this.Post["target"] = string.Format("article/home/tag/{0}", tagsVO.Tags_id);
					this.Post["action"] = "";
					break;
				case "3":
					this.Post["mods_id"] = Help.Basic.JabId;
					this.Post["title"] = "在线留言";
					this.Post["target"] = "message";
					this.Post["action"] = "";
					break;
				case "9":
					this.Post["mods_id"] = Help.Basic.JabId;
					this.Post["action"] = "";
					break;
				}

				modsModel.Insert(this.Post);
				Output("#","提交成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_mods_add", this.JabinfoContext);
			view.Variable["lan"] = lan;
			view.Render();
		}
		#endregion

		#region	edit
		public void edit(string mods_id)
		{
			if (mods_id == null)
			{
				modsModel.Update(this.Post);
				Output("#","修改成功");
				return;
			}
			ModsVO modsVO = new ModsVO(mods_id);
			JabinfoView view = new JabinfoView("core_mods_edit", this.JabinfoContext);
			view.Variable["mods"] = modsVO;
			view.Variable["class"] = modsVO.Class.Replace("\"", "&#34;");
			view.Render();
		}
		#endregion

		#region	remove
		public void remove(string mods_id)
		{
			modsModel.Delete(mods_id);
		}
		#endregion

		#region	_model
		private ModsModel _model = null;
		private ModsModel modsModel
		{
			get
			{
				if (_model == null)
					_model = new ModsModel();
				return _model;
			}
		}
		#endregion
	}
}