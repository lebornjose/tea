﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:26
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Options : Sinbo.Controller
    {
        public Options()
        {
            this.Access = Right.Administrator;
        }
        #region 问题列表
        public void home(string paper_id,string question_id,string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string where=string.Format("question_id='{0}'",question_id);
            JabinfoView view = new JabinfoView("article_options_home", this.JabinfoContext);
            view.Variable["optionsList"] = optionsModel.Select(index, 30, where);
			  view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["paper_id"] = paper_id;
            view.Variable["question_id"] = question_id;
            view.Variable["total"] = optionsModel.Count(where);
            view.Render();
        }
        #endregion

        #region 增、删、改问题
        public void add(string question_id)
        {
            if (this.IsPost)
            {
                optionsModel.Insert(this.Post);
                Output("#","提交成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_options_add", this.JabinfoContext);
            view.Variable["question_id"]=question_id;
            view.Render();
        }

        public void edit(string options_id)
        {
            if (options_id == null)
            {
                optionsModel.Update(this.Post);
                Output("#","修改成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_options_edit", this.JabinfoContext);
			view.Variable["options"] = new OptionsVO(options_id);
            view.Render();
        }

        public void remove(string options_id)
        {
            optionsModel.Delete(options_id);
        }
        #endregion

        private OptionsModel _optionsModel = null;
        private OptionsModel optionsModel
        {
            get
            {
                if (_optionsModel == null)
                    _optionsModel = new OptionsModel();
                return _optionsModel;
            }
        }
    }
}