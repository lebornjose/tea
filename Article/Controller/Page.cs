﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-18 16:43:57
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Page : Sinbo.Controller
	{
		public Page()
		{
			this.Access = Right.Administrator;
		}

        #region 页面列表
        public void home(string jtype, string start)
		{
			int index=0;
			string page_id = "";
			string where = string.Empty;
			PageVO page=new PageVO(jtype);
			string template="article_page_home";
			if(jtype!="0")
                where = string.Format("jtype='{0}'", jtype);
			if(page.Page_id != null)
			{
				page_id = jtype;
				jtype = page.Jtype;
				template="article_page_home2";
			}
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			JabinfoView view = new JabinfoView(template, this.JabinfoContext);
			view.Variable["pageList"] =pageModel.Select(index, 30, where);
			view.Variable["index"] = index;
			view.Variable["size"] = 30;
			view.Variable["jtype"] = jtype;
			view.Variable["total"] = pageModel.Count(where);
			view.Variable["page_id"] = page_id;
			view.Render();
		}
        #endregion

        #region 页面类型列表
        public void type()
		{
			JabinfoView view = new JabinfoView("article_page_type", this.JabinfoContext);
			view.Variable["page"] = Jabinfo.Help.Config.Get("enum.page");
			view.Render();
		}
        #endregion

        #region 页面的增、删、改
        public void add(string jtype)
		{
			if (this.IsPost)
			{
				if(this.Post["jtype"] == null)
					this.Post["jtype"] = "0";
                if (string.IsNullOrEmpty(this.Post["summary"]))
                {
                    string content = Jabinfo.Help.Formate.HtmlClear(this.Post["content"]);
                    if (content.Length < 360)
                        this.Post["describe"] = content;
                    else
                        this.Post["describe"] = content.Substring(0, 360);
                }
				string articler_id = pageModel.Insert(this.Post);
				if(this.Files["file_20"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(articler_id, this.Files["file"]);
				}
				this.Jump(string.Format("article/page/edit/{0}",articler_id),"提交成功");
				return;
			}
			if(string.IsNullOrEmpty(jtype))
				jtype="0";
			JabinfoView view = new JabinfoView("article_page_add", this.JabinfoContext);
			view.Variable["jtype"] = jtype;
            if (jtype == "0")
                view.Variable["type"] = Jabinfo.Help.Config.Get("enum.page");
			view.Render();
		}

		public void edit(string page_id)
		{
			if (page_id == null)
			{
				pageModel.Update(this.Post);
				Jabinfo.Help.Basic.Clear("page",this.Post["page_id"]);
				foreach(string key in this.Files.AllKeys)
				{
					if(this.Files[key].ContentLength > 1)
					{
						string[] file = key.Split('_');
						Jabinfo.Help.Image.Save(string.Format("{0}_{1}",this.Post["page_id"],file[1]), this.Files["file_20"]);
					}
				}
				this.Jump(string.Format("article/page/edit/{0}",this.Post["page_id"]),"编辑成功");
				return;
			}
			JabinfoView view = new JabinfoView("article_page_edit", this.JabinfoContext);
			view.Variable["page"] = new PageVO(page_id);
			view.Render();
		}
		
		public void remove(string page_id)
		{
			pageModel.Delete(page_id);
		}
        #endregion

        #region 设置页面是否可以评论
        public void set_post(string page_id)
        {
            PageVO page = new PageVO(page_id);
            if (page.Page_id != null)
            {
                JabinfoKeyValue data = new JabinfoKeyValue();
                data["page_id"] = page_id;
                if (page.Ispost == "1")
                    data["ispost"] = "0";
                else
                    data["ispost"] = "1";
                pageModel.Update(data);
                Jabinfo.Help.Basic.Clear("article_home_page", page_id);
                Print(data["ispost"]);
            }
        }
        #endregion

        #region Model
        private PageModel _Model = null;
		private PageModel pageModel
		{
			get
			{
				if (_Model == null)
					_Model = new PageModel();
				return _Model;
			}
        }
        #endregion
    }
}