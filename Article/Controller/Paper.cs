﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:26
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Paper : Sinbo.Controller
	{
		public Paper()
		{
			this.Access = Right.Administrator;
		}
		public void home(string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			JabinfoView view = new JabinfoView("article_page_home", this.JabinfoContext);
			view.Variable["paperList"] =paperModel.Select(index, 30);
			view.Variable["index"] = index;
			view.Variable["size"] = 30;
			view.Variable["total"] = paperModel.Count();
			view.Render();
		}

		public void add()
		{
			if (this.IsPost)
			{
				paperModel.Insert(this.Post);
			   Output("#","提交成功");
				return;
			}
			JabinfoView view = new JabinfoView("article_paper_add", this.JabinfoContext);
			view.Render();
		}

		public void edit(string paper_id)
		{
			if (paper_id == null)
			{
				paperModel.Update(this.Post);
				Output("#","修改成功");
				return;
			}
			JabinfoView view = new JabinfoView("article_paper_edit", this.JabinfoContext);
			view.Variable["paper"] = new PaperVO(paper_id);
			view.Render();
		}

		public void remove(string paper_id)
		{
			paperModel.Delete(paper_id);
		}

		private PaperModel _paperModel = null;
		private PaperModel paperModel
		{
			get
			{
				if (_paperModel == null)
					_paperModel = new PaperModel();
				return _paperModel;
			}
		}
	}
}