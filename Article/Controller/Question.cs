﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:26
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Question : Sinbo.Controller
	{
		public Question()
		{
			this.Access = Right.Administrator;
		}

		public void home(string paper_id,string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			string where=string.Format("paper_id='{0}'",paper_id);
			JabinfoView view = new JabinfoView("article_question_home", this.JabinfoContext);
			view.Variable["questionList"] =questionModel.Select(index, 30,where);
			view.Variable["index"] = index;
			view.Variable["size"] = 30;
			view.Variable["paper_id"] = paper_id;
			view.Variable["total"] = questionModel.Count(where);
			view.Render();
		}

		public void add(string paper_id)
		{
			if (this.IsPost)
			{
				questionModel.Insert(this.Post);
				Output("#","提交成功");
				return;
			}
			JabinfoView view = new JabinfoView("article_question_add", this.JabinfoContext);
			view.Variable["paper_id"]=paper_id;
			view.Render();
		}

		public void edit(string question_id)
		{
			if (question_id == null)
			{
				questionModel.Update(this.Post);
				Output("#","修改成功");
				return;
			}
			JabinfoView view = new JabinfoView("article_question_edit", this.JabinfoContext);
			view.Variable["question"] = new QuestionVO(question_id);
			view.Render();
		}

		public void remove(string question_id)
		{
			questionModel.Delete(question_id);
		}

		private QuestionModel _questionModel = null;
		private QuestionModel questionModel
		{
			get
			{
				if (_questionModel == null)
					_questionModel = new QuestionModel();
				return _questionModel;
			}
		}
	}
}