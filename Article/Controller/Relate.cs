/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:26
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;

namespace Jabinfo.Article
{
    class Relate : Sinbo.Controller
    {
        public Relate()
        {
            this.Access = Right.Administrator;
        }
        #region 关联文章列表
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView("article_relate_home", this.JabinfoContext);
            view.Variable["relateList"] =relateModel.Select(index, 30);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = relateModel.Count();
            view.Render();
        }
        #endregion

        public void add()
        {
            if (this.IsPost)
            {
                relateModel.Insert(this.Post);
                Output("#","提交成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_relate_add", this.JabinfoContext);
            view.Render();
        }

        public void edit(string article_id)
        {
            if (article_id == null)
            {
                relateModel.Update(this.Post);
                Output("#","修改成功");
                return;
            }
            JabinfoView view = new JabinfoView("article_relate_edit", this.JabinfoContext);
			view.Variable["relate"] = new RelateVO(article_id);
            view.Render();
        }

        public void remove(string article_id)
        {
            relateModel.Delete(article_id);
        }

        private RelateModel _relateModel = null;
        private RelateModel relateModel
        {
            get
            {
                if (_relateModel == null)
                    _relateModel = new RelateModel();
                return _relateModel;
            }
        }
    }
}