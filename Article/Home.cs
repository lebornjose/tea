﻿/*
 * Created by SharpDevelop.
 * User: Sinbo
 * Date: 2010/6/22
 * Time: 23:11
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;
using System.Text;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Article
{
    /// <summary>
    /// Description of Home.
    /// </summary>
    class Home : Sinbo.Controller
    {
        public Home()
        {
            this.Skin = true;
        }

        #region 内容系统首页
        public void index(string lang)
        {

            if (string.IsNullOrEmpty(lang))
                lang = "article_home_index";
            else
                lang = string.Format("article_index_{0}", lang);
            JabinfoView view = new JabinfoView(lang, this.JabinfoContext);
            if (view.Cache(lang, "home"))
                return;
            view.Serialize("home", lang);
        }
        #endregion

        #region 多语言支持首页
        public void lang()
        {
            string name = "";
            JabinfoKeyValue pack = Jabinfo.Help.Config.Get("");
            if (pack.ContainsKey(name))
            {
                this.index(pack["name"]);
            }
            else
            {
                this.index(pack["default"]);
            }
        }
        #endregion

		#region 所有文章
		public void news(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			ArticleModel articleModel = new ArticleModel();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			ArticleVO[] articles = articleModel.Select(index, 25, string.Empty, "`pubtime` desc");
			view.Variable ["articleList"] = articles;
			view.Variable ["index"] = index;
			view.Variable ["size"] = 25;
			view.Variable ["total"] = articleModel.Count (string.Empty);
			if (view.Cache ())
				return;
			view.Serialize ();
		}
		#endregion

        #region 对应分类下的文章列表
        public void category(string category_id, string start)
        {
			string where = string.Format("categorys like '%{0}%'",category_id);
            string key = string.Format("{0}-{1}", category_id, start);
            JabinfoView view = new JabinfoView("article_home_category", this.JabinfoContext);
            if (view.Cache(key, category_id))
                return;
            CategoryVO category = new CategoryVO(category_id);
            if (category.Category_id == null)
            {
                this.Jump("error");
                return;
            }
            if (category.Type == "1")
            {
                view.SetTemplate("core_home_jump");
                view.Variable["jump"] = category.Out_url;
                view.Serialize(category_id, key);
                return;
            }
            if (!string.IsNullOrEmpty(category.Extend))
            {
                view.SetTemplate(category.Extend);
            }
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            ArticleModel articleModel = new ArticleModel();
            int total = articleModel.Count(where);
            if (total == 0 && index > 0)
            {
                this.Jump("error");
                return;
            } 
            ArticleVO[] articles = articleModel.Select(index, category.Pagesize, where, "`pubtime` desc");
            view.Variable["category"] = category;
            view.Variable["articles"] = articles;
            view.Variable["index"] = index;
            view.Variable["size"] = category.Pagesize;
            view.Variable["total"] = total;
            view.Variable["start"] = index * category.Pagesize;
			view.Variable["city"] = Jabinfo.Help.Config.Get("schoolrc.city");
            view.Serialize(category_id, key);
        }
		#endregion

		#region 文章标签列表
		public void tag(string tag_id, string start)
        {
            string key = string.Format("{0}-{1}", tag_id, start);
            JabinfoView view = new JabinfoView("article_home_category", this.JabinfoContext);
            if (view.Cache(key, tag_id))
                return;

            TagsVO tag = new TagsVO(tag_id);
            if (tag.Tags_id == null)
            {
                this.Jump("error");
                return;
            }
            CategoryVO category;
            if (tag.Category_id == "0")
            {
                category = new CategoryVO();
                category.Title = "顶级分类";
                category.Category_id = "0";
                category.Parent_id = "0";
            }
            else
            {
                category = new CategoryVO(tag.Category_id);
            }
            if (!string.IsNullOrEmpty(tag.Extend))
            {
                view.SetTemplate(string.Format("article_tag_{0}", tag.Extend));
            }

            string where = string.Format("tags like '%,{0},%'", tag_id);
            int index = 0;
            if (start != null)
                index = Convert.ToInt32(start);
            ArticleModel articleModel = new ArticleModel();
            int total = articleModel.Count(where);
            if (total == 0 && index > 0)
            {
                this.Jump("error");
                return;
            }
            ArticleVO[] articles = articleModel.Select(index, tag.Pagesize, where, string.Format("article_id desc"));
            view.Variable["category"] = category;
            view.Variable["articles"] = articles;
            view.Variable["index"] = index;
            view.Variable["size"] = tag.Pagesize;
            view.Variable["total"] = total;
            view.Serialize(tag_id, key);
        }
        #endregion

        #region 获取专题文章
        public void special(string name)
        {
            name = string.Format("special_{0}", name);
            JabinfoView view = new JabinfoView(name, this.JabinfoContext);
            if (view.Cache(name, "page"))
            {
                return;
            }
            view.Serialize("home", name);
        }
        #endregion

        #region 获取文章的详细内容
        public void detail(string article_id)
        {
            if (Jabinfo.Help.Validation.Jabid(article_id))//记录点击次数
                Jabinfo.JabinfoSQL.Instance("article").Excute(CommandType.Text, string.Format("update article set `reads`=`reads`+1 where article_id='{0}'", article_id));
            else
            {
                this.Jump("error");
                return;
            }
            JabinfoView view = new JabinfoView("article_home_detail", this.JabinfoContext);
            if (view.Cache(article_id, "article"))
                return;
            ArticleVO article = new ArticleVO(article_id);
            if (article.Article_id == null)

            {
                this.Jump("error");
                return;
            }
            Article_detailVO detail = new Article_detailVO(article_id);
			view.Functions.Add ("sub", this.sub);
            view.Variable["name"] = article.Title;
            view.Variable["article"] = article;
            view.Variable["content"] = detail.Content;
            view.Serialize("article", article_id);
        }

        #endregion

		public string sub(object [] args)
		{
			string str = Convert.ToString (args [0]);
			str = str.Substring (0, 50);
			return str;
		}

        #region 文章RSS订阅发布
        /// <summary>
        /// 文章RSS订阅发布
        /// </summary>
        public void rss()
        {
            ArticleModel articleModel = new ArticleModel();
            JabinfoView view = new JabinfoView("rss", this.JabinfoContext);
            view.Variable["list"] = articleModel.Select(0, 20, string.Empty, "article_id desc");
            view.Render();
        }
        #endregion

        #region 点击热门
        /// <summary>
        /// 设置热门
        /// </summary>
        /// <param name="article_id"></param>
        public void set_hot(string article_id)
        {
            if (Jabinfo.Help.Validation.Jabid(article_id))
                Jabinfo.JabinfoSQL.Instance("article").Excute(CommandType.Text, string.Format("update article set reads=reads+1 where article_id='{0}'", article_id));
            Output("success");
        }
        #endregion

        #region 页面文章
        public void page(string page_id)
        {
            JabinfoView view = new JabinfoView("article_home_page", this.JabinfoContext);
            if (view.Cache(page_id, "page"))
                return;
            PageVO page = new PageVO(page_id);
            if (page.Page_id == null)
            {
                this.Jump("error");
                return;
            }
            CategoryVO categoryVO = new CategoryVO(page.Jtype);
            if (categoryVO.Category_id != null && !string.IsNullOrEmpty(categoryVO.Extend))
                view.SetTemplate(categoryVO.Extend);//加载分类page自定义模板（比如帮助中心首页）


            if (page.Jtype == "0")
            {
                page.Image_id = page.Page_id;
            }
            else if (Jabinfo.Help.Upload.PysPath(page_id, "jpg") == null)
            {
                page.Image_id = page.Jtype;
            }
            else
            {
                page.Image_id = page.Page_id;
            }
            view.Variable["page"] = page;
            view.Variable["page_id"] = page_id;
            view.Serialize("page", page_id);
        }
        #endregion

        #region 广告监控
        /// <summary>
        /// 广告监控
        /// </summary>
        public void click(string code_id)
        {
            string jabinfo_ads = this.JabinfoContext.Cookie.Get("jabinfo_ads");
            if (jabinfo_ads == null)//广告点击次数
            {
                this.JabinfoContext.Cookie.Add("jabinfo_ads", code_id, 60 * 24 * 30);//广告有效期30天
            }

            PositionVO position = new PositionVO(code_id);
            if (position.Position_id == null)
            {
                this.Jump("");//跳转到网站首页
            }
            else//更新点击次数
            {
                this.Jump(position.Target);//跳转到目标地址
            }
        }
        #endregion

        #region 提交留言
        public void message()
        {
            object imgcode = this.JabinfoContext.Session.Get("jabinfocode");
            if (imgcode == null || this.Post["imgcode"] == null || imgcode.ToString() != this.Post["imgcode"].ToUpper())
            {
                Output("", "请输入正确的验证码");
                return;
            }
            if (this.Post["object_id"] == null)
            {
                this.Post["object_id"] = "";
                this.Post["mtype"] = "0";
            }
            this.Post["uid"] = this.Uid;
            MessageModel messageModel = new MessageModel();
            messageModel.Insert(this.Post);
            Output("#", "提交成功");
        }
        #endregion

        #region 留言列表
        public void msg_list(string start)
        {
            int index = 0;
            if (start != null)
                index = Convert.ToInt32(start);
            MessageModel messageModel = new MessageModel();
            JabinfoView view = new JabinfoView("article_home_msg_list", this.JabinfoContext);
            view.Variable["messageList"] = messageModel.Select(index, 20, "`status`='1'");
            view.Variable["size"] = 20;
            view.Variable["total"] = messageModel.Count("`status`='1'");
            view.Variable["index"] = index;
            view.Render();
        }
        #endregion

        #region 文章搜索
        public void search(string start)
        {
			string where = string.Empty;
            if (this.IsPost)
            {
				string keyword=this.Post["title"];
                where = string.Format("title like '%{0}%'", keyword);
                this.JabinfoContext.Session.Add("article_search", where);
            }
            else
            {
                object obj = this.JabinfoContext.Session.Get("article_search");
                if (obj != null)
                    where = obj.ToString();
            }
            ArticleModel articleModel = new ArticleModel();
            int index = 0;
            if (start != null)
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView("article_home_search", this.JabinfoContext);
            view.Variable["articleList"] = articleModel.Select(index, 12, where, "article_id desc");
            view.Variable["index"] = index;
            view.Variable["size"] = 12;
            view.Variable["total"] = articleModel.Count(where);
            view.Render();
        }
        #endregion


	}
}