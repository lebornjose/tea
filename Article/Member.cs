﻿/*
 * Created by SharpDevelop.
 * User: Sinbo
 * Date: 2010/6/22
 * Time: 23:12
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Article
{
	/// <summary>
	/// Description of Member.
	/// </summary>
    class Member : Sinbo.Controller
	{
		public Member()
		{
			this.Skin=true;
			this.Access = Right.Signer;
		}
		
		public void index()
		{
			JabinfoView view = new JabinfoView("article_member_index", this.JabinfoContext);
			view.Variable["role"] = this.Role;
			view.Render();
		}
		
		#region 通用模板
		/// <summary>
		/// 通用列表
		/// </summary>
		/// <param name="category_id"></param>
		/// <param name="index"></param>
		public void list(string category_id,string start)
		{
			JabinfoKeyValue template = Jabinfo.Help.Config.Get("enum.member");
			if(template[category_id] != null)
			{
				int index=0;

				if (!string.IsNullOrEmpty (start))
					index = Convert.ToInt32 (start);
				JabinfoView view = new JabinfoView(template[category_id], this.JabinfoContext);
				view.Variable["id"] = category_id;
				view.Variable["index"] = index;
				view.Variable["total"] = "0";
				view.Render();
			}
		}
		
		public void add(string category_id)
		{
			if(this.IsPost)
			{
				this.Post["article_id"] = Jabinfo.Help.Basic.JabId;
				Article_detailVO detail = new Article_detailVO();
				detail.Insert(this.Post);
				return;
			}
			JabinfoKeyValue template = Jabinfo.Help.Config.Get("enum.member");
			if(template[string.Format("{0}_1",category_id)] != null)
			{
				JabinfoView view = new JabinfoView(template[string.Format("{0}_1",category_id)], this.JabinfoContext);
				view.Variable["id"] = category_id;
				view.Render();
			}
		}
		
		public void edit(string category_id,string article_id)
		{
			if(this.IsPost)
			{
				Article_detailVO detail = new Article_detailVO();
				detail.Insert(this.Post);
				return;
			}
			JabinfoKeyValue template = Jabinfo.Help.Config.Get("enum.member");
			if(template[string.Format("{0}_2",category_id)] != null)
			{
				ArticleVO article = new ArticleVO(article_id);
				if(article.Article_id!=null && article.Uid == this.Uid)
				{
					JabinfoView view = new JabinfoView(template[string.Format("{0}_1",category_id)], this.JabinfoContext);
					view.Variable["id"] = category_id;
					view.Variable["article"] = article;
					view.Variable["detail"] = new Article_detailVO(this.Post["article_id"]);
					view.Render();
				}
			}
		}
		
		/// <summary>
		/// 通用删除
		/// </summary>
		/// <param name="article_id"></param>
		public void remove(string article_id)
		{
			ArticleModel articleModel = new ArticleModel();
			ArticleVO artilce = new ArticleVO(article_id);
			if(artilce.Article_id!=null && artilce.Uid == this.Uid)
			{
				articleModel.Delete(article_id);
			}
		}
		#endregion
	}
}
