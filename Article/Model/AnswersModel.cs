﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class AnswersModel : Sinbo.Model<AnswersVO>
    {
        public AnswersModel()
            : base("article", "answers")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AnswersVO Get(string answers_id)
        {
            Object cache = CacheGet(answers_id);
            if (cache != null)
                return (AnswersVO)cache;
            AnswersVO answersVO = new AnswersVO(answers_id);
            if(answersVO.Answers_id==null)
                return null;
            CacheSet(answers_id, answersVO);
            return answersVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AnswersVO Get(DataRow row)
        {
            return new AnswersVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public AnswersVO[] Select()
        {
            return Collection(ISelect("answers_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public AnswersVO[] Select(int index, int length, string where)
        {
              return Collection(ISelect("answers_id").Where(where).Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
        	  return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["answers_id"] = Help.Basic.JabId;
            AnswersVO answersVO = new AnswersVO();
            answersVO.Insert(data);
            return data["answers_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            AnswersVO answersVO = new AnswersVO();
            answersVO.Update(data);
            CacheDelete(data["answers_id"]);
            return data["answers_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string answers_id)
        {
            IDelete().Where("answers_id", answers_id, DataType.Char, 24).Excute();
            CacheDelete(answers_id);
        }
    }
}