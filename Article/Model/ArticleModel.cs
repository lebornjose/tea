﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
	class ArticleModel : Sinbo.Model<ArticleVO>
	{
		public ArticleModel()
			: base("article", "article")
		{
		}
		//*
        /// <summary>
		/// 获取一条记录
		/// </summary>
		public override ArticleVO Get(string article_id)
		{
            Object cache = CacheGet(article_id);
            if (cache != null)
                return (ArticleVO)cache;
			ArticleVO articleVO = new ArticleVO(article_id);
			if(articleVO.Article_id==null)
				return null;
			CacheSet(article_id, articleVO);
			return articleVO;
		}
        //*/
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override ArticleVO Get(DataRow row)
        {
            return new ArticleVO(row);
        }
		/// <summary>
		/// 分页查询数据
		/// </summary>
		/// <param name="index">页面索引</param>
		/// <param name="length">分页长度</param>
		/// <returns></returns>
		public ArticleVO[] Select(int index, int length ,string where,string order)
		{
			return Collection(ISelect("article_id").Where(where).Order(order).Limit(index, length).All());
		}

        public string[] IDS(int index, int length, string where, string order)
        {
            return ISelect("article_id").Where(where).Order(order).Limit(index, length).Ids();
        }
		
		public ArticleVO[] Select_search(string sql)
		{
			string[] ids = this.Query.Ids(sql);
			return Collection(ids);
		}
		
		public int Count_search(string sql)
		{
			string count = (string)this.Query.Ids(sql).GetValue(0);
			return Convert.ToInt32(count);
		}
		
		public ArticleVO[] Like(int index,int length, string keyword, string where, string order)
		{
			return Collection(ISelect("article_id").Like("title", keyword, DataType.Varchar, 200).Where(where).Order(order).Limit(index, length).All());
		}

        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count()
        {
            return ISelect().Count();
        }

		/// <summary>
		/// 统计数据
		/// </summary>
		public int Count(string where)
		{
			return ISelect().Where(where).Count();
		}
		/// <summary>
		/// 插入数据
		/// </summary>
		public string Insert(JabinfoKeyValue data)
		{
			ArticleVO articleVO = new ArticleVO();
			articleVO.Insert(data);
			return data["article_id"];
		}
		/// <summary>
		/// 更新数据
		/// </summary>
		public string Update(JabinfoKeyValue data)
		{
			ArticleVO articleVO = new ArticleVO();
			articleVO.Update(data);
			CacheDelete(data["article_id"]);
			return data["article_id"];  
		}
		/// <summary>
		/// 删除数据
		/// </summary>
		public void Delete(string article_id)
		{
			IDelete().Where("article_id", article_id, DataType.Char, 24).Excute();
			CacheDelete(article_id);
		}
	}
}