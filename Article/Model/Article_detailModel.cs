﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class Article_detailModel : Sinbo.Model<Article_detailVO>
    {
        public Article_detailModel()
            : base("article", "article_detail")
        {
        }

        #region Get
        
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override Article_detailVO Get(string article_id)
        {
            Object cache = CacheGet(article_id);
            if (cache != null)
                return (Article_detailVO)cache;
            Article_detailVO article_detailVO = new Article_detailVO(article_id);
            if(article_detailVO.Article_id==null)
                return null;
            CacheSet(article_id, article_detailVO);
            return article_detailVO;
        }
       /* */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override Article_detailVO Get(DataRow row)
        {
            return new Article_detailVO(row);
        }
        #endregion
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            Article_detailVO article_detailVO = new Article_detailVO();
            article_detailVO.Insert(data);
            return data["article_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            Article_detailVO article_detailVO = new Article_detailVO();
            article_detailVO.Update(data);
            CacheDelete(data["article_id"]);
            return data["article_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string article_id)
        {
            IDelete().Where("article_id", article_id, DataType.Char, 24).Excute();
            CacheDelete(article_id);
        }
    }
}