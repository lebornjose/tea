﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class AttachModel : Sinbo.Model<AttachVO>
    {
        public AttachModel()
            : base("article", "attach")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AttachVO Get(string attach_id)
        {
            Object cache = CacheGet(attach_id);
            if (cache != null)
                return (AttachVO)cache;
            AttachVO attachVO = new AttachVO(attach_id);
            if(attachVO.Attach_id==null)
                return null;
            CacheSet(attach_id, attachVO);
            return attachVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AttachVO Get(DataRow row)
        {
            return new AttachVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public AttachVO[] Select(string object_id)
        {
            return Collection(ISelect("attach_id").Where(string.Format("object_id='{0}'", object_id)).Order("attach_id desc").All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count()
        {
            return ISelect().Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            AttachVO attachVO = new AttachVO();
            attachVO.Insert(data);
            return data["attach_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            AttachVO attachVO = new AttachVO();
            attachVO.Update(data);
            CacheDelete(data["attach_id"]);
            return data["attach_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string attach_id)
        {
            IDelete().Where("attach_id", attach_id, DataType.Char, 24).Excute();
            CacheDelete(attach_id);
        }
    }
}