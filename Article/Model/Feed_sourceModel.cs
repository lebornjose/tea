﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-3-3 下06时27分09秒
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class Feed_sourceModel : Sinbo.Model<Feed_sourceVO>
    {
		#region constructor
        public Feed_sourceModel()
            : base("article", "feed_source")
        {
        }
		#endregion

		#region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override Feed_sourceVO Get(string source_id)
        {
            Object cache = CacheGet(source_id);
            if (cache != null)
                return (Feed_sourceVO)cache;
            Feed_sourceVO feed_sourceVO = new Feed_sourceVO(source_id);
            if(feed_sourceVO.Source_id==null)
                return null;
            CacheSet(source_id, feed_sourceVO);
            return feed_sourceVO;
        }
        */
		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override Feed_sourceVO Get(DataRow row)
        {
            return new Feed_sourceVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public Feed_sourceVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("source_id").Where(where).Order(order).All()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public Feed_sourceVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("source_id").Where(where).Order(order).Limit(index, length).All()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["source_id"] = Jabinfo.Help.Basic.JabId;
            Feed_sourceVO feed_sourceVO = new Feed_sourceVO();
            feed_sourceVO.Insert(data);
            return data["source_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            Feed_sourceVO feed_sourceVO = new Feed_sourceVO();
            feed_sourceVO.Update(data);
            //CacheDelete(data["source_id"]);
            return data["source_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string source_id)
        {
            IDelete().Where("source_id", source_id, DataType.Char, 32).Excute();
            //CacheDelete(source_id);
        }
		#endregion
    }
}