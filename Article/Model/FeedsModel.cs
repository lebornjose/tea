﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-3-3 下06时28分07秒
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class FeedsModel : Sinbo.Model<FeedsVO>
    {
		#region constructor
        public FeedsModel()
            : base("article", "feeds")
        {
        }
		#endregion

		#region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override FeedsVO Get(string feeds_id)
        {
            Object cache = CacheGet(feeds_id);
            if (cache != null)
                return (FeedsVO)cache;
            FeedsVO feedsVO = new FeedsVO(feeds_id);
            if(feedsVO.Feeds_id==null)
                return null;
            CacheSet(feeds_id, feedsVO);
            return feedsVO;
        }
        */
		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override FeedsVO Get(DataRow row)
        {
            return new FeedsVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public FeedsVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("feeds_id").Where(where).Order(order).All()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public FeedsVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("feeds_id").Where(where).Order(order).Limit(index, length).All()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["feeds_id"] = Jabinfo.Help.Basic.JabId;
            FeedsVO feedsVO = new FeedsVO();
            feedsVO.Insert(data);
            return data["feeds_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            FeedsVO feedsVO = new FeedsVO();
            feedsVO.Update(data);
            //CacheDelete(data["feeds_id"]);
            return data["feeds_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string feeds_id)
        {
            IDelete().Where("feeds_id", feeds_id, DataType.Char, 32).Excute();
            //CacheDelete(feeds_id);
        }
		#endregion
    }
}