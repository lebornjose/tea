﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class LinksModel : Sinbo.Model<LinksVO>
    {
        public LinksModel()
            : base("article", "links")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override LinksVO Get(string links_id)
        {
            Object cache = CacheGet(links_id);
            if (cache != null)
                return (LinksVO)cache;
            LinksVO linksVO = new LinksVO(links_id);
            if(linksVO.Links_id==null)
                return null;
            CacheSet(links_id, linksVO);
            return linksVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override LinksVO Get(DataRow row)
        {
            return new LinksVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public LinksVO[] Select()
        {
            return Collection(ISelect("links_id").All());
        }
         
        public LinksVO[] Select(int index, int length,string where)
        {
              return Collection(ISelect("links_id").Where(where).Limit(index, length).Order("`index` desc").All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
        	  return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["links_id"] = Help.Basic.JabId;
            LinksVO linksVO = new LinksVO();
            linksVO.Insert(data);
            return data["links_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            LinksVO linksVO = new LinksVO();
            linksVO.Update(data);
            CacheDelete(data["links_id"]);
            return data["links_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string links_id)
        {
            IDelete().Where("links_id", links_id, DataType.Char, 24).Excute();
            CacheDelete(links_id);
        }
    }
}