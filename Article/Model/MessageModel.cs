﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class MessageModel : Sinbo.Model<MessageVO>
    {
        public MessageModel()
            : base("article", "message")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override MessageVO Get(string message_id)
        {
            Object cache = CacheGet(message_id);
            if (cache != null)
                return (MessageVO)cache;
            MessageVO messageVO = new MessageVO(message_id);
            if(messageVO.Message_id==null)
                return null;
            CacheSet(message_id, messageVO);
            return messageVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override MessageVO Get(DataRow row)
        {
            return new MessageVO(row);
        }
        #endregion

        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public MessageVO[] Select(int index, int length,string where)
        {
            return Collection(ISelect("message_id").Where(where).Order("index","asc").Order("message_id","desc").Order("addtime","desc").Limit(index, length).All());
        }

        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count(); 
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["message_id"] = Help.Basic.JabId;
            data["addtime"] = Jabinfo.Help.Date.Now.ToString();
            MessageVO messageVO = new MessageVO();
            messageVO.Insert(data);
            return data["message_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            MessageVO messageVO = new MessageVO();
            messageVO.Update(data);
            CacheDelete(data["message_id"]);
            return data["message_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string message_id)
        {
            IDelete().Where("message_id", message_id, DataType.Char, 24).Excute();
            CacheDelete(message_id);
        }
    }
}