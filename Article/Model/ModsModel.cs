﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2012/5/21 11:29:22
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class ModsModel : Sinbo.Model<ModsVO>
    {
		#region constructor
        public ModsModel()
            : base("core", "mods")
        {
        }
		#endregion

		#region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override ModsVO Get(string mods_id)
        {
            Object cache = CacheGet(mods_id);
            if (cache != null)
                return (ModsVO)cache;
            ModsVO modsVO = new ModsVO(mods_id);
            if(modsVO.Mods_id==null)
                return null;
            CacheSet(mods_id, modsVO);
            return modsVO;
        }
        */
		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override ModsVO Get(DataRow row)
        {
            return new ModsVO(row);
        }
		#endregion

        public ModsVO[] Select(string lan)
        {
            return Collection(
                ISelect("mods_id").Where(string.Format("lan='{0}'", lan)).Order("jindex asc").All()
            );
        }

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public ModsVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("mods_id").Where(where).Order(order).All()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public ModsVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("mods_id").Where(where).Order(order).Limit(index, length).All()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["mods_id"] = Jabinfo.Help.Basic.JabId;
            ModsVO modsVO = new ModsVO();
            modsVO.Insert(data);
            return data["mods_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            ModsVO modsVO = new ModsVO();
            modsVO.Update(data);
            return data["mods_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string mods_id)
        {
            IDelete().Where("mods_id", mods_id, DataType.Char, 32).Excute();
        }
		#endregion
    }
}