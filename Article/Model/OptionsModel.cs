﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class OptionsModel : Sinbo.Model<OptionsVO>
    {
        public OptionsModel()
            : base("article", "options")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override OptionsVO Get(string options_id)
        {
            Object cache = CacheGet(options_id);
            if (cache != null)
                return (OptionsVO)cache;
            OptionsVO optionsVO = new OptionsVO(options_id);
            if(optionsVO.Options_id==null)
                return null;
            CacheSet(options_id, optionsVO);
            return optionsVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override OptionsVO Get(DataRow row)
        {
            return new OptionsVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public OptionsVO[] Select()
        {
            return Collection(ISelect("options_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public OptionsVO[] Select(int index, int length, string where)
        {
              return Collection(ISelect("options_id").Where(where).Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
        	  return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["options_id"] = Help.Basic.JabId;
            OptionsVO optionsVO = new OptionsVO();
            optionsVO.Insert(data);
            return data["options_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            OptionsVO optionsVO = new OptionsVO();
            optionsVO.Update(data);
            CacheDelete(data["options_id"]);
            return data["options_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string options_id)
        {
            IDelete().Where("options_id", options_id, DataType.Char, 24).Excute();
            CacheDelete(options_id);
        }
    }
}