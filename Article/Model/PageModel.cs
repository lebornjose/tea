﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-18 16:43:57
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
	class PageModel : Sinbo.Model<PageVO>
	{
		public PageModel()
			: base("article", "page")
		{
		}

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override PageVO Get(string page_id)
        {
            Object cache = CacheGet(page_id);
            if (cache != null)
                return (PageVO)cache;
            PageVO pageVO = new PageVO(page_id);
            if(pageVO.Page_id==null)
                return null;
            CacheSet(page_id, pageVO);
            return pageVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override PageVO Get(DataRow row)
        {
            return new PageVO(row);
        }
        #endregion
		/// <summary>
		/// 查询数据
		/// </summary>
		public PageVO[] Select(string jtype)
		{
            return Collection(ISelect("page_id").Where(string.Format("jtype='{0}'", jtype)).Order("index", "asc").All());
		}
		/// <summary>
		/// 分页查询数据
		/// </summary>
		/// <param name="index">页面索引</param>
		/// <param name="length">分页长度</param>
		/// <returns></returns>
		public PageVO[] Select(int index, int length, string where)
		{
            return Collection(ISelect("page_id").Where(where).Order("index", "asc").Order("page_id", "desc").Limit(index, length).All());
		}
		/// <summary>
		/// 统计数据
		/// </summary>
		public int Count(string where)
		{
			return ISelect().Where(where).Count();
		}
		/// <summary>
		/// 插入数据
		/// </summary>
		public string Insert(JabinfoKeyValue data)
		{
			data["page_id"] = Help.Basic.JabId;
			PageVO pageVO = new PageVO();
			pageVO.Insert(data);
			return data["page_id"];
		}
		/// <summary>
		/// 更新数据
		/// </summary>
		public string Update(JabinfoKeyValue data)
		{
			PageVO pageVO = new PageVO();
			pageVO.Update(data);
			CacheDelete(data["page_id"]);
			return data["page_id"];
		}
		/// <summary>
		/// 删除数据
		/// </summary>
		public void Delete(string page_id)
		{
			IDelete().Where("page_id", page_id, DataType.Char, 24).Excute();
			CacheDelete(page_id);
		}
	}
}