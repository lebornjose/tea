﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class PaperModel : Sinbo.Model<PaperVO>
    {
        public PaperModel()
            : base("article", "paper")
        {
        }
        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override PaperVO Get(string paper_id)
        {
            Object cache = CacheGet(paper_id);
            if (cache != null)
                return (PaperVO)cache;
            PaperVO paperVO = new PaperVO(paper_id);
            if(paperVO.Paper_id==null)
                return null;
            CacheSet(paper_id, paperVO);
            return paperVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override PaperVO Get(DataRow row)
        {
            return new PaperVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public PaperVO[] Select()
        {
            return Collection(ISelect("paper_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public PaperVO[] Select(int index, int length)
        {
            return Collection(ISelect("paper_id").Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count()
        {
        	return ISelect().Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["paper_id"] = Help.Basic.JabId;
            data["starttime"]=Jabinfo.Help.Date.StringToDate(data["starttime"]).ToString();
            data["addtime"]=Jabinfo.Help.Date.Now.ToString();
            data["endtime"]=Jabinfo.Help.Date.StringEndDate(data["endtime"]).ToString();
            
            PaperVO paperVO = new PaperVO();
            paperVO.Insert(data);
            return data["paper_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {   
        	  data["starttime"]=Jabinfo.Help.Date.StringToDate(data["starttime"]).ToString();
        	  data["endtime"] = Jabinfo.Help.Date.StringEndDate(data["endtime"]).ToString();
        	
            PaperVO paperVO = new PaperVO();
            paperVO.Update(data);
            CacheDelete(data["paper_id"]);
            return data["paper_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string paper_id)
        {
            IDelete().Where("paper_id", paper_id, DataType.Char, 24).Excute();
            CacheDelete(paper_id);
        }
    }
}