﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class QuestionModel : Sinbo.Model<QuestionVO>
    {
        public QuestionModel()
            : base("article", "question")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override QuestionVO Get(string question_id)
        {
            Object cache = CacheGet(question_id);
            if (cache != null)
                return (QuestionVO)cache;
            QuestionVO questionVO = new QuestionVO(question_id);
            if(questionVO.Question_id==null)
                return null;
            CacheSet(question_id, questionVO);
            return questionVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override QuestionVO Get(DataRow row)
        {
            return new QuestionVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public QuestionVO[] Select()
        {
            return Collection(ISelect("question_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public QuestionVO[] Select(int index, int length,string where)
        {
              return Collection(ISelect("question_id").Where(where).Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
        	  return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["question_id"] = Help.Basic.JabId;
            QuestionVO questionVO = new QuestionVO();
            questionVO.Insert(data);
            return data["question_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            QuestionVO questionVO = new QuestionVO();
            questionVO.Update(data);
            CacheDelete(data["question_id"]);
            return data["question_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string question_id)
        {
            IDelete().Where("question_id", question_id, DataType.Char, 24).Excute();
            CacheDelete(question_id);
        }
    }
}