﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:25
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class RelateModel : Sinbo.Model<RelateVO>
    {
        public RelateModel()
            : base("article", "relate")
        {
        }
        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override RelateVO Get(string article_id)
        {
            Object cache = CacheGet(article_id);
            if (cache != null)
                return (RelateVO)cache;
            RelateVO relateVO = new RelateVO(article_id);
            if(relateVO.Article_id==null)
                return null;
            CacheSet(article_id, relateVO);
            return relateVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override RelateVO Get(DataRow row)
        {
            return new RelateVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public RelateVO[] Select()
        {
            return Collection(ISelect("article_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public RelateVO[] Select(int index, int length)
        {
            return Collection(ISelect("article_id").Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count()
        {
            return ISelect().Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["article_id"] = Help.Basic.JabId;
            RelateVO relateVO = new RelateVO();
            relateVO.Insert(data);
            return data["article_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            RelateVO relateVO = new RelateVO();
            relateVO.Update(data);
            CacheDelete(data["article_id"]);
            return data["article_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string article_id)
        {
            IDelete().Where("article_id", article_id, DataType.Char, 24).Excute();
            CacheDelete(article_id);
        }
    }
}