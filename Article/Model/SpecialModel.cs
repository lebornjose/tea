﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/7/1 10:14:55
 */
using System;
using System.Data;
using Jabinfo.Article.VO;

namespace Jabinfo.Article.Model
{
    class SpecialModel : Sinbo.Model<SpecialVO>
    {
        public SpecialModel()
            : base("article", "special")
        {
        }

        #region Get
        //*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override SpecialVO Get(string special_id)
        {
            Object cache = CacheGet(special_id);
            if (cache != null)
                return (SpecialVO)cache;
            SpecialVO specialVO = new SpecialVO(special_id);
            if(specialVO.Special_id==null)
                return null;
            CacheSet(special_id, specialVO);
            return specialVO;
        }
        
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override SpecialVO Get(DataRow row)
        {
            return new SpecialVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public ArticleVO[] Select(string object_id)
        {
            ArticleModel articleModel = new ArticleModel();
            return articleModel.Collection(ISelect("article_id").Where(string.Format("object_id='{0}'", object_id)).Ids());
        }

        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["special_id"] = Help.Basic.JabId;
            SpecialVO specialVO = new SpecialVO();
            specialVO.Insert(data);
            return data["special_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            SpecialVO specialVO = new SpecialVO();
            specialVO.Update(data);
            CacheDelete(data["special_id"]);
            return data["special_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string article_id ,string object_id)
        {
        	IDelete().Where("article_id", article_id, DataType.Char, 24).Where("object_id", object_id, DataType.Char, 24).Excute();
        }
    }
}