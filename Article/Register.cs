﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 15:49:26
 */
using System;

namespace Jabinfo.Article
{
    /// <summary>
    /// 控制器注册
    /// </summary>
    public class Register
    {
        private static Register _Instance = null;
        public static Register Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new Register();
                return _Instance;
            }
        }
        private Register() { }

        public object article { get { return new Article(); } }

        public object links { get { return new Links(); } }

        public object message { get { return new Message(); } }

        public object paper { get { return new Paper(); } }

        public object question { get { return new Question(); } }

        public object options { get { return new Options(); } }

        public object answers { get { return new Answers(); } }

        public object relate { get { return new Relate(); } }

        public object page { get { return new Page(); } }

        public object home { get { return new Home(); } }

        public object member { get { return new Member(); } }

        public object feed_source { get { return new Feed_source(); } }

        public object feeds { get { return new Feeds(); } }


        public API api { get { return API.Instance; } }
    }
}