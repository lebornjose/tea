﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 21:16:26
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// answers数据元
    /// </summary>
    [Serializable]
    public class AnswersVO
    {
        #region	属性


        private string _Answers_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Answers_id
        {
            get { return _Answers_id; }
            set { _Answers_id = value; }
        }

        private string _Uid;
        /// <summary>
        /// 用户编号
        /// </summary>
        public string Uid
        {
            get { return _Uid; }
            set { _Uid = value; }
        }

        private string _Question_id;
        /// <summary>
        /// 问题编号
        /// </summary>
        public string Question_id
        {
            get { return _Question_id; }
            set { _Question_id = value; }
        }

        private string _Options;
        /// <summary>
        /// 输入选项，逗号分隔
        /// </summary>
        public string Options
        {
            get { return _Options; }
            set { _Options = value; }
        }

        private string _Content;
        /// <summary>
        /// 回答内容
        /// </summary>
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }
        #endregion

        /// <summary>
        /// answers数据元
        /// </summary>
        public AnswersVO() { }

        /// <summary>
        /// answers数据元
        /// </summary>
        /// <param name="answers_id">数据主键</param>
        public AnswersVO(string answers_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("answers").Where("answers_id", answers_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Answers_id = dataReader["answers_id"].ToString();
                this.Uid = dataReader["uid"].ToString();
                this.Question_id = dataReader["question_id"].ToString();
                this.Options = dataReader["options"].ToString();
                this.Content = dataReader["content"].ToString();
            }
        }
        /// <summary>
        /// answers
        /// </summary>
        /// <param name="answers_id">Primary Key</param>
        public AnswersVO(DataRow dataReader)
        {
            this.Answers_id = dataReader["answers_id"] as string;
            this.Uid = dataReader["uid"] as string;
            this.Question_id = dataReader["question_id"] as string;
            this.Options = dataReader["options"] as string;
            this.Content = dataReader["content"] as string;
        }
        /// <summary>
        /// answers插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("answers").
                        Value("answers_id", data["answers_id"], DataType.Char, 24).
                        Value("uid", data["uid"], DataType.Char, 24).
                        Value("question_id", data["question_id"], DataType.Char, 24).
                        Value("options", data["options"], DataType.Varchar, 500).
                        Value("content", data["content"], DataType.Varchar, 1000).
                        Excute();
        }

        /// <summary>
        /// answers更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("answers").
                        Set("uid", data["uid"], DataType.Char, 24).
                        Set("question_id", data["question_id"], DataType.Char, 24).
                        Set("options", data["options"], DataType.Varchar, 500).
                        Set("content", data["content"], DataType.Varchar, 1000).
                                    Where("answers_id", data["answers_id"], DataType.Char, 24).
                        Excute();
        }
    }
}