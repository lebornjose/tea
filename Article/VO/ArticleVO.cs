/*
* Created:  by JabinfoCoder
* Contact:  http://www.jabinfo.com
* Author :  Sinbo
* Date   :  2011-11-24 08:52:18
*/
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// article数据元
    ///	</summary>
    [Serializable]
    public class ArticleVO
    {
        #region	属性

        private string  _Article_id;
        /// <summary>
        /// 编号
        ///</summary>
        public string  Article_id
        {
            get{ return _Article_id; }
            set{ _Article_id = value; }
        }

        private string  _Title;
        /// <summary>
        /// 文章标题
        ///</summary>
        public string  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }

        private string  _Caption;
        /// <summary>
        /// 副标题
        ///</summary>
        public string  Caption
        {
            get{ return _Caption; }
            set{ _Caption = value; }
        }

        private string  _Jtype;
        /// <summary>
        /// 文章类型,0草稿,1普通,2外连接，3图片集，6专题
        ///</summary>
        public string  Jtype
        {
            get{ return _Jtype; }
            set{ _Jtype = value; }
        }

        private string  _Author;
        /// <summary>
        /// 作者
        ///</summary>
        public string  Author
        {
            get{ return _Author; }
            set{ _Author = value; }
        }

        private string  _Model;
        /// <summary>
        /// 参数说明
        ///</summary>
        public string  Model
        {
            get{ return _Model; }
            set{ _Model = value; }
        }

        private int  _Pubtime;
        /// <summary>
        /// 发布时间
        ///</summary>
        public int  Pubtime
        {
            get{ return _Pubtime; }
            set{ _Pubtime = value; }
        }

        private string  _Summary;
        /// <summary>
        /// 文章摘要
        ///</summary>
        public string  Summary
        {
            get{ return _Summary; }
            set{ _Summary = value; }
        }

        private int  _Reads;
        /// <summary>
        /// 阅读次数
        ///</summary>
        public int  Reads
        {
            get{ return _Reads; }
            set{ _Reads = value; }
        }

        private string  _Ispost;
        /// <summary>
        /// 是否允许评论,0否，1是
        ///</summary>
        public string  Ispost
        {
            get{ return _Ispost; }
            set{ _Ispost = value; }
        }

        private string  _Attach;
        /// <summary>
        /// 附件类型,0无,1图片，2下载，3图片集，4下载集
        ///</summary>
        public string  Attach
        {
            get{ return _Attach; }
            set{ _Attach = value; }
        }

        private string  _Come;
        /// <summary>
        /// 文章来源
        ///</summary>
        public string  Come
        {
            get{ return _Come; }
            set{ _Come = value; }
        }

        private string  _Come_url;
        /// <summary>
        /// 来源地址
        ///</summary>
        public string  Come_url
        {
            get{ return _Come_url; }
            set{ _Come_url = value; }
        }

        private string  _Keyword;
        /// <summary>
        /// 文章关键字
        ///</summary>
        public string  Keyword
        {
            get{ return _Keyword; }
            set{ _Keyword = value; }
        }

        private string  _Out_url;
        /// <summary>
        /// 外链接地址
        ///</summary>
        public string  Out_url
        {
            get{ return _Out_url; }
            set{ _Out_url = value; }
        }

        private string  _Iscash;
        /// <summary>
        /// 是否是草稿,0否，1是
        ///</summary>
        public string  Iscash
        {
            get{ return _Iscash; }
            set{ _Iscash = value; }
        }

        private string  _Uid;
        /// <summary>
        /// 发布人
        ///</summary>
        public string  Uid
        {
            get{ return _Uid; }
            set{ _Uid = value; }
        }

        private string  _Tags;
        /// <summary>
        /// 标签序列
        ///</summary>
        public string  Tags
        {
            get{ return _Tags; }
            set{ _Tags = value; }
        }

        private string  _Categorys;
        /// <summary>
        /// 分类序列
        ///</summary>
        public string  Categorys
        {
            get{ return _Categorys; }
            set{ _Categorys = value; }
        }

        private string  _Category_id;
        /// <summary>
        /// 分类编号
        ///</summary>
        public string  Category_id
        {
            get{ return _Category_id; }
            set{ _Category_id = value; }
        }
        #endregion

        /// <summary>
        /// article数据元
        /// </summary>
        public ArticleVO(){}

        /// <summary>
        /// article数据元
        /// </summary>
        /// <param name="article_id">数据主键</param>
        public ArticleVO(string article_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("article").Where("article_id",article_id, DataType.Char, 24).Row();
            if(dataReader != null)
            {
            this.Article_id = dataReader["article_id"].ToString();
            this.Title = dataReader["title"].ToString();
            this.Caption = dataReader["caption"].ToString();
            this.Jtype = dataReader["jtype"].ToString();
            this.Author = dataReader["author"].ToString();
            this.Model = dataReader["model"].ToString();
            this.Pubtime = Convert.ToInt32(dataReader["pubtime"]);
            this.Summary = dataReader["summary"].ToString();
            this.Reads = Convert.ToInt32(dataReader["reads"]);
            this.Ispost = dataReader["ispost"].ToString();
            this.Attach = dataReader["attach"].ToString();
            this.Come = dataReader["come"].ToString();
            this.Come_url = dataReader["come_url"].ToString();
            this.Keyword = dataReader["keyword"].ToString();
            this.Out_url = dataReader["out_url"].ToString();
            this.Iscash = dataReader["iscash"].ToString();
            this.Uid = dataReader["uid"].ToString();
            this.Tags = dataReader["tags"].ToString();
            this.Categorys = dataReader["categorys"].ToString();
            this.Category_id = dataReader["category_id"].ToString();
            }
        }
        /// <summary>
        /// ${table.Name}
        /// </summary>
        /// <param name="${table.Primary}">Primary Key</param>
        public ArticleVO(DataRow dataReader)
        {
            this.Article_id = dataReader["article_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Caption = dataReader["caption"] as string;
            this.Jtype = dataReader["jtype"] as string;
            this.Author = dataReader["author"] as string;
            this.Model = dataReader["model"] as string;
            this.Pubtime = Convert.ToInt32(dataReader["pubtime"]);
            this.Summary = dataReader["summary"] as string;
            this.Reads = Convert.ToInt32(dataReader["reads"]);
            this.Ispost = dataReader["ispost"] as string;
            this.Attach = dataReader["attach"] as string;
            this.Come = dataReader["come"] as string;
            this.Come_url = dataReader["come_url"] as string;
            this.Keyword = dataReader["keyword"] as string;
            this.Out_url = dataReader["out_url"] as string;
            this.Iscash = dataReader["iscash"] as string;
            this.Uid = dataReader["uid"] as string;
            this.Tags = dataReader["tags"] as string;
            this.Categorys = dataReader["categorys"] as string;
            this.Category_id = dataReader["category_id"] as string;
        }
        /// <summary>
        /// article插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("article").
            Value("article_id", data["article_id"], DataType.Char, 24).
            Value("title", data["title"], DataType.Varchar, 200).
            Value("caption", data["caption"], DataType.Varchar, 200).
            Value("jtype", data["jtype"], DataType.Char, 1).
            Value("author", data["author"], DataType.Varchar, 50).
            Value("model", data["model"], DataType.Varchar, 200).
            Value("pubtime", data["pubtime"], DataType.Int).
            Value("summary", data["summary"], DataType.Varchar, 600).
            Value("reads", data["reads"], DataType.Int).
            Value("ispost", data["ispost"], DataType.Char, 1).
            Value("attach", data["attach"], DataType.Char, 1).
            Value("come", data["come"], DataType.Varchar, 50).
            Value("come_url", data["come_url"], DataType.Varchar, 200).
            Value("keyword", data["keyword"], DataType.Varchar, 200).
            Value("out_url", data["out_url"], DataType.Varchar, 200).
            Value("iscash", data["iscash"], DataType.Char, 1).
            Value("uid", data["uid"], DataType.Char, 24).
            Value("tags", data["tags"], DataType.Varchar, 200).
            Value("categorys", data["categorys"], DataType.Varchar, 200).
            Value("category_id", data["category_id"], DataType.Char, 24).
            Excute();
        }

        /// <summary>
        /// article更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("article").
            Set("title", data["title"], DataType.Varchar, 200).
            Set("caption", data["caption"], DataType.Varchar, 200).
            Set("jtype", data["jtype"], DataType.Char, 1).
            Set("author", data["author"], DataType.Varchar, 50).
            Set("model", data["model"], DataType.Varchar, 200).
            Set("pubtime", data["pubtime"], DataType.Int).
            Set("summary", data["summary"], DataType.Varchar, 600).
            Set("reads", data["reads"], DataType.Int).
            Set("ispost", data["ispost"], DataType.Char, 1).
            Set("attach", data["attach"], DataType.Char, 1).
            Set("come", data["come"], DataType.Varchar, 50).
            Set("come_url", data["come_url"], DataType.Varchar, 200).
            Set("keyword", data["keyword"], DataType.Varchar, 200).
            Set("out_url", data["out_url"], DataType.Varchar, 200).
            Set("iscash", data["iscash"], DataType.Char, 1).
            Set("uid", data["uid"], DataType.Char, 24).
            Set("tags", data["tags"], DataType.Varchar, 200).
            Set("categorys", data["categorys"], DataType.Varchar, 200).
            Set("category_id", data["category_id"], DataType.Char, 24).
            Where("article_id", data["article_id"], DataType.Char, 24).
            Excute();
        }
    }
}