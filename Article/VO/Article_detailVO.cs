﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/6/23 13:13:11
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// article_detail数据元
    /// </summary>
    [Serializable]
    public class Article_detailVO
    {
        #region	属性


        private string _Article_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Article_id
        {
            get { return _Article_id; }
            set { _Article_id = value; }
        }

        private string _Content;
        /// <summary>
        /// 文章内容
        /// </summary>
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private string _Tags;
        /// <summary>
        /// 所有标签
        /// </summary>
        public string Tags
        {
            get { return _Tags; }
            set { _Tags = value; }
        }

        private string _Categorys;
        /// <summary>
        /// 所有分类
        /// </summary>
        public string Categorys
        {
            get { return _Categorys; }
            set { _Categorys = value; }
        }
        #endregion

        /// <summary>
        /// article_detail数据元
        /// </summary>
        public Article_detailVO() { }

        /// <summary>
        /// article_detail数据元
        /// </summary>
        /// <param name="article_id">数据主键</param>
        public Article_detailVO(string article_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("article_detail").Where("article_id", article_id, DataType.Char).Row();

            if (dataReader != null)
            {
                this.Article_id = dataReader["article_id"].ToString();
                this.Content = dataReader["content"].ToString();
                this.Tags = dataReader["tags"].ToString();
                this.Categorys = dataReader["categorys"].ToString();
            }
        }
        /// <summary>
        /// article_detail
        /// </summary>
        /// <param name="article_id">Primary Key</param>
        public Article_detailVO(DataRow dataReader)
        {
            this.Article_id = dataReader["article_id"] as string;
            this.Content = dataReader["content"] as string;
            this.Tags = dataReader["tags"] as string;
            this.Categorys = dataReader["categorys"] as string;
        }
        /// <summary>
        /// article_detail插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("article_detail").
                        Value("article_id", data["article_id"], DataType.Char, 24).
                        Value("content", data["content"], DataType.Text).
                        Value("tags", data["tags"], DataType.Varchar, 500).
                        Value("categorys", data["categorys"], DataType.Varchar, 500).
                        Excute();
        }

        /// <summary>
        /// article_detail更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("article_detail").
                        Set("content", data["content"], DataType.Text).
                        Set("tags", data["tags"], DataType.Varchar, 500).
                        Set("categorys", data["categorys"], DataType.Varchar, 500).
                                    Where("article_id", data["article_id"], DataType.Char, 24).
                        Excute();
        }
    }
}