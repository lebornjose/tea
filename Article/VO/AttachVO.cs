﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/10/17 19:47:44
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// attach数据元
    /// </summary>
    [Serializable]
    public class AttachVO
    {
        #region	属性


        private string _Attach_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Attach_id
        {
            get { return _Attach_id; }
            set { _Attach_id = value; }
        }

        private string _Object_id;
        /// <summary>
        /// 父编号
        /// </summary>
        public string Object_id
        {
            get { return _Object_id; }
            set { _Object_id = value; }
        }

        private string _Ext;
        /// <summary>
        /// 后缀名
        /// </summary>
        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }

        private string _Describe;
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe
        {
            get { return _Describe; }
            set { _Describe = value; }
        }

        private string _Title;
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        #endregion

        /// <summary>
        /// attach数据元
        /// </summary>
        public AttachVO() { }

        /// <summary>
        /// attach数据元
        /// </summary>
        /// <param name="attach_id">数据主键</param>
        public AttachVO(string attach_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("attach").Where("attach_id", attach_id, DataType.Char).Row();

            if (dataReader != null)
            {
                this.Attach_id = dataReader["attach_id"].ToString();
                this.Object_id = dataReader["object_id"].ToString();
                this.Ext = dataReader["ext"].ToString();
                this.Describe = dataReader["describe"].ToString();
                this.Title = dataReader["title"].ToString();
            }
        }
        /// <summary>
        /// attach
        /// </summary>
        /// <param name="attach_id">Primary Key</param>
        public AttachVO(DataRow dataReader)
        {
            this.Attach_id = dataReader["attach_id"] as string;
            this.Object_id = dataReader["object_id"] as string;
            this.Ext = dataReader["ext"] as string;
            this.Describe = dataReader["describe"] as string;
        }
        /// <summary>
        /// attach插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        internal int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("attach").
                        Value("attach_id", data["attach_id"], DataType.Char, 24).
                        Value("object_id", data["object_id"], DataType.Char, 24).
                        Value("ext", data["ext"], DataType.Varchar, 10).
                        Value("describe", data["describe"], DataType.Varchar, 500).
                        Value("title", data["title"], DataType.Varchar, 100).
                        Excute();
        }

        /// <summary>
        /// attach更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("attach").
                        Set("object_id", data["object_id"], DataType.Char, 24).
                        Set("ext", data["ext"], DataType.Varchar, 10).
                        Set("describe", data["describe"], DataType.Varchar, 500).
                        Set("title", data["title"], DataType.Varchar, 100).
                                    Where("attach_id", data["attach_id"], DataType.Char, 24).
                        Excute();
        }
    }
}