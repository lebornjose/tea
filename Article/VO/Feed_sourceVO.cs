﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-3-3 下10时02分25秒
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// feed_sourceVO
    ///	</summary>
    [Serializable]
    public class Feed_sourceVO
    {
        #region	Property

        private String  _Source_id;
        /// <summary>
        /// 源编号
        ///</summary>
        public String  Source_id
        {
            get{ return _Source_id; }
            set{ _Source_id = value; }
        }

        private String  _Title;
        /// <summary>
        /// 网站名称
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }

        private String  _Url;
        /// <summary>
        /// 网站首页地址
        ///</summary>
        public String  Url
        {
            get{ return _Url; }
            set{ _Url = value; }
        }

        private String  _Source;
        /// <summary>
        /// 源地址
        ///</summary>
        public String  Source
        {
            get{ return _Source; }
            set{ _Source = value; }
        }

        private String  _Category_id;
        /// <summary>
        /// 分类编号
        ///</summary>
        public String  Category_id
        {
            get{ return _Category_id; }
            set{ _Category_id = value; }
        }

        private Int32  _Jindex;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Jindex
        {
            get{ return _Jindex; }
            set{ _Jindex = value; }
        }

        #endregion

		#region Constructor
        /// <summary>
        /// feed_source
        /// </summary>
        public Feed_sourceVO(){}
	
        /// <summary>
        /// feed_source
        /// </summary>
        /// <param name="source_id">Primary Key</param>
        public Feed_sourceVO(string source_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("feed_source").Where("source_id",source_id,DataType.Char, 24).Row();
            if(dataReader != null)
            {
				this.Source_id = dataReader["source_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Url = dataReader["url"] as string;
				this.Source = dataReader["source"] as string;
				this.Category_id = dataReader["category_id"] as string;
				this.Jindex = Convert.ToInt32(dataReader["jindex"]);
            }
        }

		/// <summary>
        /// feed_source
        /// </summary>
        /// <param name="source_id">Primary Key</param>
        public Feed_sourceVO(DataRow dataReader)
        {
			this.Source_id = dataReader["source_id"] as string;
			this.Title = dataReader["title"] as string;
			this.Url = dataReader["url"] as string;
			this.Source = dataReader["source"] as string;
			this.Category_id = dataReader["category_id"] as string;
			this.Jindex = Convert.ToInt32(dataReader["jindex"]);
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// feed_sourceInsert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("feed_source").
            Value("source_id", data["source_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 50).
            Value("url", data["url"],DataType.Varchar, 50).
            Value("source", data["source"],DataType.Varchar, 200).
            Value("category_id", data["category_id"],DataType.Char, 24).
            Value("jindex", data["jindex"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// feed_sourceUpdate
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("feed_source").
            Set("title", data["title"],DataType.Varchar, 50).
            Set("url", data["url"],DataType.Varchar, 50).
            Set("source", data["source"],DataType.Varchar, 200).
            Set("category_id", data["category_id"],DataType.Char, 24).
            Set("jindex", data["jindex"],DataType.Int).
            Where("source_id", data["source_id"],DataType.Char, 24).
            Excute();
        }
		#endregion
    }
}