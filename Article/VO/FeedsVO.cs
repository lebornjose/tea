﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-3-3 下06时28分07秒
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// feedsVO
    ///	</summary>
    [Serializable]
    public class FeedsVO
    {
        #region	Property

        private String  _Feeds_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Feeds_id
        {
            get{ return _Feeds_id; }
            set{ _Feeds_id = value; }
        }

        private String  _Category_id;
        /// <summary>
        /// 分类编号
        ///</summary>
        public String  Category_id
        {
            get{ return _Category_id; }
            set{ _Category_id = value; }
        }

        private String  _Source_id;
        /// <summary>
        /// 所属源编号
        ///</summary>
        public String  Source_id
        {
            get{ return _Source_id; }
            set{ _Source_id = value; }
        }

        private String  _Title;
        /// <summary>
        /// 标题
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }

        private String  _Describe;
        /// <summary>
        /// 描述
        ///</summary>
        public String  Describe
        {
            get{ return _Describe; }
            set{ _Describe = value; }
        }

        private String  _Link;
        /// <summary>
        /// 链接地址
        ///</summary>
        public String  Link
        {
            get{ return _Link; }
            set{ _Link = value; }
        }

        private Int32  _Addtime;
        /// <summary>
        /// 添加时间
        ///</summary>
        public Int32  Addtime
        {
            get{ return _Addtime; }
            set{ _Addtime = value; }
        }

        private Int32  _Pubdate;
        /// <summary>
        /// 更新时间
        ///</summary>
        public Int32  Pubdate
        {
            get{ return _Pubdate; }
            set{ _Pubdate = value; }
        }

        #endregion

		#region Constructor
        /// <summary>
        /// feeds
        /// </summary>
        public FeedsVO(){}
	
        /// <summary>
        /// feeds
        /// </summary>
        /// <param name="feeds_id">Primary Key</param>
        public FeedsVO(string feeds_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("feeds").Where("feeds_id",feeds_id,DataType.Char, 24).Row();
            if(dataReader != null)
            {
				this.Feeds_id = dataReader["feeds_id"] as string;
				this.Category_id = dataReader["category_id"] as string;
				this.Source_id = dataReader["source_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Describe = dataReader["describe"] as string;
				this.Link = dataReader["link"] as string;
				this.Addtime = Convert.ToInt32(dataReader["addtime"]);
				this.Pubdate = Convert.ToInt32(dataReader["pubdate"]);
            }
        }

		/// <summary>
        /// feeds
        /// </summary>
        /// <param name="feeds_id">Primary Key</param>
        public FeedsVO(DataRow dataReader)
        {
			this.Feeds_id = dataReader["feeds_id"] as string;
			this.Category_id = dataReader["category_id"] as string;
			this.Source_id = dataReader["source_id"] as string;
			this.Title = dataReader["title"] as string;
			this.Describe = dataReader["describe"] as string;
			this.Link = dataReader["link"] as string;
			this.Addtime = Convert.ToInt32(dataReader["addtime"]);
			this.Pubdate = Convert.ToInt32(dataReader["pubdate"]);
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// feedsInsert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("feeds").
            Value("feeds_id", data["feeds_id"],DataType.Char, 24).
            Value("category_id", data["category_id"],DataType.Char, 24).
            Value("source_id", data["source_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 100).
            Value("describe", data["describe"],DataType.Varchar, 1000).
            Value("link", data["link"],DataType.Varchar, 200).
            Value("addtime", data["addtime"],DataType.Int).
            Value("pubdate", data["pubdate"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// feedsUpdate
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("feeds").
            Set("category_id", data["category_id"],DataType.Char, 24).
            Set("source_id", data["source_id"],DataType.Char, 24).
            Set("title", data["title"],DataType.Varchar, 100).
            Set("describe", data["describe"],DataType.Varchar, 1000).
            Set("link", data["link"],DataType.Varchar, 200).
            Set("addtime", data["addtime"],DataType.Int).
            Set("pubdate", data["pubdate"],DataType.Int).
            Where("feeds_id", data["feeds_id"],DataType.Char, 24).
            Excute();
        }
		#endregion
    }
}