﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/6/20 18:40:33
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// links数据元
    /// </summary>
    [Serializable]
    public class LinksVO
    {
        #region	属性


        private string _Links_id;
        /// <summary>
        /// 友情链接
        /// </summary>
        public string Links_id
        {
            get { return _Links_id; }
            set { _Links_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 链接名称
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Category;
        /// <summary>
        /// 分类,见配置文件
        /// </summary>
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        private int _Index;
        /// <summary>
        /// 默认排序
        /// </summary>
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        private string _Url;
        /// <summary>
        /// 地址
        /// </summary>
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        private string _Picture;
        /// <summary>
        /// 图片
        /// </summary>
        public string Picture
        {
            get { return _Picture; }
            set { _Picture = value; }
        }
        #endregion

        /// <summary>
        /// links数据元
        /// </summary>
        public LinksVO() { }

        /// <summary>
        /// links数据元
        /// </summary>
        /// <param name="links_id">数据主键</param>
        public LinksVO(string links_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("links").Where("links_id", links_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Links_id = dataReader["links_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Category = dataReader["category"].ToString();
                this.Index = Convert.ToInt32(dataReader["index"]);
                this.Url = dataReader["url"].ToString();
                this.Picture = dataReader["picture"].ToString();
            }
        }
        /// <summary>
        /// links
        /// </summary>
        /// <param name="links_id">Primary Key</param>
        public LinksVO(DataRow dataReader)
        {
            this.Links_id = dataReader["links_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Category = dataReader["category"] as string;
            this.Index = Convert.ToInt32(dataReader["index"]);
            this.Url = dataReader["url"] as string;
            this.Picture = dataReader["picture"] as string;
        }
        /// <summary>
        /// links插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("links").
                        Value("links_id", data["links_id"], DataType.Char, 24).
                        Value("title", data["title"], DataType.Varchar, 200).
                        Value("category", data["category"], DataType.Char, 24).
                        Value("index", data["index"], DataType.Int).
                        Value("url", data["url"], DataType.Varchar, 200).
                        Value("picture", data["picture"], DataType.Varchar, 200).
                        Excute();
        }

        /// <summary>
        /// links更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("links").
                        Set("title", data["title"], DataType.Varchar, 200).
                        Set("category", data["category"], DataType.Char, 24).
                        Set("index", data["index"], DataType.Int).
                        Set("url", data["url"], DataType.Varchar, 200).
                        Set("picture", data["picture"], DataType.Varchar, 200).
                                    Where("links_id", data["links_id"], DataType.Char, 24).
                        Excute();
        }
    }
}