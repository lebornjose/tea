/*
* Created:  by JabinfoCoder
* Contact:  http://www.jabinfo.com
* Author :  Sinbo
* Date   :  2011-11-28 17:17:13
*/
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// message数据元
    ///	</summary>
    [Serializable]
    public class MessageVO
    {
        #region	属性

        private string  _Message_id;
        /// <summary>
        /// 编号
        ///</summary>
        public string  Message_id
        {
            get{ return _Message_id; }
            set{ _Message_id = value; }
        }

        private string  _Nick;
        /// <summary>
        /// 昵称
        ///</summary>
        public string  Nick
        {
            get{ return _Nick; }
            set{ _Nick = value; }
        }

        private string  _Email;
        /// <summary>
        /// 邮箱
        ///</summary>
        public string  Email
        {
            get{ return _Email; }
            set{ _Email = value; }
        }

        private string  _Telphone;
        /// <summary>
        /// 电话
        ///</summary>
        public string  Telphone
        {
            get{ return _Telphone; }
            set{ _Telphone = value; }
        }

        private string  _Contact;
        /// <summary>
        /// 联系方式
        ///</summary>
        public string  Contact
        {
            get{ return _Contact; }
            set{ _Contact = value; }
        }

        private string  _Content;
        /// <summary>
        /// 留言内容
        ///</summary>
        public string  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }

        private string  _Reply;
        /// <summary>
        /// 回复内容
        ///</summary>
        public string  Reply
        {
            get{ return _Reply; }
            set{ _Reply = value; }
        }

        private int  _Addtime;
        /// <summary>
        /// 添加时间
        ///</summary>
        public int  Addtime
        {
            get{ return _Addtime; }
            set{ _Addtime = value; }
        }

        private string  _Uid;
        /// <summary>
        /// 用户编号
        ///</summary>
        public string  Uid
        {
            get{ return _Uid; }
            set{ _Uid = value; }
        }

        private string  _Object_id;
        /// <summary>
        /// 对象编号
        ///</summary>
        public string  Object_id
        {
            get{ return _Object_id; }
            set{ _Object_id = value; }
        }

        private string  _Rank;
        /// <summary>
        /// 置首，0否，1是
        ///</summary>
        public string  Rank
        {
            get{ return _Rank; }
            set{ _Rank = value; }
        }

        private int  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public int  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }

        private string  _Mtype;
        /// <summary>
        /// 留言对象类型,见message配置,0留言薄,1文章,2产品,3页面
        ///</summary>
        public string  Mtype
        {
            get{ return _Mtype; }
            set{ _Mtype = value; }
        }

        private string  _Status;
        /// <summary>
        /// 当前状态0未审核，1通过
        ///</summary>
        public string  Status
        {
            get{ return _Status; }
            set{ _Status = value; }
        }

        private string  _Title;
        /// <summary>
        /// 留言标题
        ///</summary>
        public string  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }

        private int  _Useful;
        /// <summary>
        /// 有用
        ///</summary>
        public int  Useful
        {
            get{ return _Useful; }
            set{ _Useful = value; }
        }

        private int  _Nouse;
        /// <summary>
        /// 无用
        ///</summary>
        public int  Nouse
        {
            get{ return _Nouse; }
            set{ _Nouse = value; }
        }

        private string  _Parent_id;
        /// <summary>
        /// 引用编号
        ///</summary>
        public string  Parent_id
        {
            get{ return _Parent_id; }
            set{ _Parent_id = value; }
        }

        private int  _Star1;
        /// <summary>
        /// 星级1
        ///</summary>
        public int  Star1
        {
            get{ return _Star1; }
            set{ _Star1 = value; }
        }

        private int  _Star2;
        /// <summary>
        /// 星级2
        ///</summary>
        public int  Star2
        {
            get{ return _Star2; }
            set{ _Star2 = value; }
        }

        private int  _Star3;
        /// <summary>
        /// 星级3
        ///</summary>
        public int  Star3
        {
            get{ return _Star3; }
            set{ _Star3 = value; }
        }
        #endregion

        /// <summary>
        /// message数据元
        /// </summary>
        public MessageVO(){}

        /// <summary>
        /// message数据元
        /// </summary>
        /// <param name="message_id">数据主键</param>
        public MessageVO(string message_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("message").Where("message_id",message_id, DataType.Char, 24).Row();
            if(dataReader != null)
            {
            this.Message_id = dataReader["message_id"].ToString();
            this.Nick = dataReader["nick"].ToString();
            this.Email = dataReader["email"].ToString();
            this.Telphone = dataReader["telphone"].ToString();
            this.Contact = dataReader["contact"].ToString();
            this.Content = dataReader["content"].ToString();
            this.Reply = dataReader["reply"].ToString();
            this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            this.Uid = dataReader["uid"].ToString();
            this.Object_id = dataReader["object_id"].ToString();
            this.Rank = dataReader["rank"].ToString();
            this.Index = Convert.ToInt32(dataReader["index"]);
            this.Mtype = dataReader["mtype"].ToString();
            this.Status = dataReader["status"].ToString();
            this.Title = dataReader["title"].ToString();
            this.Useful = Convert.ToInt32(dataReader["useful"]);
            this.Nouse = Convert.ToInt32(dataReader["nouse"]);
            this.Parent_id = dataReader["parent_id"].ToString();
            this.Star1 = Convert.ToInt32(dataReader["star1"]);
            this.Star2 = Convert.ToInt32(dataReader["star2"]);
            this.Star3 = Convert.ToInt32(dataReader["star3"]);
            }
        }
        /// <summary>
        /// message
        /// </summary>
        /// <param name="message_id">Primary Key</param>
        public MessageVO(DataRow dataReader)
        {
            this.Message_id = dataReader["message_id"] as string;
            this.Nick = dataReader["nick"] as string;
            this.Email = dataReader["email"] as string;
            this.Telphone = dataReader["telphone"] as string;
            this.Contact = dataReader["contact"] as string;
            this.Content = dataReader["content"] as string;
            this.Reply = dataReader["reply"] as string;
            this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            this.Uid = dataReader["uid"] as string;
            this.Object_id = dataReader["object_id"] as string;
            this.Rank = dataReader["rank"] as string;
            this.Index = Convert.ToInt32(dataReader["index"]);
            this.Mtype = dataReader["mtype"] as string;
            this.Status = dataReader["status"] as string;
            this.Title = dataReader["title"] as string;
            this.Useful = Convert.ToInt32(dataReader["useful"]);
            this.Nouse = Convert.ToInt32(dataReader["nouse"]);
            this.Parent_id = dataReader["parent_id"] as string;
            this.Star1 = Convert.ToInt32(dataReader["star1"]);
            this.Star2 = Convert.ToInt32(dataReader["star2"]);
            this.Star3 = Convert.ToInt32(dataReader["star3"]);
        }
        /// <summary>
        /// message插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("message").
            Value("message_id", data["message_id"], DataType.Char, 24).
            Value("nick", data["nick"], DataType.Varchar, 20).
            Value("email", data["email"], DataType.Varchar, 50).
            Value("telphone", data["telphone"], DataType.Varchar, 50).
            Value("contact", data["contact"], DataType.Varchar, 100).
            Value("content", data["content"], DataType.Varchar, 500).
            Value("reply", data["reply"], DataType.Varchar, 500).
            Value("addtime", data["addtime"], DataType.Int).
            Value("uid", data["uid"], DataType.Char, 24).
            Value("object_id", data["object_id"], DataType.Char, 24).
            Value("rank", data["rank"], DataType.Char, 1).
            Value("index", data["index"], DataType.Int).
            Value("mtype", data["mtype"], DataType.Char, 1).
            Value("status", data["status"], DataType.Char, 1).
            Value("title", data["title"], DataType.Varchar, 100).
            Value("useful", data["useful"], DataType.Int).
            Value("nouse", data["nouse"], DataType.Int).
            Value("parent_id", data["parent_id"], DataType.Char, 24).
            Value("star1", data["star1"], DataType.Int).
            Value("star2", data["star2"], DataType.Int).
            Value("star3", data["star3"], DataType.Int).
            Excute();
        }

        /// <summary>
        /// message更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("message").
            Set("nick", data["nick"], DataType.Varchar, 20).
            Set("email", data["email"], DataType.Varchar, 50).
            Set("telphone", data["telphone"], DataType.Varchar, 50).
            Set("contact", data["contact"], DataType.Varchar, 100).
            Set("content", data["content"], DataType.Varchar, 500).
            Set("reply", data["reply"], DataType.Varchar, 500).
            Set("addtime", data["addtime"], DataType.Int).
            Set("uid", data["uid"], DataType.Char, 24).
            Set("object_id", data["object_id"], DataType.Char, 24).
            Set("rank", data["rank"], DataType.Char, 1).
            Set("index", data["index"], DataType.Int).
            Set("mtype", data["mtype"], DataType.Char, 1).
            Set("status", data["status"], DataType.Char, 1).
            Set("title", data["title"], DataType.Varchar, 100).
            Set("useful", data["useful"], DataType.Int).
            Set("nouse", data["nouse"], DataType.Int).
            Set("parent_id", data["parent_id"], DataType.Char, 24).
            Set("star1", data["star1"], DataType.Int).
            Set("star2", data["star2"], DataType.Int).
            Set("star3", data["star3"], DataType.Int).
            Where("message_id", data["message_id"], DataType.Char, 24).
            Excute();
        }
    }
}