/*
* Created:  by JabinfoCoder
* Contact:  http://www.jabinfo.com
* Author :  Sinbo
* Date   :  2011-12-17 15:57:49
*/
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// mods数据元
    ///	</summary>
    [Serializable]
    public class ModsVO
    {
        #region	属性

        private string  _Mods_id;
        /// <summary>
        /// 编号,对象编号,首页随机编号
        ///</summary>
        public string  Mods_id
        {
            get{ return _Mods_id; }
            set{ _Mods_id = value; }
        }

        private string  _Title;
        /// <summary>
        /// 名称
        ///</summary>
        public string  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }

        private int  _Jindex;
        /// <summary>
        /// 排序
        ///</summary>
        public int  Jindex
        {
            get{ return _Jindex; }
            set{ _Jindex = value; }
        }

        private string  _Target;
        /// <summary>
        /// 目标地址
        ///</summary>
        public string  Target
        {
            get{ return _Target; }
            set{ _Target = value; }
        }

        private string  _Action;
        /// <summary>
        /// 管理地址
        ///</summary>
        public string  Action
        {
            get{ return _Action; }
            set{ _Action = value; }
        }

        private string  _Jtype;
        /// <summary>
        /// 类型:0页面,1文章栏目,2标签,3留言薄,4标签,5首页
        ///</summary>
        public string  Jtype
        {
            get{ return _Jtype; }
            set{ _Jtype = value; }
        }

        private string  _Class;
        /// <summary>
        /// style式样
        ///</summary>
        public string  Class
        {
            get{ return _Class; }
            set{ _Class = value; }
        }

        private string  _Lan;
        /// <summary>
        /// 语言编号
        ///</summary>
        public string  Lan
        {
            get{ return _Lan; }
            set{ _Lan = value; }
        }
        #endregion

		#region 构造函数
        /// <summary>
        /// mods数据元
        /// </summary>
        public ModsVO(){}
	
        /// <summary>
        /// mods数据元
        /// </summary>
        /// <param name="mods_id">数据主键</param>
        public ModsVO(string mods_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("mods").Where("mods_id",mods_id, DataType.Char, 32).Row();
            if(dataReader != null)
            {
				this.Mods_id = dataReader["mods_id"].ToString();
				this.Title = dataReader["title"].ToString();
				this.Jindex = Convert.ToInt32(dataReader["jindex"]);
				this.Target = dataReader["target"].ToString();
				this.Action = dataReader["action"].ToString();
				this.Jtype = dataReader["jtype"].ToString();
				this.Class = dataReader["class"].ToString();
				this.Lan = dataReader["lan"].ToString();
            }
        }

		/// <summary>
        /// mods数据元
        /// </summary>
        /// <param name="mods_id">数据主键</param>
        public ModsVO(DataRow dataReader)
        {
			this.Mods_id = dataReader["mods_id"].ToString();
			this.Title = dataReader["title"].ToString();
			this.Jindex = Convert.ToInt32(dataReader["jindex"]);
			this.Target = dataReader["target"].ToString();
			this.Action = dataReader["action"].ToString();
			this.Jtype = dataReader["jtype"].ToString();
			this.Class = dataReader["class"].ToString();
			this.Lan = dataReader["lan"].ToString();
        }
		#endregion

		#region 插入，更新数据
        /// <summary>
        /// mods插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("mods").
            Value("mods_id", data["mods_id"], DataType.Char, 32).
            Value("title", data["title"], DataType.Varchar, 50).
            Value("jindex", data["jindex"], DataType.Int).
            Value("target", data["target"], DataType.Varchar, 200).
            Value("action", data["action"], DataType.Varchar, 20).
            Value("jtype", data["jtype"], DataType.Char, 1).
            Value("class", data["class"], DataType.Varchar, 100).
            Value("lan", data["lan"], DataType.Char, 1).
            Excute();
        }

        /// <summary>
        /// mods更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("mods").
            Set("title", data["title"], DataType.Varchar, 50).
            Set("jindex", data["jindex"], DataType.Int).
            Set("target", data["target"], DataType.Varchar, 200).
            Set("action", data["action"], DataType.Varchar, 20).
            Set("jtype", data["jtype"], DataType.Char, 1).
            Set("class", data["class"], DataType.Varchar, 100).
            Set("lan", data["lan"], DataType.Char, 1).
            Where("mods_id", data["mods_id"], DataType.Char, 32).
            Excute();
        }
		#endregion
    }
}