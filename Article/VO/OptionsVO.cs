﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 21:16:26
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// options数据元
    /// </summary>
    [Serializable]
    public class OptionsVO
    {
        #region	属性


        private string _Options_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Options_id
        {
            get { return _Options_id; }
            set { _Options_id = value; }
        }

        private string _Question_id;
        /// <summary>
        /// 问题编号
        /// </summary>
        public string Question_id
        {
            get { return _Question_id; }
            set { _Question_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 名称
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        #endregion

        /// <summary>
        /// options数据元
        /// </summary>
        public OptionsVO() { }

        /// <summary>
        /// options数据元
        /// </summary>
        /// <param name="options_id">数据主键</param>
        public OptionsVO(string options_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("options").Where("options_id", options_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Options_id = dataReader["options_id"].ToString();
                this.Question_id = dataReader["question_id"].ToString();
                this.Title = dataReader["title"].ToString();
            }
        }
        /// <summary>
        /// options
        /// </summary>
        /// <param name="options_id">Primary Key</param>
        public OptionsVO(DataRow dataReader)
        {
            this.Options_id = dataReader["options_id"] as string;
            this.Question_id = dataReader["question_id"] as string;
            this.Title = dataReader["title"] as string;
        }
        /// <summary>
        /// options插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("options").
                        Value("options_id", data["options_id"], DataType.Char, 24).
                        Value("question_id", data["question_id"], DataType.Char, 24).
                        Value("title", data["title"], DataType.Varchar, 200).
                        Excute();
        }

        /// <summary>
        /// options更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("options").
                        Set("question_id", data["question_id"], DataType.Char, 24).
                        Set("title", data["title"], DataType.Varchar, 200).
                                    Where("options_id", data["options_id"], DataType.Char, 24).
                        Excute();
        }
    }
}