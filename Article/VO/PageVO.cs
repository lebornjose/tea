﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/6/24 22:51:19
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// page数据元
    /// </summary>
    [Serializable]
    public class PageVO
    {
        #region	属性


        private string _Page_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Page_id
        {
            get { return _Page_id; }
            set { _Page_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Describe;
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe
        {
            get { return _Describe; }
            set { _Describe = value; }
        }

        private string _Content;
        /// <summary>
        /// 内容
        /// </summary>
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private string _Ispost;
        /// <summary>
        /// 是否允许评论,0否，1是
        /// </summary>
        public string Ispost
        {
            get { return _Ispost; }
            set { _Ispost = value; }
        }

        private int _Index;
        /// <summary>
        /// 排序
        /// </summary>
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        private string _Jtype;
        /// <summary>
        /// 页面分类
        /// </summary>
        public string Jtype
        {
            get { return _Jtype; }
            set { _Jtype = value; }
        }

        public string Image_id { get; set; }
        #endregion

        /// <summary>
        /// page数据元
        /// </summary>
        public PageVO() { }

        /// <summary>
        /// page数据元
        /// </summary>
        /// <param name="page_id">数据主键</param>
        public PageVO(string page_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("page").Where("page_id", page_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Page_id = dataReader["page_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Describe = dataReader["describe"].ToString();
                this.Content = dataReader["content"].ToString();
                this.Ispost = dataReader["ispost"].ToString();
                this.Index = Convert.ToInt32(dataReader["index"]);
                this.Jtype = dataReader["jtype"].ToString();
            }
        }
        /// <summary>
        /// page
        /// </summary>
        /// <param name="page_id">Primary Key</param>
        public PageVO(DataRow dataReader)
        {
            this.Page_id = dataReader["page_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Describe = dataReader["describe"] as string;
            this.Content = dataReader["content"] as string;
            this.Ispost = dataReader["ispost"] as string;
            this.Index = Convert.ToInt32(dataReader["index"]);
            this.Jtype = dataReader["jtype"] as string;
        }
        /// <summary>
        /// page插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("page").
                Value("page_id", data["page_id"], DataType.Char, 24).
                Value("title", data["title"], DataType.Varchar, 100).
                Value("describe", data["describe"], DataType.Varchar, 500).
                Value("content", data["content"], DataType.Text).
                Value("ispost", data["ispost"], DataType.Char, 1).
                Value("index", data["index"], DataType.Int).
                Value("jtype", data["jtype"], DataType.Char, 24).
                Excute();
        }

        /// <summary>
        /// page更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("page").
                Set("title", data["title"], DataType.Varchar, 100).
                Set("describe", data["describe"], DataType.Varchar, 500).
                Set("content", data["content"], DataType.Text).
                Set("ispost", data["ispost"], DataType.Char, 1).
                Set("index", data["index"], DataType.Int).
                Set("jtype", data["jtype"], DataType.Char, 24).
                Where("page_id", data["page_id"], DataType.Char, 24).
                Excute();
        }
    }
}