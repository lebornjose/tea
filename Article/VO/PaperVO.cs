﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 21:16:25
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// paper数据元
    /// </summary>
    [Serializable]
    public class PaperVO
    {
        #region	属性


        private string _Paper_id;
        /// <summary>
        /// 问卷编号
        /// </summary>
        public string Paper_id
        {
            get { return _Paper_id; }
            set { _Paper_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 名称
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Describe;
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe
        {
            get { return _Describe; }
            set { _Describe = value; }
        }

        private int _Addtime;
        /// <summary>
        /// 添加时间
        /// </summary>
        public int Addtime
        {
            get { return _Addtime; }
            set { _Addtime = value; }
        }

        private int _Starttime;
        /// <summary>
        /// 开始时间
        /// </summary>
        public int Starttime
        {
            get { return _Starttime; }
            set { _Starttime = value; }
        }

        private int _Endtime;
        /// <summary>
        /// 结束时间
        /// </summary>
        public int Endtime
        {
            get { return _Endtime; }
            set { _Endtime = value; }
        }

        private string _Status;
        /// <summary>
        /// 0关闭，1开启，2过期
        /// </summary>
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private string _Objects;
        /// <summary>
        /// 调查对象
        /// </summary>
        public string Objects
        {
            get { return _Objects; }
            set { _Objects = value; }
        }
        #endregion

        /// <summary>
        /// paper数据元
        /// </summary>
        public PaperVO() { }

        /// <summary>
        /// paper数据元
        /// </summary>
        /// <param name="paper_id">数据主键</param>
        public PaperVO(string paper_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("paper").Where("paper_id", paper_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Paper_id = dataReader["paper_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Describe = dataReader["describe"].ToString();
                this.Addtime = Convert.ToInt32(dataReader["addtime"]);
                this.Starttime = Convert.ToInt32(dataReader["starttime"]);
                this.Endtime = Convert.ToInt32(dataReader["endtime"]);
                this.Status = dataReader["status"].ToString();
                this.Objects = dataReader["objects"].ToString();
            }
        }
        /// <summary>
        /// paper
        /// </summary>
        /// <param name="paper_id">Primary Key</param>
        public PaperVO(DataRow dataReader)
        {
            this.Paper_id = dataReader["paper_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Describe = dataReader["describe"] as string;
            this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            this.Starttime = Convert.ToInt32(dataReader["starttime"]);
            this.Endtime = Convert.ToInt32(dataReader["endtime"]);
            this.Status = dataReader["status"] as string;
            this.Objects = dataReader["objects"] as string;
        }
        /// <summary>
        /// paper插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("paper").
                        Value("paper_id", data["paper_id"], DataType.Char, 24).
                        Value("title", data["title"], DataType.Varchar, 200).
                        Value("describe", data["describe"], DataType.Varchar, 1000).
                        Value("addtime", data["addtime"], DataType.Int).
                        Value("starttime", data["starttime"], DataType.Int).
                        Value("endtime", data["endtime"], DataType.Int).
                        Value("status", data["status"], DataType.Char, 1).
                        Value("objects", data["objects"], DataType.Varchar, 500).
                        Excute();
        }

        /// <summary>
        /// paper更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("paper").
                        Set("title", data["title"], DataType.Varchar, 200).
                        Set("describe", data["describe"], DataType.Varchar, 1000).
                        Set("addtime", data["addtime"], DataType.Int).
                        Set("starttime", data["starttime"], DataType.Int).
                        Set("endtime", data["endtime"], DataType.Int).
                        Set("status", data["status"], DataType.Char, 1).
                        Set("objects", data["objects"], DataType.Varchar, 500).
                                    Where("paper_id", data["paper_id"], DataType.Char, 24).
                        Excute();
        }
    }
}