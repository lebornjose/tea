﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 21:16:26
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// question数据元
    /// </summary>
    [Serializable]
    public class QuestionVO
    {
        #region	属性


        private string _Question_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Question_id
        {
            get { return _Question_id; }
            set { _Question_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 名称
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Jtype;
        /// <summary>
        /// 类型
        /// </summary>
        public string Jtype
        {
            get { return _Jtype; }
            set { _Jtype = value; }
        }

        private string _Paper_id;
        /// <summary>
        /// 调查编号
        /// </summary>
        public string Paper_id
        {
            get { return _Paper_id; }
            set { _Paper_id = value; }
        }
        #endregion

        /// <summary>
        /// question数据元
        /// </summary>
        public QuestionVO() { }

        /// <summary>
        /// question数据元
        /// </summary>
        /// <param name="question_id">数据主键</param>
        public QuestionVO(string question_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("question").Where("question_id", question_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Question_id = dataReader["question_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Jtype = dataReader["jtype"].ToString();
                this.Paper_id = dataReader["paper_id"].ToString();
            }
        }
        /// <summary>
        /// question
        /// </summary>
        /// <param name="question_id">Primary Key</param>
        public QuestionVO(DataRow dataReader)
        {
            this.Question_id = dataReader["question_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Jtype = dataReader["jtype"] as string;
            this.Paper_id = dataReader["paper_id"] as string;
        }
        /// <summary>
        /// question插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("question").
                        Value("question_id", data["question_id"], DataType.Char, 24).
                        Value("title", data["title"], DataType.Varchar, 200).
                        Value("jtype", data["jtype"], DataType.Char, 1).
                        Value("paper_id", data["paper_id"], DataType.Char, 24).
                        Excute();
        }

        /// <summary>
        /// question更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("question").
                        Set("title", data["title"], DataType.Varchar, 200).
                        Set("jtype", data["jtype"], DataType.Char, 1).
                        Set("paper_id", data["paper_id"], DataType.Char, 24).
                                    Where("question_id", data["question_id"], DataType.Char, 24).
                        Excute();
        }
    }
}