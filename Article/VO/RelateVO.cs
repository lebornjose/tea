﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-16 21:16:26
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
    /// <summary>
    /// relate数据元
    /// </summary>
    [Serializable]
    public class RelateVO
    {
        #region	属性


        private string _Article_id;
        /// <summary>
        /// 文章编号
        /// </summary>
        public string Article_id
        {
            get { return _Article_id; }
            set { _Article_id = value; }
        }

        private string _Object_id;
        /// <summary>
        /// 被关联文章编号
        /// </summary>
        public string Object_id
        {
            get { return _Object_id; }
            set { _Object_id = value; }
        }

        private int _Addtime;
        /// <summary>
        /// 添加时间
        /// </summary>
        public int Addtime
        {
            get { return _Addtime; }
            set { _Addtime = value; }
        }

        private string _Keyword;
        /// <summary>
        /// 关联关键字
        /// </summary>
        public string Keyword
        {
            get { return _Keyword; }
            set { _Keyword = value; }
        }
        #endregion

        /// <summary>
        /// relate数据元
        /// </summary>
        public RelateVO() { }

        /// <summary>
        /// relate数据元
        /// </summary>
        /// <param name="article_id">数据主键</param>
        public RelateVO(string article_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("relate").Where("article_id", article_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Article_id = dataReader["article_id"].ToString();
                this.Object_id = dataReader["object_id"].ToString();
                this.Addtime = Convert.ToInt32(dataReader["addtime"]);
                this.Keyword = dataReader["keyword"].ToString();
            }
        }
        /// <summary>
        /// relate
        /// </summary>
        /// <param name="article_id">Primary Key</param>
        public RelateVO(DataRow dataReader)
        {
            this.Article_id = dataReader["article_id"] as string;
            this.Object_id = dataReader["object_id"] as string;
            this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            this.Keyword = dataReader["keyword"] as string;
        }
        /// <summary>
        /// relate插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Insert("relate").
                        Value("article_id", data["article_id"], DataType.Char, 24).
                        Value("object_id", data["object_id"], DataType.Char, 24).
                        Value("addtime", data["addtime"], DataType.Int).
                        Value("keyword", data["keyword"], DataType.Varchar, 200).
                        Excute();
        }

        /// <summary>
        /// relate更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("article").Update("relate").
                        Set("object_id", data["object_id"], DataType.Char, 24).
                        Set("addtime", data["addtime"], DataType.Int).
                        Set("keyword", data["keyword"], DataType.Varchar, 200).
                                    Where("article_id", data["article_id"], DataType.Char, 24).
                        Excute();
        }
    }
}