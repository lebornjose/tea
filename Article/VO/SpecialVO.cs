﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/7/1 10:14:56
 */
using System;
using System.Data;

namespace Jabinfo.Article.VO
{
	/// <summary>
	/// special数据元
	/// </summary>
	[Serializable]
	public class SpecialVO
	{
		#region	属性


		private string  _Special_id;
		/// <summary>
		/// 编号
		/// </summary>
		public string  Special_id
		{
			get{ return _Special_id; }
			set{ _Special_id = value; }
		}

		private string  _Article_id;
		/// <summary>
		/// 文章编号
		/// </summary>
		public string  Article_id
		{
			get{ return _Article_id; }
			set{ _Article_id = value; }
		}

		private string  _Object_id;
		/// <summary>
		/// 对象编号
		/// </summary>
		public string  Object_id
		{
			get{ return _Object_id; }
			set{ _Object_id = value; }
		}
		#endregion
		
		/// <summary>
		/// special数据元
		/// </summary>
		public SpecialVO(){}

		/// <summary>
		/// special数据元
		/// </summary>
		/// <param name="special_id">数据主键</param>
		public SpecialVO(string special_id)
		{
            DataRow dataReader = JabinfoSQL.Instance("article").Select("*").From("special").Where("special_id", special_id, DataType.Char).Row();
			    if (dataReader != null)
                {
					this.Special_id = dataReader["special_id"].ToString();
					this.Article_id = dataReader["article_id"].ToString();
					this.Object_id = dataReader["object_id"].ToString();
				}
		}
        /// <summary>
        /// special
        /// </summary>
        /// <param name="special_id">Primary Key</param>
        public SpecialVO(DataRow dataReader)
        {
            this.Special_id = dataReader["special_id"] as string;
            this.Article_id = dataReader["article_id"] as string;
            this.Object_id = dataReader["object_id"] as string;
        }
		/// <summary>
		/// special插入数据
		/// </summary>
		/// <param name="data">数据集</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("article").Insert("special").
				Value("special_id", data["special_id"], DataType.Char, 24).
				Value("article_id", data["article_id"], DataType.Char, 24).
				Value("object_id", data["object_id"], DataType.Char, 24).
				Excute();
		}

		/// <summary>
		/// special更新数据
		/// </summary>
		/// <param name="data">数据集</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("article").Update("special").
				Set("article_id", data["article_id"], DataType.Char, 24).
				Set("object_id", data["object_id"], DataType.Char, 24).
				Where("special_id", data["special_id"], DataType.Char, 24).
				Excute();
		}
	}
}