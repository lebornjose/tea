﻿/*
 * Created by SharpDevelop.
 * User: Sinbo
 * Date: 2010-6-17
 * Time: 15:07
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Collections.Generic;
using Jabinfo.Core.VO;
using Jabinfo.Core.Model;


namespace Jabinfo.Core
{
    /// <summary>
    /// Description of Category.
    /// </summary>
    public class API
    {
        private static API _Instance;
        public static API Instance { get { if (_Instance == null)_Instance = new API(); return _Instance; } }
        private API()
        {
        }

        #region 获取分类
        /// <summary>
        /// 获取一个分类对象，如果为空，则返回空对象，不是null值
        /// </summary>
        /// <param name="category_id">分类编号</param>
        /// <returns></returns>
        public CategoryVO get(string category_id)
        {
            //if (category_id.Length < 10)
            //return new VO.CategoryVO();
            CategoryVO categoryVO = new CategoryVO(category_id);
            if (categoryVO.Category_id == null)
                return new CategoryVO();
            return categoryVO;
        }

        /// <summary>
        /// 获取一组分类
        /// </summary>
        /// <param name="parent_id">父编号</param>
        /// <returns></returns>
        public CategoryVO[] group(string parent_id)
        {
            CategoryModel categoryModel = new CategoryModel();
            return categoryModel.Select(parent_id);
        }
        #endregion

        #region 根据父编号，获取分类组
        
        public CategoryVO[] group(string parent_id, int length)
        {
            CategoryModel categoryModel = new CategoryModel();
            string where = string.Format("parent_id='{0}'", parent_id);
            return categoryModel.Select(length, where, "category_id");
        }
        #endregion

        #region 获取标签
        /// <summary>
        /// 获取一个标签
        /// </summary>
        /// <param name="tag_id"></param>
        /// <returns></returns>
        public TagsVO tag(string tag_id)
        {
            TagsVO tagVO = new TagsVO(tag_id);
            if (tagVO.Tags_id == null)
                return new TagsVO();
            return tagVO;
        }

        public TagsVO nav_tag(string tags_id)
        {
            TagsModel tagModel = new TagsModel();
            TagsVO tagVO = tagModel.Get_nav(tags_id);
            return (tagVO != null && Convert.ToInt32(tagVO.Tags_id) < 1249 && Convert.ToInt32(tagVO.Tags_id) > 1243) ? tagVO : null;
        }

        /// <summary>
        /// 分类关联所有标签
        /// </summary>
        /// <param name="category_id"></param>
        /// <returns></returns>
        public TagsVO[] tagGroup(string category_id)
        {
            TagsModel tagsModel = new TagsModel();
            TagsVO[] result = tagsModel.Select(category_id);
            return result;
        }

        /// <summary>
        /// 递归获取分类树形结构
        /// </summary>
        /// <param name="parent_id">父级编号</param>
        /// <param name="i">input显示偏移位计算</param>
        /// <returns></returns>
        public string categoryOption(string parent_id, int i)
        {
            StringBuilder stringBuilder = new StringBuilder(1500);
            CategoryVO[] categorys = this.group(parent_id);
            if (categorys == null)
                return string.Empty;
            i = i + 1;
            foreach (CategoryVO category in categorys)
            {
                if (i == 1)
                {
                    stringBuilder.AppendFormat("<li class=\"jibie{0}\">", i);
                }
                else
                {
                    stringBuilder.AppendFormat("<li style=\"display:none\" class=\"cls{0} jibie{1}\">", category.Parent_id, i);
                }
                for (int j = 1; j < i; j++)
                {
                    stringBuilder.Append("&nbsp;&nbsp;");
                }
                stringBuilder.AppendFormat("<input type=\"checkbox\" name=\"categorys\" onclick=\"setTag(this,{2},{3})\" value=\"{0}\" />&nbsp;<span>{1}</span></li>", category.Category_id, category.Title, i, category.Parent_id);
                if (category.Childen != "0")
                {
                    stringBuilder.Append(categoryOption(category.Category_id, i));
                }
            }
            return stringBuilder.ToString();
        }
        #endregion

        #region 根据 节点id获取节点
        public string option(string parent_id, int i)
        {
            return option(parent_id, i, "0");
        }
        /// <summary>
        /// 父元素
        /// </summary>
        /// <param name="parent_id"></param>
        /// <param name="i"></param>
        /// <param name="ischildren">是否只要子节点（“1”只要一级子节点，“0”是所有子节点，默认0）</param>
        /// <returns></returns>
        public string option(string parent_id, int i, string ischildren)
        {
            if (string.IsNullOrEmpty(ischildren))
            {
                ischildren = "0";
            }
            StringBuilder stringBuilder = new StringBuilder(1500);
            CategoryVO[] categorys = this.group(parent_id);
            if (categorys == null)
                return string.Empty;
            i = i + 1;
            foreach (CategoryVO category in categorys)
            {
                stringBuilder.AppendFormat("<option value=\"{0}\">", category.Category_id);
                if (ischildren == "0")
                {
                    for (int j = 1; j < i; j++)
                    {
                        stringBuilder.Append("&nbsp;&nbsp;&nbsp;");
                    }
                    stringBuilder.AppendFormat("{0}.{1}</option>", i, category.Title);
                    if (category.Childen != "0")
                    {
                        stringBuilder.Append(option(category.Category_id, i, ischildren));
                    }
                }
                else
                {
                    stringBuilder.AppendFormat("{0}</option>", category.Title);
                }
            }
            return stringBuilder.ToString();
        }
        #endregion

        #region 广告
        /// <summary>
        /// 获取一个广告位
        /// </summary>
        /// <returns></returns>
        public string position(string position_id)
        {
            PositionModel positionModel = new PositionModel();
            PositionVO position = new PositionVO(position_id);
            if (position.Position_id == null)
                return string.Empty;
            if (string.IsNullOrEmpty(position.Content))
            {
                AdvertiseModel advertiseModel = new AdvertiseModel();
                AdvertiseVO[] advertises = advertiseModel.Select(position_id, 1);
                if (advertises != null)
                {
                    AdvertiseVO advertise = advertises[0];
                    JabinfoKeyValue data = new JabinfoKeyValue();
                    data["position_id"] = position_id;
                    data["content"] = advertise.Content;
                    data["advertise_id"] = advertise.Advertise_id;
                    data["title"] = advertise.Title;
                    data["target"] = advertise.Title;
                    positionModel.Update(data);
                    return advertise.Content;
                }
            }
            return position.Content;
        }

        /// <summary>
        /// 获取一个广告
        /// </summary>
        /// <returns></returns>
        public AdvertiseVO ads(string position_id)
        {
            AdvertiseModel advertiseModel = new AdvertiseModel();
            AdvertiseVO[] advertise = advertiseModel.Select(position_id, 1);
            return advertise == null ? null : advertise[0];
        }
        /// <summary>
        /// 获取所有有效广告
        /// </summary>
        /// <returns></returns>
        public AdvertiseVO[] all(string position_id)
        {
            AdvertiseModel advertiseModel = new AdvertiseModel();
            return advertiseModel.Select(position_id, 0);
        }

        /// <summary>
        /// 广告位所以图象
        /// </summary>
        /// <param name="position_id"></param>
        /// <returns></returns>
        public AdvertiseVO[] adsList(string position_id, int length)
        {
            AdvertiseModel advertiseModel = new AdvertiseModel();
            return advertiseModel.Select(position_id, length);
        }
        #endregion

    }
}
