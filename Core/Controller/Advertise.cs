﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Core
{
    class Advertise : Sinbo.Controller
    {
        public Advertise()
        {
            this.Access = Right.Administrator;
        }

        public void home(string position_id, string start)
        {
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string where = string.Format("position_id='{0}'", position_id);
            PositionModel positionModel = new PositionModel();
            PositionVO position = new PositionVO(position_id);
            JabinfoView view = new JabinfoView("core_advertise_home", this.JabinfoContext);
            view.Variable["advertiseList"] = advertiseModel.Select(index, 30, where, "jindex asc");
            view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["position_id"] = position_id;
            view.Variable["position"] = position;
            view.Variable["total"] = advertiseModel.Count(where);
            view.Render();
        }

        public void add(string position_id)
        {
            if (this.IsPost)
            {
                string result = string.Format("position_id='{0}' and object_id='{1}'", this.Post["position_id"], this.Post["object_id"]);
                AdvertiseVO[] advertise = advertiseModel.Select(result);
                if (advertise != null)
                {
                    this.Jump(this.Url.Site(string.Format("core/advertise/home/{0}", this.Post["position_id"])), "添加失败，该产品已存在此推荐位上！");
                    return;
                }
                
                this.Post["advertise_id"] = Jabinfo.Help.Basic.JabId;
                if (this.Post["jtype"] != "0" && this.Files["file"].ContentLength > 0)
                {
                    Jabinfo.Help.Upload.Save(this.Post["advertise_id"], this.Files["file"], this.Post["jtype"]);
                    string file = Jabinfo.Help.Upload.Get(this.Post["advertise_id"], this.Post["jtype"]);
                    PositionModel positionModel = new PositionModel();
                    PositionVO position = new PositionVO(this.Post["position_id"]);
                    switch (this.Post["jtype"])
                    {
                        case "jpg":
                            this.Post["content"] = string.Format("<img src=\"{0}\" width=\"{1}\" height=\"{2}\" alt=\"{3}\" />", file, position.Width, position.Height, this.Post["title"]);
                            break;
                        case "swf":
                            this.Post["content"] = string.Format("");
                            break;
                    }
                }
                if (this.Post["jtype"] == "0")
                {
                    this.Post["content"] = this.Post["summary"];
                }
                if (string.IsNullOrEmpty(this.Post["jindex"]))
                {
                    this.Post["jindex"] = "0";
                }
                if (advertiseModel.Insert(this.Post) == "-1")
                    this.Jump(this.Url.Site(string.Format("core/advertise/home/{0}", this.Post["position_id"])), "添加失败，结束时间不能小于开始时间");
                else
                    this.Jump(this.Url.Site(string.Format("core/advertise/home/{0}", this.Post["position_id"])), "提交成功");
                return;
            }
            JabinfoView view = new JabinfoView("core_advertise_add", this.JabinfoContext);
            view.Variable["position_id"] = position_id;
            view.Render();
        }

        public void edit(string advertise_id)
        {
            if (advertise_id == null)
            {
                if (!string.IsNullOrEmpty(this.Post["all"]))
                {
                    this.Post["starttime"] = "0";
                    this.Post["endtime"] = "0";
                }
                else
                {
                    int starttime = Jabinfo.Help.Date.StringToDate(this.Post["starttime"]);
                    int endtime = Jabinfo.Help.Date.StringEndDate(this.Post["endtime"]);
                    if (endtime <= starttime)
                    {
                        this.Jump(this.Url.Site(string.Format("core/advertise/home/{0}", this.Post["position_id"])), "结束时间不能小于开始时间");
                        return;
                    }
                    this.Post["starttime"] = starttime.ToString();
                    this.Post["endtime"] = endtime.ToString();
                }
                AdvertiseVO advertise = new AdvertiseVO(this.Post["advertise_id"]);
                if (this.Post["jtype"] != "0" && this.Files["file"].ContentLength > 0)
                {
                    PositionModel positionModel = new PositionModel();
                    PositionVO position = new PositionVO(advertise.Position_id);
                    Jabinfo.Help.Upload.Save(this.Post["advertise_id"], this.Files["file"], this.Post["jtype"]);
                    string file = Jabinfo.Help.Upload.Get(this.Post["advertise_id"], this.Post["jtype"]);
                    switch (this.Post["jtype"])
                    {
                        case "jpg":
                            this.Post["content"] = string.Format("<img src=\"{0}\" width=\"{1}\" height=\"{2}\" />", file, position.Width, position.Height);
                            break;
                        case "swf":
                            this.Post["content"] = string.Format("");
                            break;
                    }
                }
                if (this.Post["jtype"] == "0")
                {
                    this.Post["content"] = this.Post["summary"];
                }
                advertiseModel.Update(this.Post);
                this.Jump(this.Url.Site(string.Format("core/advertise/home/{0}", advertise.Position_id)), "提交成功");
                return;
            }
            JabinfoView view = new JabinfoView("core_advertise_edit", this.JabinfoContext);
            view.Variable["advertise"] = new AdvertiseVO(advertise_id);
            view.Render();
        }

        public void remove(string advertise_id)
        {
            AdvertiseVO advertise = new AdvertiseVO(advertise_id);
            if (advertise.Advertise_id != null)
            {
                Jabinfo.Help.Upload.Remove(advertise_id, advertise.Jtype);
                advertiseModel.Delete(advertise_id);
            }
        }

        private AdvertiseModel _advertiseModel = null;
        private AdvertiseModel advertiseModel
        {
            get
            {
                if (_advertiseModel == null)
                    _advertiseModel = new AdvertiseModel();
                return _advertiseModel;
            }
        }
    }
}