﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-15 16:51:48
 */
using System;
using System.Text;
using System.Collections.Generic;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Core
{
    class Category : Sinbo.Controller
    {
        public Category()
        {
        }
        public void home(string parent_id)
        {
            this.CheckUser(Right.Administrator);
            TagsModel tagsModel = new TagsModel();
            string parent_title = "顶级分类";
            string last_id = parent_id;
            string currnt_title = "";
            string top_id;
            if (parent_id.Length > 2)
            {
                CategoryVO categoryVO = new CategoryVO(parent_id);
                if (categoryVO.Category_id != null)
                {
                    currnt_title = categoryVO.Title;
                    last_id = categoryVO.Parent_id;
                    CategoryVO lastVO = new CategoryVO(last_id);
                    parent_title = lastVO.Title;
                }
                top_id = topid(categoryVO);
            }
            else
            {
                top_id = parent_id;
            }

            JabinfoView view = new JabinfoView(string.Format("core_category_home{0}", top_id), this.JabinfoContext);
            view.Variable["categoryList"] = categoryModel.Select(parent_id);
            view.Variable["tagsList"] = tagsModel.Select(parent_id);
            view.Variable["parent_id"] = parent_id;
            view.Variable["last_id"] = last_id;
            view.Variable["currnt_title"] = currnt_title;
            view.Variable["parent_title"] = parent_title;
            view.Variable["type"] = Jabinfo.Help.Config.Get("core.category");
            view.Render();
        }

        public void add(string parent_id)
        {
            this.CheckUser(Right.Administrator);
            if (this.IsPost)
            {
                string url = this.Url.Site(string.Format("core/category/home/{0}", this.Post["parent_id"]));
                string keyword = this.Post["keyword"].Trim();
                foreach (string name in this.Files.AllKeys)
                {
                    if (this.Files[name].ContentLength > 5)
                    {
                        System.Web.HttpPostedFile temp = this.Files[name];
                        Jabinfo.Help.Image.Save(string.Format("{0}_{1}", this.Post["category_id"], name.Split('_')[1]), this.Files[name]);
                    }
                }
                if (this.Post["parent_id"].Length > 1)
                {
                    CategoryVO categoryVO = new CategoryVO(this.Post["parent_id"]);
                    if (categoryVO.Category_id == null)
                    {
                        this.Jump(url, "添加失败，不存在父级分类");
                        return;
                    }
                    if (categoryVO.Childen == "0")
                    {
                        JabinfoKeyValue data = new JabinfoKeyValue();
                        data["category_id"] = this.Post["parent_id"];
                        data["childen"] = "1";
                        this.categoryModel.Update(data);
                    }
                    JabinfoKeyValue cate_Data = new JabinfoKeyValue();
                    this.Post["childen"] = (Convert.ToInt32(categoryVO.Childen) + 1).ToString();
                }
                categoryModel.Insert(this.Post);
                this.Jump(url, "添加成功");
                return;
            }
            JabinfoView view = new JabinfoView("core_category_add", this.JabinfoContext);
            view.Variable["parent_id"] = parent_id;
            view.Render();
        }

        public void edit(string category_id)
        {
            this.CheckUser(Right.Administrator);
            if (category_id == null)
            {
                CategoryVO category = new CategoryVO(this.Post["category_id"]);
                string url = this.Url.Site(string.Format("core/category/home/{0}", category.Parent_id));
                string keyword = this.Post["keyword"].Trim();
                if (keyword.Contains(",") || keyword == "")
                {
                    categoryModel.Update(this.Post);
                    foreach (string name in this.Files.AllKeys)
                    {
                        if (this.Files[name].ContentLength > 5)
                        {
                            Jabinfo.Help.Image.Save(string.Format("{0}_{1}", this.Post["category_id"], name.Split('_')[1]), this.Files[name]);
                        }
                    }
                    this.Jump(url, "修改成功");
                    return;
                }
                else
                {
                    this.Jump(url, "关键字请用逗号隔开");
                    return;
                }
            }
            JabinfoView view = new JabinfoView("core_category_edit", this.JabinfoContext);
            view.Variable["category"] = new CategoryVO(category_id);
            view.Render();
        }

        public void remove(string category_id)
        {
            this.CheckUser(Right.Administrator);
            CategoryVO categoryVO = new CategoryVO(category_id);
            CategoryVO[] categorysVO = this.categoryModel.Select(category_id);
            if (categorysVO!= null)
            {
                Output("", "删除失败，请先删除子分类");
                return;
            }
            categoryModel.Delete(category_id);
            if (categoryVO.Parent_id.Length > 10)
            {
                int count = this.categoryModel.Count(string.Format("parent_id='{0}'", categoryVO.Parent_id));
                if (count == 0)
                {
                    JabinfoKeyValue data = new JabinfoKeyValue();
                    data["category_id"] = categoryVO.Parent_id;
                    data["childen"] = "0";
                    this.categoryModel.Update(data);
                }
            }
            TagsModel tagsModel = new TagsModel();
            tagsModel.DeleteGroup(category_id);
            Output("#", string.Empty);
        }

        public void tag(string tag_id)
        {
            TagsVO tagVO = new TagsVO(tag_id);
            string last_title = "";
            string last_id = "";
            if (tagVO.Category_id != null)
            {
                CategoryVO categoryVO = new CategoryVO(tagVO.Category_id);
                last_title = categoryVO.Title;
                last_id = categoryVO.Parent_id;
            }
            TagsModel tagsModel = new TagsModel();
            JabinfoView view = new JabinfoView("core_category_tag", this.JabinfoContext);
            view.Variable["tagsList"] = tagsModel.Sons(tag_id);
            view.Variable["tag"] = tagVO;
            view.Variable["last_title"] = last_title;
            view.Variable["last_id"] = last_id;
            view.Render();
        }

        private TagsVO[] tag_mod(object[] args)
        {
            string tags_id = args[0].ToString();
            TagsModel tagsModel = new TagsModel();
            TagsVO[] tags = tagsModel.Select(tags_id);
            return tags;
        }

        public void add_tag(string parent_id, string category_id)
        {
            this.CheckUser(Right.Administrator);
            if (this.IsPost)
            {
                string keyword = this.Post["keyword"].Trim();
                if (keyword.Contains(",") || keyword == "")
                {
                    TagsModel tagsModel = new TagsModel();
                    if (this.Post["color"] != string.Empty)
                        this.Post["style"] = string.Format("color:{0};", this.Post["color"]);
                    if (this.Post["size"] != string.Empty)
                        this.Post["style"] = string.Format("{0}font-size:{1}px;", this.Post["style"], this.Post["size"]);
                    if (!string.IsNullOrEmpty(this.Post["bold"]))
                        this.Post["style"] = string.Format("{0}font-weight:bold;", this.Post["style"], this.Post["bold"]);
                    if (!string.IsNullOrEmpty(this.Post["style"]))
                        this.Post["style"] = string.Format(" style=\"{0}\"", this.Post["style"]);
                    tagsModel.Insert(this.Post);
                    Output("#", "提交成功");
                    return;
                }
                else
                {
                    Output("", "关键字请用逗号隔开");
                    return;
                }
            }
            JabinfoView view = new JabinfoView("core_category_add_tag", this.JabinfoContext);
            view.Variable["parent_id"] = parent_id;
            view.Variable["category_id"] = category_id;
            view.Render();
        }

        public void edit_tag(string tag_id)
        {
            this.CheckUser(Right.Administrator);
            TagsModel tagsModel = new TagsModel();
            if (this.IsPost)
            {
                string keyword = this.Post["keyword"].Trim();
                if (keyword.Contains(",") || keyword == "")
                {
                    this.Post["style"] = string.Empty;
                    if (this.Post["color"] != string.Empty)
                        this.Post["style"] = string.Format("color:{0};", this.Post["color"]);
                    if (this.Post["size"] != string.Empty)
                        this.Post["style"] = string.Format("{0}font-size:{1}px;", this.Post["style"], this.Post["size"]);
                    if (!string.IsNullOrEmpty(this.Post["bold"]))
                        this.Post["style"] = string.Format("{0}font-weight:bold;", this.Post["style"], this.Post["bold"]);
                    if (this.Post["style"] != string.Empty)
                        this.Post["style"] = string.Format(" style=\"{0}\"", this.Post["style"]);
                    tagsModel.Update(this.Post);
                    Output("#", "修改成功");
                    return;
                }
                else
                {
                    Output("关键字请用逗号隔开");
                    return;
                }
            }
            TagsVO tagsVO = new TagsVO(tag_id);
            string color = "";
            string size = "";
            string bold = "";
            if (!string.IsNullOrEmpty(tagsVO.Style))
            {
                if (tagsVO.Style.Contains("color"))
                    color = tagsVO.Style.Substring(tagsVO.Style.IndexOf(":") + 1, tagsVO.Style.IndexOf(';') - tagsVO.Style.IndexOf(":") - 1);
                if (tagsVO.Style.Contains("size"))
                    size = tagsVO.Style.Substring(tagsVO.Style.IndexOf("size") + 5, 2);
                if (tagsVO.Style.Contains("weight"))
                    bold = "1";
            }
            JabinfoView view = new JabinfoView("core_category_edit_tag", this.JabinfoContext);
            view.Variable["tags"] = tagsVO;
            view.Variable["color"] = color;
            view.Variable["size"] = size;
            view.Variable["bold"] = bold;
            view.Render();
        }

        public void remove_tag(string tags_id)
        {
            this.CheckUser(Right.Administrator);
            TagsModel tagsModel = new TagsModel();
            int count = tagsModel.Count(string.Format("parent_id='{0}'", tags_id));
            if (count > 0)
            {
                Output("", "请先删除子标签");
                return;
            }
            TagsVO tags = new TagsVO(tags_id);
            if (tags.Tags_id == null)
            {
                return;
            }
            tagsModel.Delete(tags_id);
            Print("#$");
        }

        /// <summary>
        /// 获取标签选项
        /// </summary>
        /// <param name="category_id"></param>
        public void tag_option(string category_id)
        {
            TagsModel tagsModel = new TagsModel();
            TagsVO[] tags = tagsModel.Select(category_id);
            StringBuilder initTag = new StringBuilder();
            if (tags == null)
            {
                Print("no");
                return;
            }
            foreach (TagsVO tag in tags)
            {
                Print(string.Format("<input type=\"checkbox\" onclick=\"sonTag(this)\" name=\"tags\" value=\"{0}\" />{1}", tag.Tags_id, tag.Title));
            }
            Print("</br>");
        }

        #region 获取标签的子标签
        public void tag_sons(string tags_id)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("<div style=\"margin-left: 16px;\" id=\"t{0}\">", tags_id);
            TagsModel tagsModel = new TagsModel();
            TagsVO[] tags = tagsModel.Sons(tags_id);
            if (tags != null)
            {
                foreach (TagsVO tagsVO in tags)
                    builder.AppendFormat("<input type=\"checkbox\" class=\"son{0}\" name=\"tags\" value=\"{1}\" />{2}&nbsp;", tags_id, tagsVO.Tags_id, tagsVO.Title);
            }
            builder.Append("</div>");
            Print(builder.ToString());
        }
        #endregion

        /// <summary>
        /// 通过递归找到顶级分类id
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private string topid(CategoryVO category)
        {
            if (category.Parent_id.Length < 2)
                return category.Parent_id;
            CategoryVO parent = new CategoryVO(category.Parent_id);
            return topid(parent);
        }
        /// <summary>
        /// 通过递归将分类级别更新
        /// </summary>
        /// <param name="category"></param>
        /// <param name="grade"></param>
        public void cate_grade(string grade)
        {
            if (grade != "4")
            {
                string where = string.Empty;
                where = string.Format("childen = '{0}'", grade);
                JabinfoKeyValue cateData = new JabinfoKeyValue();
                CategoryVO[] categorysVO = categoryModel.Select(100, where, string.Empty);
                grade = (Convert.ToInt32(grade) + 1).ToString();
                foreach (CategoryVO categoryVO in categorysVO)
                {
                    CategoryVO[] category = categoryModel.Select(categoryVO.Category_id);
                    if (category != null)
                    {
                        foreach (CategoryVO cate in category)
                        {
                            cateData["category_id"] = cate.Category_id;
                            cateData["childen"] = grade;
                            categoryModel.Update(cateData);
                        }
                    }
                }
                cate_grade(grade);
            }
            else
            {
                this.Jump("core/category/home/1", "更新成功");
            }
        }
        public void cache(string category_id)
        {
            this.CheckUser(Right.Administrator);
            CategoryVO category = new CategoryVO(category_id);
            if (category.Category_id != null)
            {
                Jabinfo.Help.Basic.ClearPath(category_id);
                if (category.Childen != "0")
                {
                    CategoryVO[] cats = categoryModel.Select(category.Category_id);
                    foreach (CategoryVO cat in cats)
                    {
                        cache(cat.Category_id);
                    }
                }
            }
            else
            {
                TagsVO tags = new TagsVO(category_id);
                if (tags.Tags_id != null)
                {
                    Jabinfo.Help.Basic.ClearPath(category_id);
                }
            }
        }

        private CategoryModel _categoryModel = null;
        private CategoryModel categoryModel
        {
            get
            {
                if (_categoryModel == null)
                    _categoryModel = new CategoryModel();
                return _categoryModel;
            }
        }
    }
}