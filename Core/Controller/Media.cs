﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Core
{
    class Media : Sinbo.Controller
	{
		public Media()
		{
			this.Access = Right.Administrator;
		}
		public void home(string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			JabinfoView view = new JabinfoView("core_media_home", this.JabinfoContext);
			view.Variable["mediaList"] =mediaModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
			view.Variable["size"] = 30;
			view.Variable["total"] = mediaModel.Count(string.Empty);
			view.Render();
		}

		public void add()
		{
			if (this.IsPost)
			{
				mediaModel.Insert(this.Post);
                Output("#","提交成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_media_add", this.JabinfoContext);
			view.Render();
		}

		public void edit(string media_id)
		{
			if (media_id == null)
			{
				mediaModel.Update(this.Post);
                Output("#","修改成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_media_edit", this.JabinfoContext);
			view.Variable["media"] = new MediaVO(media_id);
			view.Render();
		}

		public void remove(string media_id)
		{
			mediaModel.Delete(media_id);
		}

		private MediaModel _mediaModel = null;
		private MediaModel mediaModel
		{
			get
			{
				if (_mediaModel == null)
					_mediaModel = new MediaModel();
				return _mediaModel;
			}
		}
	}
}