/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2011-12-15 13:26:23
 */
using System;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;


namespace Jabinfo.Core
{
    class Mods : Sinbo.Controller
    {
        #region	Constructor
        public Mods()
        {
            this.Access = Right.Administrator;
        }
        #endregion

        #region	home
        public void home(string lan)
        {
            if (string.IsNullOrEmpty(lan))
                lan = "0";
            string where=string.Format("lan='{0}'",lan);
            int count = this.modsModel.Count(where);
            if (count == 0)
            {
                JabinfoKeyValue data = new JabinfoKeyValue();
                data["mods_id"] = Jabinfo.Help.Basic.JabId;
                data["title"] = "首页";
                data["target"] = "";
                data["jindex"] = "0";
                data["action"] = "";
                data["lan"] = lan;
                data["jtype"] = "";
                this.modsModel.Insert(data);
            }
            JabinfoView view = new JabinfoView("core_mods_home", this.JabinfoContext);
            view.Variable["modsList"] = modsModel.Select(0, 30, where, "jindex asc");
            view.Variable["lan"] = lan;
            view.Variable["type"] = Jabinfo.Help.Config.Get("core.mods");
            view.Render();
        }
        #endregion

        #region	edit
        public void edit(string mods_id)
        {
            if (mods_id == null)
            {
                modsModel.Update(this.Post);
                Output("#","修改成功");
                return;
            }
            ModsVO modsVO = new ModsVO(mods_id);
            JabinfoView view = new JabinfoView("core_mods_edit", this.JabinfoContext);
            view.Variable["mods"] = modsVO;
            view.Variable["class"] = modsVO.Class.Replace("\"", "&#34;");
            view.Render();
        }
        #endregion

        #region	remove
        public void remove(string mods_id)
        {
            modsModel.Delete(mods_id);
        }
        #endregion

        #region	_model
        private ModsModel _model = null;
        private ModsModel modsModel
        {
            get
            {
                if (_model == null)
                    _model = new ModsModel();
                return _model;
            }
        }
        #endregion
    }
}