﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Core
{
    class Monitor : Sinbo.Controller
	{
		public Monitor()
		{
			this.Access = Right.Administrator;
		}
		public void home(string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			JabinfoView view = new JabinfoView("core_monitor_home", this.JabinfoContext);
			view.Variable["monitorList"] =monitorModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
			view.Variable["size"] = 30;
			view.Variable["total"] = monitorModel.Count(string.Empty);
			view.Render();
		}

		public void add()
		{
			if (this.IsPost)
			{
				monitorModel.Insert(this.Post);
                Output("#","提交成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_monitor_add", this.JabinfoContext);
			view.Render();
		}

		public void edit(string monitor_id)
		{
			if (monitor_id == null)
			{
				monitorModel.Update(this.Post);
                Output("#","修改成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_monitor_edit", this.JabinfoContext);
			view.Variable["monitor"] = new MonitorVO(monitor_id);
			view.Render();
		}

		public void remove(string monitor_id)
		{
			monitorModel.Delete(monitor_id);
		}

		private MonitorModel _monitorModel = null;
		private MonitorModel monitorModel
		{
			get
			{
				if (_monitorModel == null)
					_monitorModel = new MonitorModel();
				return _monitorModel;
			}
		}
	}
}