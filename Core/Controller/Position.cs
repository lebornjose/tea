﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;

namespace Jabinfo.Core
{
    class Position : Sinbo.Controller
	{
		public Position()
		{
			this.Access = Right.Administrator;
		}
		public void home(string media_id,string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
            if (media_id == null)
                media_id = Jabinfo.Help.Config.Get("system.core", "skin");
			string where = string.Format("media_id='{0}'",media_id);
			JabinfoView view = new JabinfoView("core_position_home", this.JabinfoContext);
            view.Variable["positionList"] = positionModel.Select(index, 30, where, "position_id desc");
			view.Variable["index"] = index;
			view.Variable["media_id"] = media_id;
			view.Variable["size"] = 30;
			view.Variable["total"] = positionModel.Count(where);
			view.Render();
		}

		public void add(string media_id)
		{
			if (this.IsPost)
			{
				if(this.Post["status"]==null)
				{
					if(this.Post["position_id"] == null || this.Post["position_id"].Trim()==string.Empty)
					{
                        Output("请输入广告位编号");
						return;
					}
					else
					{
						PositionVO checkVO = new PositionVO(this.Post["position_id"]);
						if(checkVO.Position_id != null)
						{
                            Output("该广告位已存在，请重新输入");
							return;
						}
					}
				}
				else
				{
					this.Post["position_id"] = Jabinfo.Help.Basic.JabId;
				}
				positionModel.Insert(this.Post);
                Output("#","提交成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_position_add", this.JabinfoContext);
			view.Variable["media_id"] = media_id;
			view.Render();
		}

		public void edit(string position_id)
		{
			if (position_id == null)
			{
				positionModel.Update(this.Post);
                Output("#","修改成功");
				return;
			}
			JabinfoView view = new JabinfoView("core_position_edit", this.JabinfoContext);
			view.Variable["position"] = new PositionVO(position_id);
			view.Render();
		}
		
		public void clear(string position_id)
		{
			JabinfoKeyValue data = new JabinfoKeyValue();
			data["position_id"] = position_id;
			data["content"] = "";
			this.positionModel.Update(data);
            Output("#","更新完成");
		}

		public void remove(string position_id)
		{
			AdvertiseModel advertiseModel = new AdvertiseModel();
			int count = advertiseModel.Count(string.Format("position_id='{0}'",position_id));
			if(count>0)
			{
                Output("删除失败，请先删除广告内容");
				return;
			}
			positionModel.Delete(position_id);
            Output("#","删除成功");
		}

        public void search_option()
        {
            JabinfoView view = new JabinfoView("core_position_search_option", this.JabinfoContext);
            view.Render();            
        }

        public void search(string start)
        {
            int index = 0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            string where = string.Empty;
            string position_id = string.Empty;
            string describe = string.Empty;
            if (this.IsPost)
            {
                position_id = this.Post["position_id"];
                describe = this.Post["describe"];
                this.JabinfoContext.Session.Add("position_id", position_id);
                this.JabinfoContext.Session.Add("describe", describe);
            }
            else
            {
                where = this.JabinfoContext.Session.Get("position_id").ToString();
                where = this.JabinfoContext.Session.Get("describe").ToString();
            }
            PositionModel positionModel = new PositionModel();
            JabinfoView view = new JabinfoView("core_position_search", this.JabinfoContext);
            view.Variable["positionList"] = positionModel.Like(index, 30, string.Empty, position_id,describe, "position_id desc");
            view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = positionModel.Count_like(string.Empty,position_id,describe);
            view.Render();
        }

        private PositionModel _positionModel = null;
		private PositionModel positionModel
		{
			get
			{
				if (_positionModel == null)
					_positionModel = new PositionModel();
				return _positionModel;
			}
		}
	}
}