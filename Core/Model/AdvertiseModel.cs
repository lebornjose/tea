﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
	class AdvertiseModel : Sinbo.Model<AdvertiseVO>
	{
		public AdvertiseModel()
			: base("core", "advertise")
		{
		}


        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AdvertiseVO Get(string advertise_id)
        {
            Object cache = CacheGet(advertise_id);
            if (cache != null)
                return (AdvertiseVO)cache;
            AdvertiseVO advertiseVO = new AdvertiseVO(advertise_id);
            if(advertiseVO.Advertise_id==null)
                return null;
            CacheSet(advertise_id, advertiseVO);
            return advertiseVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AdvertiseVO Get(DataRow row)
        {
            return new AdvertiseVO(row);
        }
        #endregion
		
		/// <summary>
		/// 查询有效的广告位
		/// </summary>
		public AdvertiseVO[] Select(string position_id,int length)
		{
            if (length == 0)  // 获取所有广告
            {
                return Collection(ISelect("advertise_id").Where(string.Format("position_id='{0}' and (endtime=0 or (starttime<{1} and endtime>{1}))", position_id, Jabinfo.Help.Date.Now)).Order("jindex asc").All());
            }
            else  // 获取有效广告
            {
               return Collection(ISelect("advertise_id").Where(string.Format("position_id='{0}' and (endtime=0 or (starttime<{1} and endtime>{1}))", position_id, Jabinfo.Help.Date.Now)).Limit(0, length).Order("jindex asc").All());
            }
			
		}

		/// <summary>
		/// 分页查询数据
		/// </summary>
		/// <param name="index">页面索引</param>
		/// <param name="length">分页长度</param>
		/// <returns></returns>
		public AdvertiseVO[] Select(int index, int length ,string where ,string order)
		{
            return Collection(ISelect("advertise_id").Where(where).Order(order).Limit(index, length).All());
		}

        /// <summary>
        /// 查询所有有效的广告位
        /// </summary>
        public AdvertiseVO[] Select(string where)
		{
            return Collection(ISelect("advertise_id").Where(where).All());
		}
		/// <summary>
		/// 统计数据
		/// </summary>
		public int Count(string where)
		{
			return ISelect().Where(where).Count();
		}
		/// <summary>
		/// 插入数据
		/// </summary>
		public string Insert(JabinfoKeyValue data)
		{
			if(!string.IsNullOrEmpty(data["all"]))
			{
				data["starttime"] = "0";
				data["endtime"] = "0";
			}
			else
			{
				int starttime = Jabinfo.Help.Date.StringToDate(data["starttime"]);
				int endtime = Jabinfo.Help.Date.StringEndDate(data["endtime"]);
				if(endtime<=starttime)
				{
					return "-1";//结束时间大于开始时间
				}
				data["starttime"] = starttime.ToString();
				data["endtime"] = endtime.ToString();
			}
			AdvertiseVO advertiseVO = new AdvertiseVO();
			advertiseVO.Insert(data);
			return data["advertise_id"];
		}
		/// <summary>
		/// 更新数据
		/// </summary>
		public string Update(JabinfoKeyValue data)
		{
			AdvertiseVO advertiseVO = new AdvertiseVO();
			advertiseVO.Update(data);
			CacheDelete(data["advertise_id"]);
			return data["advertise_id"];
		}
		/// <summary>
		/// 删除数据
		/// </summary>
		public void Delete(string advertise_id)
		{
			IDelete().Where("advertise_id", advertise_id, DataType.Char, 24).Excute();
			CacheDelete(advertise_id);
		}
	}
}