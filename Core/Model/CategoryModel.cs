﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-15 16:51:47
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
    class CategoryModel : Sinbo.Model<CategoryVO>
    {
        public CategoryModel()
            : base("core", "category")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override CategoryVO Get(string category_id)
        {
            Object cache = CacheGet(category_id);
            if (cache != null)
                return (CategoryVO)cache;
            CategoryVO categoryVO = new CategoryVO(category_id);
            if(categoryVO.Category_id==null)
                return null;
            CacheSet(category_id, categoryVO);
            return categoryVO;
        }*/
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override CategoryVO Get(DataRow row)
        {
            return new CategoryVO(row);
        }
        #endregion

        public CategoryVO[] Select(string parent_id)
        {
            return Collection(ISelect("category_id").Where(string.Format("parent_id='{0}'", parent_id)).Order("index", "asc").All());
        }
       
        /// <summary>
        /// 查询数据
        /// </summary>
        public CategoryVO[] Select(int length, string where, string order)
        {
            return Collection(ISelect("category_id").Where(where).Order(order).Limit(0, length).All());
        }
        public CategoryVO[] Select(int index, int length, string order)
        {
            return Collection(ISelect("category_id").Order(order).Limit(index, length).All());
        }
        public CategoryVO[] Select(int index, int length)
        {
            return Collection(ISelect("category_id").Limit(index, length).All());
        }
		public CategoryVO[] Select(int index, int length,string where,string order)
		{
			return Collection(ISelect("*").Where(where).Order(order).Limit(index, length).All());
		}
        public string[] IDS(string where,string order,int length)
        {
            return ISelect("category_id").Where(where).Limit(0, length).Order(order).Ids();
        }
        public CategoryVO[] Like(int length, string keyword, string keyword2, string where, string order)
        {
            return Collection(ISelect("category_id").Like("keyword", keyword, Jabinfo.DataType.Varchar, 200).
                Like("title", keyword2, Jabinfo.DataType.Varchar, 200).Where(where).Order(order).Limit(0, length).All());
        }

        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["category_id"] = Help.Basic.AutoId("cat_tag");
            CategoryVO categoryVO = new CategoryVO();
            categoryVO.Insert(data);
            return data["category_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            CategoryVO categoryVO = new CategoryVO();
            categoryVO.Update(data);
            //CacheDelete(data["category_id"]);
            return data["category_id"];
        }

        /// <summary>
        /// 清空所有价格区间字段
        /// </summary>
        public void ClearPrice()
        {
            this.Query.Update(this.Table).Set("prices", "", DataType.Varchar, 200).Excute();
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string category_id)
        {
            IDelete().Where("category_id", category_id, DataType.Char, 24).Excute();
            //CacheDelete(category_id);
        }
    }
}