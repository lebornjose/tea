﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
    class MediaModel : Sinbo.Model<MediaVO>
    {
        public MediaModel()
            : base("core", "media")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override MediaVO Get(string media_id)
        {
            Object cache = CacheGet(media_id);
            if (cache != null)
                return (MediaVO)cache;
            MediaVO mediaVO = new MediaVO(media_id);
            if(mediaVO.Media_id==null)
                return null;
            CacheSet(media_id, mediaVO);
            return mediaVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override MediaVO Get(DataRow row)
        {
            return new MediaVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public MediaVO[] Select()
        {
            return Collection(ISelect("media_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public MediaVO[] Select(int index, int length ,string where ,string order)
        {
            return Collection(ISelect("media_id").Where(where).Order(order).Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["media_id"] = Help.Basic.JabId;
            MediaVO mediaVO = new MediaVO();
            mediaVO.Insert(data);
            return data["media_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            MediaVO mediaVO = new MediaVO();
            mediaVO.Update(data);
            CacheDelete(data["media_id"]);
            return data["media_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string media_id)
        {
            IDelete().Where("media_id", media_id, DataType.Char, 24).Excute();
            CacheDelete(media_id);
        }
    }
}