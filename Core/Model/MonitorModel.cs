﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
    class MonitorModel : Sinbo.Model<MonitorVO>
    {
        public MonitorModel()
            : base("core", "monitor")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override MonitorVO Get(string monitor_id)
        {
            Object cache = CacheGet(monitor_id);
            if (cache != null)
                return (MonitorVO)cache;
            MonitorVO monitorVO = new MonitorVO(monitor_id);
            if(monitorVO.Monitor_id==null)
                return null;
            CacheSet(monitor_id, monitorVO);
            return monitorVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override MonitorVO Get(DataRow row)
        {
            return new MonitorVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public MonitorVO[] Select()
        {
            return Collection(ISelect("monitor_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public MonitorVO[] Select(int index, int length ,string where ,string order)
        {
            return Collection(ISelect("monitor_id").Where(where).Order(order).Limit(index, length).All());
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["monitor_id"] = Help.Basic.JabId;
            MonitorVO monitorVO = new MonitorVO();
            monitorVO.Insert(data);
            return data["monitor_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            MonitorVO monitorVO = new MonitorVO();
            monitorVO.Update(data);
            CacheDelete(data["monitor_id"]);
            return data["monitor_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string monitor_id)
        {
            IDelete().Where("monitor_id", monitor_id, DataType.Char, 24).Excute();
            CacheDelete(monitor_id);
        }
    }
}