﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
    class PositionModel : Sinbo.Model<PositionVO>
    {
        public PositionModel()
            : base("core", "position")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override PositionVO Get(string position_id)
        {
            Object cache = CacheGet(position_id);
            if (cache != null)
                return (PositionVO)cache;
            PositionVO positionVO = new PositionVO(position_id);
            if(positionVO.Position_id==null)
                return null;
            CacheSet(position_id, positionVO);
            return positionVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override PositionVO Get(DataRow row)
        {
            return new PositionVO(row);
        }
        #endregion
        /// <summary>
        /// 查询数据
        /// </summary>
        public PositionVO[] Select()
        {
            string[] ids = ISelect("position_id").Ids();
            return Collection(ISelect("position_id").All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public PositionVO[] Select(int index, int length ,string where , string order)
        {
            return Collection(ISelect("position_id").Where(where).Order(order).Limit(index, length).All());
        }
        /// <summary>
        /// 分页查询数据
        /// </summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public PositionVO[] Like(int index, int length, string where, string position, string describe, string order)
        {
            return Collection(ISelect("position_id").Where(where).Like("position_id", position, DataType.Varchar, 200).Like("describe", describe, DataType.Varchar, 200).Order(order).Limit(index, length).All());
        }

        public int Count_like(string where, string position, string describe)
        {
            return ISelect().Where(where).Like("position_id", position, DataType.Varchar, 200).Like("describe", describe, DataType.Varchar, 200).Count();
        }
        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            PositionVO positionVO = new PositionVO();
            positionVO.Insert(data);
            return data["position_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            PositionVO positionVO = new PositionVO();
            positionVO.Update(data);
            CacheDelete(data["position_id"]);
            return data["position_id"];
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string position_id)
        {
            IDelete().Where("position_id", position_id, DataType.Char, 24).Excute();
            CacheDelete(position_id);
        }
    }
}