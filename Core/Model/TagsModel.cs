﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-15 16:51:47
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
    class TagsModel : Sinbo.Model<TagsVO>
    {
        public TagsModel()
            : base("core", "tags")
        {
        }

        #region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override TagsVO Get(string tags_id)
        {
            Object cache = CacheGet(tags_id);
            if (cache != null)
                return (TagsVO)cache;
            TagsVO tagsVO = new TagsVO(tags_id);
            if(tagsVO.Tags_id==null)
                return null;
            CacheSet(tags_id, tagsVO);
            return tagsVO;
        }
        */
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override TagsVO Get(DataRow row)
        {
            return new TagsVO(row);
        }
        #endregion
        /// <summary>
        /// 获取导航栏一个标签
        /// </summary>
        /// <param name="tags_id"></param>
        /// <returns></returns>
        public TagsVO Get_nav(string tags_id)
        {
            return Collection(ISelect("tags_id").Where(string.Format("tags_id='{0}'", tags_id)).All())[0];
        }
        /// <summary>
        /// 查询一个分类下的标签
        /// </summary>
        public TagsVO[] Select(string category_id)
        {
            return Collection(
                ISelect("tags_id").Where("category_id", category_id, DataType.Char, 24).Order("index", "asc").Order("tags_id", "desc").All()
                );
        }

        public TagsVO[] Sons(string parent_id)
        {
            return Collection(
                ISelect("tags_id").Where("parent_id", parent_id, DataType.Char, 24).Order("index", "asc").Order("tags_id", "desc").All()
                );
        }
        /// <summary>
        /// 查询一个分类下的标签
        /// </summary>
        public TagsVO[] Select(string category_id, int number)
        {
            return Collection(ISelect("tags_id").Where(string.Format("category_id='{0}'", category_id)).Limit(0, number).Order("index", "asc").Order("tags_id", "desc").All());
        }
        /// <summary>
        /// 获取所有标签
        /// </summary>
        public TagsVO[] Select(int index, int length)
        {
            return Collection(ISelect("tags_id").Where(string.Format("1 limit {0},{1}", index, length)).All());
        }
        /// <summary>
        /// 获取所有标签
        /// </summary>
        public TagsVO[] Select(int index, int length, string order)
        {
            return Collection(ISelect("tags_id").Order(order).Limit(index, length).All());
        }

        public string[] IDS(string where)
        {
            return ISelect("tags_id").Where(where).Ids();
        }
        /// <summary>
        /// 关键字查询标签
        /// </summary>
        public TagsVO[] Like(int index, int length, string where)
        {
            return Collection(ISelect("tags_id").Where(where).Limit(index, length).All());
        }
        public TagsVO[] Like(int index, int length, string keyword, string keywoerd2, string order)
        {
            return Collection(ISelect("tags_id").Like("keyword", keyword, Jabinfo.DataType.Varchar, 200).
                Like("title", keywoerd2, Jabinfo.DataType.Varchar, 200).Limit(index, length).Order(order).All());
        }
        /// <summary>
        /// 查询某个分类和父级分类的标签总和
        /// </summary>
        /// <param name="category_id">某个分类</param>
        /// <param name="parent_id">父级分类</param>
        /// <returns></returns>
        public TagsVO[] Select(string category_id, string parent_id)
        {
            return Collection(ISelect("tags_id").Where(string.Format("category_id='{0}' or category_id='{1}'", category_id, parent_id)).Order("index", "asc").Order("tags_id", "desc").All());
        }

        /// <summary>
        /// 统计数据
        /// </summary>
        public int Count()
        {
            return ISelect().Count();
        }

        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
        /// <summary>
        /// 插入数据
        /// </summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["tags_id"] = Help.Basic.AutoId("cat_tag");
            TagsVO tagsVO = new TagsVO();
            tagsVO.Insert(data);
            return data["tags_id"];
        }
        /// <summary>
        /// 更新数据
        /// </summary>
        public string Update(JabinfoKeyValue data)
        {
            TagsVO tagsVO = new TagsVO();
            tagsVO.Update(data);
            return data["tags_id"];
        }
        /// <summary>
        /// 清空所有标签价格区间
        /// </summary>
        public void ClearPrice()
        {
            this.Query.Update(this.Table).Set("prices", "", DataType.Varchar, 200).Excute();
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        public void Delete(string tags_id)
        {
            IDelete().Where("tags_id", tags_id, DataType.Char, 24).Excute();
        }

        public void DeleteGroup(string category_id)
        {
            IDelete().Where("category_id", category_id, DataType.Char, 24).Excute();
        }
    }
}