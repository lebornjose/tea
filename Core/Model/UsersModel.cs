﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-2-28 上02时35分41秒
 */
using System;
using System.Data;
using Jabinfo.Core.VO;

namespace Jabinfo.Core.Model
{
    class UsersModel : Sinbo.Model<UsersVO>
    {
		#region constructor
        public UsersModel()
            : base("core", "users")
        {
        }
		#endregion

		#region Get
        /*
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override UsersVO Get(string uid)
        {
            Object cache = CacheGet(uid);
            if (cache != null)
                return (UsersVO)cache;
            UsersVO usersVO = new UsersVO(uid);
            if(usersVO.Uid==null)
                return null;
            CacheSet(uid, usersVO);
            return usersVO;
        }
        */
		/// <summary>
        /// 获取一条记录
        ///</summary>
        public override UsersVO Get(DataRow row)
        {
            return new UsersVO(row);
        }
		#endregion

		#region Select
		// <summary>
		// 查询数据
		//</summary>
		public UsersVO[] Select(string where ,string order)
		{
			return Collection(
				ISelect("uid").Where(where).Order(order).All()
			);
		}
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public UsersVO[] Select(int index,int length,string where ,string order)
        {
			return Collection(
				ISelect("uid").Where(where).Order(order).Limit(index, length).All()
            );
        }
		#endregion

        #region 搜索团队内成员
        public UsersVO[] Search(string team_id, string keyword)
        {
            return Collection(
                ISelect("*").Where("email",team_id,DataType.Varchar,50).OrLike("username",keyword,DataType.Char,50).OrLike("nick",keyword,DataType.Char,50).All()
                );
        }
        #endregion

        #region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion
    }
}