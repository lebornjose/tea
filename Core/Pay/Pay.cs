﻿using System;
using System.Collections;
using System.Collections.Generic;
using tenpay;
using Alipay;
using Jabinfo.Basic.Model;
using Jabinfo.Basic.VO;

namespace Jabinfo.Core
{
    class Pay : Sinbo.Controller
    {
        #region 构造函数
        public Pay()
        {
            this.Skin = true;
        }
        #endregion

        #region 结算
        public void create()
        {
            string type = this.Post["type"];
            double money = Convert.ToDouble(this.Post["money"]);
            if (money < 0)
            {
                this.Jump("basic/member/pay", "请输入正确的金额");
                return;
            }

            JabinfoKeyValue data = new JabinfoKeyValue();
            data["uid"] = this.Uid;
            data["reach"] = "0";
            data["message"] = "帐户充值";
            data["object_id"] = "pay";
            data["current"] = money.ToString();
            CashModel cashModel = new CashModel();
            string cash_id = cashModel.Insert(data);

            Output("请稍等，正在跳转到支付平台...");
            string payURL = string.Empty;
            switch (type)
            {
                case "2":
                    payURL = this.AliCreate(cash_id, money);
                    break;
                case "3":
                    payURL = this.TenCreate(cash_id, money);
                    break;
                case "4":
                    payURL = string.Format("team/home/paypal/{0}/{1}",cash_id, money);
                    break;
            }
            if (payURL == string.Empty)
            {
                this.Jump("basic/member/pay", "请选择正确的支付方式");
                return;
            }
            this.Jump(payURL);
        }

        #endregion

        #region 财付通 

        #region 创建财付通交易
        private string TenCreate(string cash_id, double money)
        {
            JabinfoKeyValue data = Jabinfo.Help.Config.Get("enum.pay");
            //商户号
            string bargainor_id = data["bargainor_id"];
            //密钥
            string key = data["key"];
            string domain = Jabinfo.Help.Config.Get("system.core", "domain");
            //当前时间 yyyyMMdd
            string date = DateTime.Now.ToString("yyyyMMdd");
            //生成订单10位序列号，此处用时间和随机数生成，商户根据自己调整，保证唯一,strReq必须为数字
            string strReq = DateTime.Now.ToString("HHmmss") + TenpayUtil.BuildRandomStr(4);

            //商户订单号，不超过32位，财付通只做记录，不保证唯一性
            string sp_billno = cash_id;

            //财付通订单号，10位商户号+8位日期+10位序列号，需保证全局唯一
            string transaction_id = bargainor_id + date + strReq;

            string return_url = string.Format("{0}/{1}", domain, data["tenreturn"]);
            //创建PayRequestHandler实例
			PayRequestHandler reqHandler = null;

            //设置密钥
            reqHandler.setKey(key);

            //初始化
            reqHandler.init();
            //-----------------------------
            //设置支付参数
            //-----------------------------
            reqHandler.setParameter("bargainor_id", bargainor_id);			//商户号
            reqHandler.setParameter("sp_billno", sp_billno);				//商家订单号
            reqHandler.setParameter("transaction_id", transaction_id);		//财付通交易单号
            reqHandler.setParameter("return_url", return_url);				//支付通知url
            reqHandler.setParameter("desc", "帐户充值_" + cash_id);	//商品名称
            double fen = money * 100;
            reqHandler.setParameter("total_fee", fen.ToString());						//商品金额,以分为单位

            //用户ip,测试环境时不要加这个ip参数，正式环境再加此参数
            reqHandler.setParameter("spbill_create_ip", this.JabinfoContext.Request.Ip);
            //获取请求带参数的url
            return reqHandler.getRequestURL();
        }
        #endregion

        #region 财付通回调
        /// <summary>
        /// 财付通回调
        /// </summary>
        public void tenreturn()
        {
            JabinfoKeyValue data = Jabinfo.Help.Config.Get("enum.pay");
            //创建PayResponseHandler实例
			PayResponseHandler resHandler = null;
            resHandler.setKey(data["key"]);
            //判断签名
            if (resHandler.isTenpaySign())
            {
                //支付结果
                string pay_result = resHandler.getParameter("pay_result");
                string order_id = resHandler.getParameter("sp_billno");
                string total_fee = resHandler.getParameter("total_fee");


                string payMoney = (double.Parse(total_fee) / 100).ToString();

                if ("0".Equals(pay_result))
                {
					string domain = "";
                }
                else
                {

                }
                Output("支付失败");
            }
            else
            {
                Output("认证签名失败");
            }
        }
        #endregion

        #endregion
      
        #region 支付宝
        #region 创建支付宝交易
        private string AliCreate(string cash_id,double money)
        {
            JabinfoKeyValue data = Jabinfo.Help.Config.Get("enum.pay");
            string partner = data["partner"];                                     //合作身份者ID
            string key = data["security_code"];                         //安全检验码

            string seller_email = data["seller_email"];                             //签约支付宝账号或卖家支付宝帐户
            string input_charset = "utf-8";                                          //字符编码格式 目前支持 gbk 或 utf-8
            string domain = Jabinfo.Help.Config.Get("system.core", "domain");

            string notify_url = string.Format("{0}/{1}", domain, data["alinotify"]); //交易过程中服务器通知的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数
            string return_url = string.Format("{0}/{1}", domain, data["alireturn"]); //付完款后跳转的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数
            string show_url = "";                //网站商品的展示地址，不允许加?id=123这类自定义参数

            string sign_type = "MD5";                                                //加密方式 不需修改
            string antiphishing = "0";                                               //防钓鱼功能开关，'0'表示该功能关闭，'1'表示该功能开启。默认为关闭
            //一旦开启，就无法关闭，根据商家自身网站情况请慎重选择是否开启。
            //申请开通方法：联系我们的客户经理或拨打商户服务电话0571-88158090，帮忙申请开通
            //若要使用防钓鱼功能，建议使用POST方式请求数据

            ///////////////////////以下参数是需要通过下单时的订单数据传入进来获得////////////////////////////////
            //必填参数
            string out_trade_no = cash_id;  //请与贵网站订单系统中的唯一订单号匹配
            string subject = "在线充值_" + cash_id;                                    //订单名称，显示在支付宝收银台里的“商品名称”里，显示在支付宝的交易管理的“商品名称”的列表里。
            
            string body = "帐户金额";                   //订单描述、订单详细、订单备注，显示在支付宝收银台里的“商品描述”里

            string total_fee = money.ToString();                                       //订单总金额，显示在支付宝收银台里的“应付总额”里

            //扩展功能参数——网银提前
            string paymethod = "bankPay";                                   //默认支付方式，四个值可选：bankPay(网银); cartoon(卡通); directPay(余额); CASH(网点支付)
            string defaultbank = "CMB";                                     //默认网银代号，代号列表见c

            //扩展功能参数——防钓鱼
            string encrypt_key = "";                                        //防钓鱼时间戳，初始值
            string exter_invoke_ip = "";                                    //客户端的IP地址，初始值
            if (antiphishing == "1")
            {
                encrypt_key = AlipayFunction.Query_timestamp(partner);
                exter_invoke_ip = "";                                       //获取客户端的IP地址，建议：编写获取客户端IP地址的程序
            }

            //扩展功能参数——其他
            string extra_common_param = "";                //自定义参数，可存放任何内容（除=、&等特殊字符外），不会显示在页面上
            string buyer_email = "";			                            //默认买家支付宝账号

            //扩展功能参数——分润(若要使用，请按照注释要求的格式赋值)
            string royalty_type = "";                                   //提成类型，该值为固定值：10，不需要修改
            string royalty_parameters = "";
            //提成信息集，与需要结合商户网站自身情况动态获取每笔交易的各分润收款账号、各分润金额、各分润说明。最多只能设置10条
            //提成信息集格式为：收款方Email_1^金额1^备注1|收款方Email_2^金额2^备注2
            //如：
            //royalty_type = "10";
            //royalty_parameters = "111@126.com^0.01^分润备注一|222@126.com^0.01^分润备注二";

            //扩展功能参数——自定义超时(若要使用，请按照注释要求的格式赋值)
            //该功能默认不开通，需联系客户经理咨询
            string it_b_pay = "";  //超时时间，不填默认是15天。八个值可选：1h(1小时),2h(2小时),3h(3小时),1d(1天),3d(3天),7d(7天),15d(15天),1c(当天)

            //构造请求函数
            AlipayService aliService = new AlipayService(partner, seller_email, return_url, notify_url, show_url, out_trade_no, subject, body, total_fee, paymethod, defaultbank, encrypt_key, exter_invoke_ip, extra_common_param, buyer_email, royalty_type, royalty_parameters, it_b_pay, key, input_charset, sign_type);
            //GET方式传递
            return aliService.Create_url();
        }
        #endregion

        #region 支付宝通知页面
        /// <summary>
        /// 支付宝通知页面
        /// </summary>
        public void alinotify()
        {
            int i = 0;
            ArrayList sArray = new ArrayList();
            //Load Form variables into NameValueCollection variable.
            JabinfoKeyValue coll = this.Post;

            // Get names of all forms into a string array.
            String[] requestItem = coll.Keys;
            String returnMessage = "";
            for (i = 0; i < requestItem.Length; i++)
            {
               }

            ///////////////////////以下参数是需要设置的相关配置参数，设置后不会更改的//////////////////////
            JabinfoKeyValue data = Jabinfo.Help.Config.Get("enum.pay");
            string partner = data["partner"];                //合作身份者ID
            string key = data["security_code"];    //安全检验码
            string input_charset = "utf-8";                     //字符编码格式 目前支持 gb2312 或 utf-8
            string sign_type = "MD5";                        //加密方式 不需修改
            string transport = "http";                         //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
            //////////////////////////////////////////////////////////////////////////////////////////////
            SortedDictionary<string, string> sArrary = getRequestPost();

            if (sArray.Count > 0)//判断是否有带返回参数
            {
                AlipayNotify aliNotify = new AlipayNotify(sArrary, this.Post["notify_id"], partner, key, input_charset, sign_type, transport);
                string responseTxt = aliNotify.ResponseTxt; //获取远程服务器ATN结果，验证是否是支付宝服务器发来的请求
                string sign = this.Post["sign"];         //获取支付宝反馈回来的sign结果
                string mysign = aliNotify.Mysign;           //获取通知返回后计算后（验证）的加密结果

                //写日志记录（若要调试，请取消下面两行注释）
                //string sWord = "responseTxt=" + responseTxt + "\n notify_url_log:sign=" + this.Post["sign"] + "&mysign=" + mysign + "\n notify回来的参数：" + AlipayFunction.Create_linkstring(sArray);
                //AlipayFunction.log_result(Server.MapPath("log/" + DateTime.Now.ToString().Replace(":", "")) + ".txt", sWord);

                //判断responsetTxt是否为ture，生成的签名结果mysign与获得的签名结果sign是否一致
                //responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
                //mysign与sign不等，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关

                if (responseTxt == "true" && sign == mysign)//验证成功
                {
                    //获取支付宝的通知返回参数
                    string trade_no = this.Post["trade_no"];         //支付宝交易号
                    string order_no = this.Post["out_trade_no"];     //获取订单号
                    string total_fee = this.Post["total_fee"];       //获取总金额
                    string subject = this.Post["subject"];           //商品名称、订单名称
                    string body = this.Post["body"];                 //商品描述、订单备注、描述
                    string buyer_email = this.Post["buyer_email"];   //买家支付宝账号
                    string trade_status = this.Post["trade_status"]; //交易状态
                    
                    //假设：
                    //sOld_trade_status="0"	表示订单未处理；
                    //sOld_trade_status="1"	表示交易成功（TRADE_FINISHED/TRADE_SUCCESS）
                    if (this.Post["trade_status"] == "TRADE_SUCCESS")
                    {
                        Output("success");
                    }
                    else
                    {
                        Output("success");  //其他状态判断。普通即时到帐中，其他状态不用判断，直接打印success。
                    }
                    CashModel cashModel = new CashModel();
                    CashVO cashVO = new CashVO(order_no);
                    if (cashVO.Reach==0)//如果未支付，更新状态
                    {
                        //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
                        BusinessModel businessModel = new BusinessModel();
                        BusinessVO businessVO = new BusinessVO(cashVO.Uid);

                        JabinfoKeyValue cash = new JabinfoKeyValue();
                        cash["cash_id"] = order_no;
                        cash["latest"] = (businessVO.Latest + cashVO.Current).ToString();
                        cash["reach"] = Jabinfo.Help.Date.Now.ToString();
                        cashModel.Update(cash);

                        
                        JabinfoKeyValue busi = new JabinfoKeyValue();
                        busi["uid"] = cashVO.Uid;
                        busi["latest"] = (businessVO.Latest + cashVO.Current).ToString();
                        businessModel.Update(busi);
                    }
                    else
                    {
                        JabinfoLoger.Error("支付宝重复回调异常", string.Format("当前订单编号{0}", cashVO.Cash_id));
                    }
                }
                else//验证失败
                {
                    Output("fail");
                }
            }
            else
            {
                Output("无通知参数");
            }
        }
        private SortedDictionary<string, string> getRequestPost()
        {
            int i = 0;
            SortedDictionary<string, string> sArray = new SortedDictionary<string, string>();
            String[] requestItem = this.Post.Keys;

            for (i = 0; i < this.Post.Count; i++)
            {
                sArray.Add(this.Post.Keys[i], this.Post.Values[i]);
            }

            return sArray;
        }
        #endregion

        #region 支付宝返回页面
        /// <summary>
        /// 支付宝返回页面
        /// </summary>
        public void ali_return()
        {

        }
        #endregion
        #endregion

    }
}