﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010-6-15 16:51:48
 */
using System;

namespace Jabinfo.Core
{
    /// <summary>
    /// 控制器注册
    /// </summary>
    public class Register
    {
        private static Register _Instance = null;
        public static Register Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new Register();
                return _Instance;
            }
        }
        private Register() { }

        public object category { get { return new Category(); } }

        public object monitor { get { return new Monitor(); } }

        public object position { get { return new Position(); } }

        public object advertise { get { return new Advertise(); } }

        public object media { get { return new Media(); } }
        
        public Jabinfo.Core.API api { get { return Jabinfo.Core.API.Instance; } }
    }
}