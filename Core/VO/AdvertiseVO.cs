/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2011/5/16 14:26:48
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// advertise数据元
    /// </summary>
    [Serializable]
    public class AdvertiseVO
    {
        #region	属性


        private string _Advertise_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Advertise_id
        {
            get { return _Advertise_id; }
            set { _Advertise_id = value; }
        }

        private string _Position_id;
        /// <summary>
        /// 广告位编号
        /// </summary>
        public string Position_id
        {
            get { return _Position_id; }
            set { _Position_id = value; }
        }

        private int _Starttime;
        /// <summary>
        /// 开始时间
        /// </summary>
        public int Starttime
        {
            get { return _Starttime; }
            set { _Starttime = value; }
        }

        private int _Endtime;
        /// <summary>
        /// 结束时间
        /// </summary>
        public int Endtime
        {
            get { return _Endtime; }
            set { _Endtime = value; }
        }

        private string _Content;
        /// <summary>
        /// 广告内容
        /// </summary>
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private string _Target;
        /// <summary>
        /// 广告目标地址
        /// </summary>
        public string Target
        {
            get { return _Target; }
            set { _Target = value; }
        }

        private int _Click;
        /// <summary>
        /// 点击次数
        /// </summary>
        public int Click
        {
            get { return _Click; }
            set { _Click = value; }
        }

        private string _Title;
        /// <summary>
        /// 广告标题
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Jtype;
        /// <summary>
        /// 类型，0文字，jpg,swf
        /// </summary>
        public string Jtype
        {
            get { return _Jtype; }
            set { _Jtype = value; }
        }

        private string _Summary;
        /// <summary>
        /// 内容摘要
        /// </summary>
        public string Summary
        {
            get { return _Summary; }
            set { _Summary = value; }
        }

        private int _Jindex;
        /// <summary>
        /// 排序
        /// </summary>
        public int Jindex
        {
            get { return _Jindex; }
            set { _Jindex = value; }
        }

        private string _Object_id;
        /// <summary>
        /// 对象编号
        /// </summary>
        public string Object_id
        {
            get { return _Object_id; }
            set { _Object_id = value; }
        }
        #endregion

        /// <summary>
        /// advertise数据元
        /// </summary>
        public AdvertiseVO() { }

        /// <summary>
        /// advertise数据元
        /// </summary>
        /// <param name="advertise_id">数据主键</param>
        public AdvertiseVO(string advertise_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("advertise").Where("advertise_id", advertise_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Advertise_id = dataReader["advertise_id"].ToString();
                this.Position_id = dataReader["position_id"].ToString();
                this.Starttime = Convert.ToInt32(dataReader["starttime"]);
                this.Endtime = Convert.ToInt32(dataReader["endtime"]);
                this.Content = dataReader["content"].ToString();
                this.Target = dataReader["target"].ToString();
                this.Click = Convert.ToInt32(dataReader["click"]);
                this.Title = dataReader["title"].ToString();
                this.Jtype = dataReader["jtype"].ToString();
                this.Summary = dataReader["summary"].ToString();
                this.Jindex = Convert.ToInt32(dataReader["jindex"]);
                this.Object_id = dataReader["object_id"].ToString();
            }
        }
        /// <summary>
        /// advertise
        /// </summary>
        /// <param name="advertise_id">Primary Key</param>
        public AdvertiseVO(DataRow dataReader)
        {
            this.Advertise_id = dataReader["advertise_id"] as string;
            this.Position_id = dataReader["position_id"] as string;
            this.Starttime = Convert.ToInt32(dataReader["starttime"]);
            this.Endtime = Convert.ToInt32(dataReader["endtime"]);
            this.Content = dataReader["content"] as string;
            this.Target = dataReader["target"] as string;
            this.Click = Convert.ToInt32(dataReader["click"]);
            this.Title = dataReader["title"] as string;
            this.Jtype = dataReader["jtype"] as string;
            this.Summary = dataReader["summary"] as string;
            this.Jindex = Convert.ToInt32(dataReader["jindex"]);
            this.Object_id = dataReader["object_id"] as string;
        }
        /// <summary>
        /// advertise插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("advertise").
                        Value("advertise_id", data["advertise_id"], DataType.Char, 24).
                        Value("position_id", data["position_id"], DataType.Char, 24).
                        Value("starttime", data["starttime"], DataType.Int).
                        Value("endtime", data["endtime"], DataType.Int).
                        Value("content", data["content"], DataType.Varchar, 1000).
                        Value("target", data["target"], DataType.Varchar, 100).
                        Value("click", data["click"], DataType.Int).
                        Value("title", data["title"], DataType.Varchar, 100).
                        Value("jtype", data["jtype"], DataType.Varchar, 5).
                        Value("summary", data["summary"], DataType.Varchar, 500).
                        Value("jindex", data["jindex"], DataType.Int).
                        Value("object_id", data["object_id"], DataType.Char, 24).
                        Excute();
        }

        /// <summary>
        /// advertise更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("advertise").
                        Set("position_id", data["position_id"], DataType.Char, 24).
                        Set("starttime", data["starttime"], DataType.Int).
                        Set("endtime", data["endtime"], DataType.Int).
                        Set("content", data["content"], DataType.Varchar, 1000).
                        Set("target", data["target"], DataType.Varchar, 100).
                        Set("click", data["click"], DataType.Int).
                        Set("title", data["title"], DataType.Varchar, 100).
                        Set("jtype", data["jtype"], DataType.Varchar, 5).
                        Set("summary", data["summary"], DataType.Varchar, 500).
                        Set("jindex", data["jindex"], DataType.Int).
                        Set("object_id", data["object_id"], DataType.Char, 24).
                                    Where("advertise_id", data["advertise_id"], DataType.Char, 24).
                        Excute();
        }
    }
}