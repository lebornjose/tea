/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2011/5/10 9:44:31
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// category数据元
    /// </summary>
    [Serializable]
    public class CategoryVO
    {
        #region	属性


        private string _Category_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Category_id
        {
            get { return _Category_id; }
            set { _Category_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Parent_id;
        /// <summary>
        /// 父编号,模块，0CMS，1shop,2社区，4群组,5页面
        /// </summary>
        public string Parent_id
        {
            get { return _Parent_id; }
            set { _Parent_id = value; }
        }

        private int _Index;
        /// <summary>
        /// 排序
        /// </summary>
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        private string _Childen;
        /// <summary>
        /// 是否有子分类,0否，1有
        /// </summary>
        public string Childen
        {
            get { return _Childen; }
            set { _Childen = value; }
        }

        private string _Out_url;
        /// <summary>
        /// 外链接地址
        /// </summary>
        public string Out_url
        {
            get { return _Out_url; }
            set { _Out_url = value; }
        }

        private string _Extend;
        /// <summary>
        /// 保留字段,自定义模板名称,或管理员
        /// </summary>
        public string Extend
        {
            get { return _Extend; }
            set { _Extend = value; }
        }

        private string _Type;
        /// <summary>
        /// 类型，0普通，1外连接
        /// </summary>
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _Describe;
        /// <summary>
        /// 栏目描述
        /// </summary>
        public string Describe
        {
            get { return _Describe; }
            set { _Describe = value; }
        }

        private int _Pagesize;
        /// <summary>
        /// 分页大小
        /// </summary>
        public int Pagesize
        {
            get { return _Pagesize; }
            set { _Pagesize = value; }
        }

        private string _Keyword;
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword
        {
            get { return _Keyword; }
            set { _Keyword = value; }
        }

        private string _Prices;
        /// <summary>
        /// 价格区间
        /// </summary>
        public string Prices
        {
            get { return _Prices; }
            set { _Prices = value; }
        }
        #endregion

        /// <summary>
        /// category数据元
        /// </summary>
        public CategoryVO() { }

        /// <summary>
        /// category数据元
        /// </summary>
        /// <param name="category_id">数据主键</param>
        public CategoryVO(string category_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("category").Where("category_id", category_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Category_id = dataReader["category_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Parent_id = dataReader["parent_id"].ToString();
                this.Index = Convert.ToInt32(dataReader["index"]);
				//this.Childen = dataReader["childen"].ToString();
                this.Out_url = dataReader["out_url"].ToString();
                this.Extend = dataReader["extend"].ToString();
                this.Type = dataReader["type"].ToString();
                this.Describe = dataReader["describe"].ToString();
                this.Pagesize = Convert.ToInt32(dataReader["pagesize"]);
                this.Keyword = dataReader["keyword"].ToString();
				//this.Prices = dataReader["prices"].ToString();
            }
        }
        /// <summary>
        /// category
        /// </summary>
        /// <param name="category_id">Primary Key</param>
        public CategoryVO(DataRow dataReader)
        {
            this.Category_id = dataReader["category_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Parent_id = dataReader["parent_id"] as string;
            this.Index = Convert.ToInt32(dataReader["index"]);
			//this.Childen = dataReader["childen"] as string;
            this.Out_url = dataReader["out_url"] as string;
            this.Extend = dataReader["extend"] as string;
            this.Type = dataReader["type"] as string;
            this.Describe = dataReader["describe"] as string;
            this.Pagesize = Convert.ToInt32(dataReader["pagesize"]);
            this.Keyword = dataReader["keyword"] as string;
			//this.Prices = dataReader["prices"] as string;
        }
        /// <summary>
        /// category插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("category").
                        Value("category_id", data["category_id"], DataType.Char, 24).
                        Value("title", data["title"], DataType.Varchar, 50).
                        Value("parent_id", data["parent_id"], DataType.Char, 24).
                        Value("index", data["index"], DataType.Int).
                        Value("childen", data["childen"], DataType.Char, 1).
                        Value("out_url", data["out_url"], DataType.Varchar, 200).
                        Value("extend", data["extend"], DataType.Varchar, 200).
                        Value("type", data["type"], DataType.Char, 1).
                        Value("describe", data["describe"], DataType.Varchar, 500).
                        Value("pagesize", data["pagesize"], DataType.Int).
                        Value("keyword", data["keyword"], DataType.Varchar, 1000).
                        Value("prices", data["prices"], DataType.Varchar, 500).
                        Excute();
        }

        /// <summary>
        /// category更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("category").
                        Set("title", data["title"], DataType.Varchar, 50).
                        Set("parent_id", data["parent_id"], DataType.Char, 24).
                        Set("index", data["index"], DataType.Int).
                        Set("childen", data["childen"], DataType.Char, 1).
                        Set("out_url", data["out_url"], DataType.Varchar, 200).
                        Set("extend", data["extend"], DataType.Varchar, 200).
                        Set("type", data["type"], DataType.Char, 1).
                        Set("describe", data["describe"], DataType.Varchar, 500).
                        Set("pagesize", data["pagesize"], DataType.Int).
                        Set("keyword", data["keyword"], DataType.Varchar, 1000).
                        Set("prices", data["prices"], DataType.Varchar, 500).
                        Where("category_id", data["category_id"], DataType.Char, 24).
                        Excute();
        }
    }
}