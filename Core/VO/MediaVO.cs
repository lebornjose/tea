﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:24
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// media数据元
    /// </summary>
    [Serializable]
    public class MediaVO
    {
        #region	属性


        private string _Media_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Media_id
        {
            get { return _Media_id; }
            set { _Media_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 媒体名称
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private int _Addtime;
        /// <summary>
        /// 添加时间
        /// </summary>
        public int Addtime
        {
            get { return _Addtime; }
            set { _Addtime = value; }
        }

        private string _Category;
        /// <summary>
        /// 媒体分类,见配置文件
        /// </summary>
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }
        #endregion

        /// <summary>
        /// media数据元
        /// </summary>
        public MediaVO() { }

        /// <summary>
        /// media数据元
        /// </summary>
        /// <param name="media_id">数据主键</param>
        public MediaVO(string media_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("media").Where("media_id", media_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Media_id = dataReader["media_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Addtime = Convert.ToInt32(dataReader["addtime"]);
                this.Category = dataReader["category"].ToString();
            }
        }

        /// <summary>
        /// media
        /// </summary>
        /// <param name="media_id">Primary Key</param>
        public MediaVO(DataRow dataReader)
        {
            this.Media_id = dataReader["media_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            this.Category = dataReader["category"] as string;
        }
        /// <summary>
        /// media插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("media").
                Value("media_id", data["media_id"], DataType.Char, 24).
                Value("title", data["title"], DataType.Varchar, 200).
                Value("addtime", data["addtime"], DataType.Int).
                Value("category", data["category"], DataType.Char, 2).
                Excute();
        }

        /// <summary>
        /// media更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("media").
                Set("title", data["title"], DataType.Varchar, 200).
                Set("addtime", data["addtime"], DataType.Int).
                Set("category", data["category"], DataType.Char, 2).
                Where("media_id", data["media_id"], DataType.Char, 24).
                Excute();
        }
    }
}