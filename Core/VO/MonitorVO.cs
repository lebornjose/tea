﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/24 22:31:25
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// monitor数据元
    /// </summary>
    [Serializable]
    public class MonitorVO
    {
        #region	属性


        private string _Monitor_id;
        /// <summary>
        /// 监控编号
        /// </summary>
        public string Monitor_id
        {
            get { return _Monitor_id; }
            set { _Monitor_id = value; }
        }

        private string _Position_id;
        /// <summary>
        /// 广告位编号
        /// </summary>
        public string Position_id
        {
            get { return _Position_id; }
            set { _Position_id = value; }
        }

        private int _Addtime;
        /// <summary>
        /// 添加时间
        /// </summary>
        public int Addtime
        {
            get { return _Addtime; }
            set { _Addtime = value; }
        }

        private string _Uid;
        /// <summary>
        /// 用户编号
        /// </summary>
        public string Uid
        {
            get { return _Uid; }
            set { _Uid = value; }
        }

        private string _Ip;
        /// <summary>
        /// 访问ip
        /// </summary>
        public string Ip
        {
            get { return _Ip; }
            set { _Ip = value; }
        }

        private string _Islogin;
        /// <summary>
        /// 注册效果,0否,其它是uid
        /// </summary>
        public string Islogin
        {
            get { return _Islogin; }
            set { _Islogin = value; }
        }

        private string _Isbuy;
        /// <summary>
        /// 是否有购买动作,0否,其他是orders_id
        /// </summary>
        public string Isbuy
        {
            get { return _Isbuy; }
            set { _Isbuy = value; }
        }

        private string _Isact;
        /// <summary>
        /// 是否有其他动作,0否,其它动作编号
        /// </summary>
        public string Isact
        {
            get { return _Isact; }
            set { _Isact = value; }
        }

        private string _Isid;
        /// <summary>
        /// 其它动作id
        /// </summary>
        public string Isid
        {
            get { return _Isid; }
            set { _Isid = value; }
        }
        #endregion

        /// <summary>
        /// monitor数据元
        /// </summary>
        public MonitorVO() { }

        /// <summary>
        /// monitor数据元
        /// </summary>
        /// <param name="monitor_id">数据主键</param>
        public MonitorVO(string monitor_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("monitor").Where("monitor_id", monitor_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Monitor_id = dataReader["monitor_id"].ToString();
                this.Position_id = dataReader["position_id"].ToString();
                this.Addtime = Convert.ToInt32(dataReader["addtime"]);
                this.Uid = dataReader["uid"].ToString();
                this.Ip = dataReader["ip"].ToString();
                this.Islogin = dataReader["islogin"].ToString();
                this.Isbuy = dataReader["isbuy"].ToString();
                this.Isact = dataReader["isact"].ToString();
                this.Isid = dataReader["isid"].ToString();
            }
        }

        /// <summary>
        /// monitor
        /// </summary>
        /// <param name="monitor_id">Primary Key</param>
        public MonitorVO(DataRow dataReader)
        {
            this.Monitor_id = dataReader["monitor_id"] as string;
            this.Position_id = dataReader["position_id"] as string;
            this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            this.Uid = dataReader["uid"] as string;
            this.Ip = dataReader["ip"] as string;
            this.Islogin = dataReader["islogin"] as string;
            this.Isbuy = dataReader["isbuy"] as string;
            this.Isact = dataReader["isact"] as string;
            this.Isid = dataReader["isid"] as string;
        }
        /// <summary>
        /// monitor插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("monitor").
                Value("monitor_id", data["monitor_id"], DataType.Char, 24).
                Value("position_id", data["position_id"], DataType.Char, 24).
                Value("addtime", data["addtime"], DataType.Int).
                Value("uid", data["uid"], DataType.Char, 24).
                Value("ip", data["ip"], DataType.Varchar, 30).
                Value("islogin", data["islogin"], DataType.Char, 24).
                Value("isbuy", data["isbuy"], DataType.Char, 24).
                Value("isact", data["isact"], DataType.Char, 24).
                Value("isid", data["isid"], DataType.Char, 24).
                Excute();
        }

        /// <summary>
        /// monitor更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("monitor").
                Set("position_id", data["position_id"], DataType.Char, 24).
                Set("addtime", data["addtime"], DataType.Int).
                Set("uid", data["uid"], DataType.Char, 24).
                Set("ip", data["ip"], DataType.Varchar, 30).
                Set("islogin", data["islogin"], DataType.Char, 24).
                Set("isbuy", data["isbuy"], DataType.Char, 24).
                Set("isact", data["isact"], DataType.Char, 24).
                Set("isid", data["isid"], DataType.Char, 24).
                Where("monitor_id", data["monitor_id"], DataType.Char, 24).
                Excute();
        }
    }
}