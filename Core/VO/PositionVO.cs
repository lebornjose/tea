﻿/*
 * Created:  by JabinfoCoder
 * Contact:  http://www.jabinfo.com
 * Author :  Sinbo
 * Date   :  2010/9/29 20:21:17
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// position数据元
    /// </summary>
    [Serializable]
    public class PositionVO
    {
        #region	属性


        private string _Position_id;
        /// <summary>
        /// 编号
        /// </summary>
        public string Position_id
        {
            get { return _Position_id; }
            set { _Position_id = value; }
        }

        private string _Title;
        /// <summary>
        /// 名称
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Content;
        /// <summary>
        /// 内容
        /// </summary>
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private string _Media_id;
        /// <summary>
        /// 媒体编号，0表示站内
        /// </summary>
        public string Media_id
        {
            get { return _Media_id; }
            set { _Media_id = value; }
        }

        private string _Advertise_id;
        /// <summary>
        /// 当前广告
        /// </summary>
        public string Advertise_id
        {
            get { return _Advertise_id; }
            set { _Advertise_id = value; }
        }

        private int _Code_id;
        /// <summary>
        /// 广告位编号,int自动增长型
        /// </summary>
        public int Code_id
        {
            get { return _Code_id; }
            set { _Code_id = value; }
        }

        private string _Describe;
        /// <summary>
        /// 广告描述
        /// </summary>
        public string Describe
        {
            get { return _Describe; }
            set { _Describe = value; }
        }

        private int _Width;
        /// <summary>
        /// 宽
        /// </summary>
        public int Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        private int _Height;
        /// <summary>
        /// 高
        /// </summary>
        public int Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        private string _Target;
        /// <summary>
        /// 目标地址
        /// </summary>
        public string Target
        {
            get { return _Target; }
            set { _Target = value; }
        }

        private string _Summary;
        /// <summary>
        /// 内容摘要
        /// </summary>
        public string Summary
        {
            get { return _Summary; }
            set { _Summary = value; }
        }
        #endregion

        /// <summary>
        /// position数据元
        /// </summary>
        public PositionVO() { }

        /// <summary>
        /// position数据元
        /// </summary>
        /// <param name="position_id">数据主键</param>
        public PositionVO(string position_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("position").Where("position_id", position_id, DataType.Char).Row();
            if (dataReader != null)
            {
                this.Position_id = dataReader["position_id"].ToString();
                this.Title = dataReader["title"].ToString();
                this.Content = dataReader["content"].ToString();
                this.Media_id = dataReader["media_id"].ToString();
                this.Advertise_id = dataReader["advertise_id"].ToString();
                this.Code_id = Convert.ToInt32(dataReader["code_id"]);
                this.Describe = dataReader["describe"].ToString();
                this.Width = Convert.ToInt32(dataReader["width"]);
                this.Height = Convert.ToInt32(dataReader["height"]);
                this.Target = dataReader["target"].ToString();
                this.Summary = dataReader["summary"].ToString();
            }
        }
        /// <summary>
        /// position
        /// </summary>
        /// <param name="position_id">Primary Key</param>
        public PositionVO(DataRow dataReader)
        {
            this.Position_id = dataReader["position_id"] as string;
            this.Title = dataReader["title"] as string;
            this.Content = dataReader["content"] as string;
            this.Media_id = dataReader["media_id"] as string;
            this.Advertise_id = dataReader["advertise_id"] as string;
            this.Code_id = Convert.ToInt32(dataReader["code_id"]);
            this.Describe = dataReader["describe"] as string;
            this.Width = Convert.ToInt32(dataReader["width"]);
            this.Height = Convert.ToInt32(dataReader["height"]);
            this.Target = dataReader["target"] as string;
        }
        /// <summary>
        /// position插入数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("position").
                Value("position_id", data["position_id"], DataType.Char, 24).
                Value("title", data["title"], DataType.Varchar, 200).
                Value("content", data["content"], DataType.Varchar, 1000).
                Value("media_id", data["media_id"], DataType.Char, 24).
                Value("advertise_id", data["advertise_id"], DataType.Char, 24).
                Value("code_id", data["code_id"], DataType.Int).
                Value("describe", data["describe"], DataType.Varchar, 100).
                Value("width", data["width"], DataType.Int).
                Value("height", data["height"], DataType.Int).
                Value("summary", data["summary"], DataType.Varchar, 500).
                Value("target", data["target"], DataType.Varchar, 150).
                Excute();
        }

        /// <summary>
        /// position更新数据
        /// </summary>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("position").
                Set("title", data["title"], DataType.Varchar, 200).
                Set("content", data["content"], DataType.Varchar, 1000).
                Set("media_id", data["media_id"], DataType.Char, 24).
                Set("advertise_id", data["advertise_id"], DataType.Char, 24).
                Set("code_id", data["code_id"], DataType.Int).
                Set("describe", data["describe"], DataType.Varchar, 100).
                Set("width", data["width"], DataType.Int).
                Set("height", data["height"], DataType.Int).
                Set("summary", data["summary"], DataType.Varchar, 500).
                Set("target", data["target"], DataType.Varchar, 150).
                Where("position_id", data["position_id"], DataType.Char, 24).
                Excute();
        }
    }
}