﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2012/4/16 11:49:40
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// tagsVO
    ///	</summary>
    [Serializable]
    public class TagsVO
    {
        #region	Property

        private String  _Tags_id;
        /// <summary>
        /// 标签编号
        ///</summary>
        public String  Tags_id
        {
            get{ return _Tags_id; }
            set{ _Tags_id = value; }
        }

        private String  _Title;
        /// <summary>
        /// 名称
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }

        private String  _Style;
        /// <summary>
        /// 标签式样
        ///</summary>
        public String  Style
        {
            get{ return _Style; }
            set{ _Style = value; }
        }

        private Int32  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }

        private String  _Category_id;
        /// <summary>
        /// 分类标签,0表示全局,3表示品牌，tag_id等于品牌编号
        ///</summary>
        public String  Category_id
        {
            get{ return _Category_id; }
            set{ _Category_id = value; }
        }

        private String  _Extend;
        /// <summary>
        /// 扩展字段，自定义模板名
        ///</summary>
        public String  Extend
        {
            get{ return _Extend; }
            set{ _Extend = value; }
        }

        private Int32  _Pagesize;
        /// <summary>
        /// 分页大小
        ///</summary>
        public Int32  Pagesize
        {
            get{ return _Pagesize; }
            set{ _Pagesize = value; }
        }

        private String  _Keyword;
        /// <summary>
        /// 关键字
        ///</summary>
        public String  Keyword
        {
            get{ return _Keyword; }
            set{ _Keyword = value; }
        }

        private String  _Prices;
        /// <summary>
        /// 价格区间
        ///</summary>
        public String  Prices
        {
            get{ return _Prices; }
            set{ _Prices = value; }
        }

        private String  _Parent_id;
        /// <summary>
        /// 父级标签编号
        ///</summary>
        public String  Parent_id
        {
            get{ return _Parent_id; }
            set{ _Parent_id = value; }
        }

        #endregion

		#region Constructor
        /// <summary>
        /// tags
        /// </summary>
        public TagsVO(){}
	
        /// <summary>
        /// tags
        /// </summary>
        /// <param name="tags_id">Primary Key</param>
        public TagsVO(string tags_id)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("tags").Where("tags_id",tags_id,DataType.Char, 24).Row();
            if(dataReader != null)
            {
				this.Tags_id = dataReader["tags_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Style = dataReader["style"] as string;
				this.Index = Convert.ToInt32(dataReader["index"]);
				this.Category_id = dataReader["category_id"] as string;
				this.Extend = dataReader["extend"] as string;
				this.Pagesize = Convert.ToInt32(dataReader["pagesize"]);
				this.Keyword = dataReader["keyword"] as string;
				this.Prices = dataReader["prices"] as string;
				this.Parent_id = dataReader["parent_id"] as string;
            }
        }

		/// <summary>
        /// tags
        /// </summary>
        /// <param name="tags_id">Primary Key</param>
        public TagsVO(DataRow dataReader)
        {
			this.Tags_id = dataReader["tags_id"] as string;
			this.Title = dataReader["title"] as string;
			this.Style = dataReader["style"] as string;
			this.Index = Convert.ToInt32(dataReader["index"]);
			this.Category_id = dataReader["category_id"] as string;
			this.Extend = dataReader["extend"] as string;
			this.Pagesize = Convert.ToInt32(dataReader["pagesize"]);
			this.Keyword = dataReader["keyword"] as string;
			this.Prices = dataReader["prices"] as string;
			this.Parent_id = dataReader["parent_id"] as string;
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// tagsInsert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Insert("tags").
            Value("tags_id", data["tags_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 50).
            Value("style", data["style"],DataType.Varchar, 100).
            Value("index", data["index"],DataType.Int).
            Value("category_id", data["category_id"],DataType.Char, 24).
            Value("extend", data["extend"],DataType.Varchar, 200).
            Value("pagesize", data["pagesize"],DataType.Int).
            Value("keyword", data["keyword"],DataType.Varchar, 1000).
            Value("prices", data["prices"],DataType.Varchar, 500).
            Value("parent_id", data["parent_id"],DataType.Char, 24).
            Excute();
        }

        /// <summary>
        /// tagsUpdate
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("core").Update("tags").
            Set("title", data["title"],DataType.Varchar, 50).
            Set("style", data["style"],DataType.Varchar, 100).
            Set("index", data["index"],DataType.Int).
            Set("category_id", data["category_id"],DataType.Char, 24).
            Set("extend", data["extend"],DataType.Varchar, 200).
            Set("pagesize", data["pagesize"],DataType.Int).
            Set("keyword", data["keyword"],DataType.Varchar, 1000).
            Set("prices", data["prices"],DataType.Varchar, 500).
            Set("parent_id", data["parent_id"],DataType.Char, 24).
            Where("tags_id", data["tags_id"],DataType.Char, 24).
            Excute();
        }
		#endregion
    }
}