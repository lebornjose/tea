﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  12-2-28 上02时35分41秒
 */
using System;
using System.Data;

namespace Jabinfo.Core.VO
{
    /// <summary>
    /// usersVO
    ///	</summary>
    [Serializable]
    public class UsersVO:Sinbo.R
    {
        #region 属性
        public int Addtime { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// users
        /// </summary>
        public UsersVO(){}
        /// <summary>
        /// users
        /// </summary>
        /// <param name="uid">Primary Key</param>
        public UsersVO(string uid)
        {
            DataRow dataReader = JabinfoSQL.Instance("core").Select("*").From("users").Where("uid",uid,DataType.Char, 24).Row();
            if(dataReader != null)
            {
				this.Uid = dataReader["uid"] as string;
				this.Username = dataReader["username"] as string;
				this.Email = dataReader["email"] as string;
				this.Logins = Convert.ToInt32(dataReader["logins"]);
				this.Nick = dataReader["nick"] as string;
				this.Rate = Convert.ToInt32(dataReader["rate"]);
				this.Lasttime = Convert.ToInt32(dataReader["lasttime"]);
                this.Honour = Convert.ToInt32(dataReader["honour"]);
				this.Mobile = dataReader["mobile"] as string;
				this.Status = dataReader["status"] as string;
				this.Avatar = dataReader["avatar"] as string;
                this.Addtime = Convert.ToInt32(dataReader["addtime"]);
            }
        }

		/// <summary>
        /// users
        /// </summary>
        /// <param name="uid">Primary Key</param>
        public UsersVO(DataRow dataReader)
        {
			this.Uid = dataReader["uid"] as string;
			this.Username = dataReader["username"] as string;
			this.Email = dataReader["email"] as string;
			this.Logins = Convert.ToInt32(dataReader["logins"]);
			this.Nick = dataReader["nick"] as string;
			this.Rate = Convert.ToInt32(dataReader["rate"]);
			this.Lasttime = Convert.ToInt32(dataReader["lasttime"]);
			this.Honour = Convert.ToInt32(dataReader["honour"]);
			this.Mobile = dataReader["mobile"] as string;
			this.Avatar = dataReader["avatar"] as string;
			this.Status = dataReader["status"] as string;
        }
		#endregion
    }
}