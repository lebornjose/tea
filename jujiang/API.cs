﻿using System;
using System.Data;
using Jabinfo.Core.Model;
using Jabinfo.Core.VO;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;


namespace Jabinfo
{
	public class API
	{
		private static API _Instance;
		public static API Instance { get { if (_Instance == null) { _Instance = new API(); } return _Instance; } }
		private API() {}

		/// <summary>
		/// 获取banner图
		/// </summary>
		public AnimationVO [] banner()
		{
			AnimationModel animatiobModel = new AnimationModel ();
			AnimationVO [] animation=animatiobModel.Select(string.Empty,"`index` desc");
			return animation;
		}

		/// <summary>
		/// 获取教师
		/// </summary>
		/// <param name="length">Length.</param>
		public TeacherVO[] teacher(int length)
		{
			TeacherModel teacherModel = new TeacherModel ();
			TeacherVO [] teacher=teacherModel.Select(0,length,string.Empty,"`date` desc ");
			return teacher;
		}

		/// <summary>
		/// 获取学术研究
		/// </summary>
		/// <param name="length">Length.</param>
		public ResearchVO[] research(int length)
		{
			ResearchModel researchModel = new ResearchModel ();
			ResearchVO[] rea = researchModel.Select (0, length, string.Empty, "`date` desc ");
			return rea;
		}

		/// <summary>
		/// 获取学员风采
		/// </summary>
		/// <param name="length">Length.</param>
		public DemeanorVO[] demeanor(int length)
		{
			DemeanorModel demeanorModel = new DemeanorModel ();
			DemeanorVO[] deme = demeanorModel.Select (0, length, string.Empty, "`demeanor_id` desc ");
			return deme;
		}

		/// <summary>
		/// 获取
		/// </summary>
		/// <param name="msg_id">Message identifier.</param>
		public Basic_msgVO basic(string msg_id)
		{
			Basic_msgVO msgVO = new Basic_msgVO (msg_id);
			return msgVO;
		}

		/// <summary>
		/// 获取友情链接
		/// </summary>
		public LinksVO [] links(int length)
		{
			LinksModel linksModel = new LinksModel ();
			LinksVO[] link = linksModel.Select (0, length, string.Empty, "`index` desc ");
			return link;
		}
	}
}

