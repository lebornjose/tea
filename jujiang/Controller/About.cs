﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class About : Sinbo.Controller
    {
		#region	Constructor
        public About()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["aboutList"] = aboutModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = aboutModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                aboutModel.Insert(this.Post);
				this.Jump("jujiang/about/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string about_id)
        {
            if (this.IsPost)
            {
                aboutModel.Update(this.Post);
				this.Jump("jujiang/about/edit/"+this.Post["about_id"],"编辑成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["about"] = new AboutVO(about_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string about_id)
        {
            aboutModel.Delete(about_id);
        }
		#endregion

		#region	_model
        private AboutModel _model = null;
        private AboutModel aboutModel
        {
            get
            {
                if (_model == null)
                    _model = new AboutModel();
                return _model;
            }
        }
		#endregion
    }
}