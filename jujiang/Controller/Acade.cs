﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Acade : Sinbo.Controller
    {
		#region	Constructor
        public Acade()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["acadeList"] = acadeModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = acadeModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                acadeModel.Insert(this.Post);
				Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string acade_id)
        {
            if (this.IsPost)
            {
                acadeModel.Update(this.Post);
                Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["acade"] = new AcadeVO(acade_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string acade_id)
        {
            acadeModel.Delete(acade_id);
        }
		#endregion

		#region	_model
        private AcadeModel _model = null;
        private AcadeModel acadeModel
        {
            get
            {
                if (_model == null)
                    _model = new AcadeModel();
                return _model;
            }
        }
		#endregion
    }
}