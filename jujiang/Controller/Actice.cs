﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Actice : Sinbo.Controller
    {
		#region	Constructor
        public Actice()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["acticeList"] = acticeModel.Select(index, 30, string.Empty, "`date` desc");
			view.Variable ["status"] = Jabinfo.Help.Config.Get ("jujiang.status");
			view.Functions.Add ("reging", this.reging);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = acticeModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		public object reging(object [] args)
		{
			string actice_id = Convert.ToString (args [0]);
			RegModel regModel = new RegModel ();
			int count = regModel.Count (string.Format ("actice_id ='{0}'", actice_id));
			return count;
		}

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["active_id"] = Jabinfo.Help.Basic.JabId;
				this.Post ["date"] = Jabinfo.Help.Date.Now.ToString ();
				this.Post["starttime"] = Jabinfo.Help.Date.StringToDate(this.Post["starttime"]).ToString();
				this.Post["status"]="1";
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["active_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["active_id"], key), this.Post["active_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
				string content = Jabinfo.Help.Formate.HtmlClear(this.Post["content"]);
				if (content.Length < 300)
					this.Post["summary"] = content;
				else
					this.Post["summary"] = content.Substring(0, 300);
                acticeModel.Insert(this.Post);
				this.Jump("jujiang/actice/home","添加成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string active_id)
        {
            if (this.IsPost)
            {
				this.Post ["starttime"] = Jabinfo.Help.Date.StringToDate (this.Post ["starttime"]).ToString ();
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["active_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["active_id"], key), this.Post["active_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
				string content = Jabinfo.Help.Formate.HtmlClear(this.Post["content"]);
				if (content.Length < 200)
					this.Post["summary"] = content;
				else
					this.Post["summary"] = content.Substring(0, 200);
                acticeModel.Update(this.Post);
				this.Jump(string.Format("jujiang/actice/edit/{0}",this.Post["active_id"]),"编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["actice"] = new ActiceVO(active_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string active_id)
        {
            acticeModel.Delete(active_id);
        }
		#endregion

		/// <summary>
		/// 活动结束
		/// </summary>
		public void over(string actice_id)
		{
			JabinfoKeyValue data = new JabinfoKeyValue ();
			data ["active_id"] = actice_id;
			data["status"]="2";
			acticeModel.Update (data);
			Output ("#","活动结束成功");
		}
		#region	_model
        private ActiceModel _model = null;
        private ActiceModel acticeModel
        {
            get
            {
                if (_model == null)
                    _model = new ActiceModel();
                return _model;
            }
        }
		#endregion
    }
}