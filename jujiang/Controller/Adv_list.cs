﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 18:49:22
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Adv_list : Sinbo.Controller
    {
		#region	Constructor
        public Adv_list()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string advertise_id,string start)
        {
            int index=0;
			string where = string.Format ("advertise_id ='{0}'", advertise_id);
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["adv_listList"] = adv_listModel.Select(index, 30, where, "`date` desc");
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
			view.Variable["total"] = adv_listModel.Count(where);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                adv_listModel.Insert(this.Post);
				Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string list_id)
        {
            if (this.IsPost)
            {
                adv_listModel.Update(this.Post);
                Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["adv_list"] = new Adv_listVO(list_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string list_id)
        {
            adv_listModel.Delete(list_id);
        }
		#endregion

		#region	_model
        private Adv_listModel _model = null;
        private Adv_listModel adv_listModel
        {
            get
            {
                if (_model == null)
                    _model = new Adv_listModel();
                return _model;
            }
        }
		#endregion
    }
}