﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 18:49:17
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Advertise : Sinbo.Controller
    {
		#region	Constructor
        public Advertise()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["advertiseList"] = advertiseModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable ["status"] = Jabinfo.Help.Config.Get ("jujiang.status");
			view.Functions.Add ("haspeople", this.haspeople);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = advertiseModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		public object haspeople(object [] args)
		{
			Adv_listModel adv_listModel = new Adv_listModel ();
			string advertise_id = Convert.ToString (args [0]);
			string where = string.Format ("advertise_id ='{0}'", advertise_id);
			return adv_listModel.Count (where);
		}

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post ["date"] = Jabinfo.Help.Date.Now.ToString ();
                advertiseModel.Insert(this.Post);
				this.Jump("jujiang/advertise/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		/// <summary>
		/// 招聘状态结束
		/// </summary>
		public void over(string advertise_id)
		{
			JabinfoKeyValue data = new JabinfoKeyValue ();
			data ["advertise_id"] = advertise_id;
			data["status"]="2";
			advertiseModel.Update (data);
			Output("#","招聘结束成功");
		}

		#region	edit
        public void edit(string advertice_id)
        {
            if (this.IsPost)
            {
                advertiseModel.Update(this.Post);
				this.Jump("jujiang/advertise/edit/"+this.Post["advertisae_id"],"编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["advertise"] = new AdvertiseVO(advertice_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string advertice_id)
        {
            advertiseModel.Delete(advertice_id);
        }
		#endregion

		#region	_model
        private AdvertiseModel _model = null;
        private AdvertiseModel advertiseModel
        {
            get
            {
                if (_model == null)
                    _model = new AdvertiseModel();
                return _model;
            }
        }
		#endregion
    }
}