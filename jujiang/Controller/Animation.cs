﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Animation : Sinbo.Controller
    {
		#region	Constructor
        public Animation()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["animationList"] = animationModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = animationModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["animation_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["animation_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["animation_id"], key), this.Post["animation_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                animationModel.Insert(this.Post);
				this.Jump("jujiang/animation/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string animation_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["animation_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["animation_id"], key), this.Post["animation_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                animationModel.Update(this.Post);
				this.Jump("jujiang/animation/edit/"+this.Post["animation_id"],"编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["animation"] = new AnimationVO(animation_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string animation_id)
        {
            animationModel.Delete(animation_id);
        }
		#endregion

		#region	_model
        private AnimationModel _model = null;
        private AnimationModel animationModel
        {
            get
            {
                if (_model == null)
                    _model = new AnimationModel();
                return _model;
            }
        }
		#endregion
    }
}