﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 9:00:22
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Basic_msg : Sinbo.Controller
    {
		#region	Constructor
        public Basic_msg()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["basic_msgList"] = basic_msgModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = basic_msgModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                basic_msgModel.Insert(this.Post);
				Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit()
        {
            if (this.IsPost)
            {
                basic_msgModel.Update(this.Post);
				this.Jump("jujiang/basic_msg/edit","编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["basic_msg"] = new Basic_msgVO("1231322234");
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string basic_id)
        {
            basic_msgModel.Delete(basic_id);
        }
		#endregion

		#region	_model
        private Basic_msgModel _model = null;
        private Basic_msgModel basic_msgModel
        {
            get
            {
                if (_model == null)
                    _model = new Basic_msgModel();
                return _model;
            }
        }
		#endregion
    }
}