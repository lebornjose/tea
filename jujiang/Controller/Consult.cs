﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Consult : Sinbo.Controller
    {
		#region	Constructor
        public Consult()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["consultList"] = consultModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = consultModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                consultModel.Insert(this.Post);
				Output("#","添加成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string consult_id)
        {
            if (this.IsPost)
            {
                consultModel.Update(this.Post);
                Output("#","编辑成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["consult"] = new ConsultVO(consult_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string consult_id)
        {
            consultModel.Delete(consult_id);
        }
		#endregion

		#region	_model
        private ConsultModel _model = null;
        private ConsultModel consultModel
        {
            get
            {
                if (_model == null)
                    _model = new ConsultModel();
                return _model;
            }
        }
		#endregion
    }
}