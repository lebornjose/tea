﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Culum : Sinbo.Controller
    {
		#region	Constructor
        public Culum()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
			string where = string.Empty;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
			if (this.IsPost) {
				string type = this.Post ["type"];
				if(!string.IsNullOrEmpty(type))
			       where = string.Format ("type='{0}'", type);
			}
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["culumList"] = culumModel.Select(index, 30, where, "`index` desc");
			view.Variable ["cource"] = Jabinfo.Help.Config.Get ("jujiang.cource");
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = culumModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                culumModel.Insert(this.Post);
				Output("#","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable ["cource"] = Jabinfo.Help.Config.Get ("jujiang.cource");
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string Column_id)
        {
            if (this.IsPost)
            {
                culumModel.Update(this.Post);
                Output("#","编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["culum"] = new CulumVO(Column_id);
			view.Variable ["cource"] = Jabinfo.Help.Config.Get ("jujiang.cource");
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string Column_id)
        {
            culumModel.Delete(Column_id);
        }
		#endregion

		#region	_model
        private CulumModel _model = null;
        private CulumModel culumModel
        {
            get
            {
                if (_model == null)
                    _model = new CulumModel();
                return _model;
            }
        }
		#endregion
    }
}