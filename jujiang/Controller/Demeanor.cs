﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Demeanor : Sinbo.Controller
    {
		#region	Constructor
        public Demeanor()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
			string where = string.Empty;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
			if (this.IsPost) {
				string name = this.Post ["name"];
				if (!string.IsNullOrEmpty (name))
					where = string.Format ("name like '%{0}%'", name);
			}
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["demeanorList"] = demeanorModel.Select(index, 10, where, "'demeanor_id' desc");
			view.Variable["index"] = index;
            view.Variable["size"] = 10;
			view.Variable["total"] = demeanorModel.Count(where);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["demeanor_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["demeanor_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["demeanor_id"], key), this.Post["demeanor_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                demeanorModel.Insert(this.Post);
				this.Jump("jujiang/demeanor/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string demeanor_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["demeanor_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["demeanor_id"], key), this.Post["demeanor_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                demeanorModel.Update(this.Post);
				this.Jump("jujiang/demeanor/edit/"+this.Post["demeanor_id"],"编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["demeanor"] = new DemeanorVO(demeanor_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string demeanor_id)
        {
            demeanorModel.Delete(demeanor_id);
        }
		#endregion

		#region	_model
        private DemeanorModel _model = null;
        private DemeanorModel demeanorModel
        {
            get
            {
                if (_model == null)
                    _model = new DemeanorModel();
                return _model;
            }
        }
		#endregion
    }
}