﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Director : Sinbo.Controller
    {
		#region	Constructor
        public Director()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["directorList"] = directorModel.Select(index, 30, string.Empty,"`index` desc");
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = directorModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				//记录图片
				this.Post["director_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["director_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["director_id"], key), this.Post["director_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                directorModel.Insert(this.Post);
				this.Jump("jujiang/director/home", "添加成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string director_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["director_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["director_id"], key), this.Post["director_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
					this.Post["attach"] = "1";//有预览图
				}
                directorModel.Update(this.Post);
				this.Jump("jujiang/director/edit/"+this.Post["director_id"],"编辑成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["director"] = new DirectorVO(director_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string director_id)
        {
            directorModel.Delete(director_id);
        }
		#endregion

		#region	_model
        private DirectorModel _model = null;
        private DirectorModel directorModel
        {
            get
            {
                if (_model == null)
                    _model = new DirectorModel();
                return _model;
            }
        }
		#endregion
    }
}