﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 8:50:30
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Dist : Sinbo.Controller
    {
		#region	Constructor
        public Dist()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["distList"] = distModel.Select(index, 5, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = distModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["dist_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["dist_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["dist_id"], key), this.Post["dist_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                distModel.Insert(this.Post);
				this.Jump("jujiang/dist/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string dist_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["dist_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["dist_id"], key), this.Post["dist_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                distModel.Update(this.Post);
				this.Jump("jujiang/dist/home","编辑成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["dist"] = new DistVO(dist_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string dist_id)
        {
            distModel.Delete(dist_id);
        }
		#endregion

		#region	_model
        private DistModel _model = null;
        private DistModel distModel
        {
            get
            {
                if (_model == null)
                    _model = new DistModel();
                return _model;
            }
        }
		#endregion
    }
}