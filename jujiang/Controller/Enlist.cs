﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/8/29 17:05:28
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Enlist : Sinbo.Controller
    {
		#region	Constructor
        public Enlist()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["enlistList"] = enlistModel.Select(index, 30, string.Empty, string.Empty);
			view.Functions.Add ("other", this.other);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = enlistModel.Count(string.Empty);
            view.Render();
        }

		public JabinfoKeyValue other(object [] args)
		{
			string list_id = Convert.ToString (args [0]);
			EnlistVO enlistVO = new EnlistVO (list_id);
			return enlistVO.OtherKV;
		}
		#endregion

		public void detail(string list_id)
		{
			EnlistVO enlistVO = new EnlistVO (list_id);
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Functions.Add ("other", this.other);
			view.Variable ["list"] = new EnlistVO (list_id);
			view.Variable ["other"] = enlistVO.OtherKV;
			view.Render ();
		}

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                enlistModel.Insert(this.Post);
				Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string list_id)
        {
            if (this.IsPost)
            {
                enlistModel.Update(this.Post);
                Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["enlist"] = new EnlistVO(list_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string list_id)
        {
            enlistModel.Delete(list_id);
        }
		#endregion

		#region	_model
        private EnlistModel _model = null;
        private EnlistModel enlistModel
        {
            get
            {
                if (_model == null)
                    _model = new EnlistModel();
                return _model;
            }
        }
		#endregion
    }
}