﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Item : Sinbo.Controller
    {
		#region	Constructor
        public Item()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["itemList"] = itemModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = itemModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                itemModel.Insert(this.Post);
				Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string item_id)
        {
            if (this.IsPost)
            {
                itemModel.Update(this.Post);
                Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["item"] = new ItemVO(item_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string item_id)
        {
            itemModel.Delete(item_id);
        }
		#endregion

		#region	_model
        private ItemModel _model = null;
        private ItemModel itemModel
        {
            get
            {
                if (_model == null)
                    _model = new ItemModel();
                return _model;
            }
        }
		#endregion
    }
}