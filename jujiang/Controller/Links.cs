﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Links : Sinbo.Controller
    {
		#region	Constructor
        public Links()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["linksList"] = linksModel.Select(index, 30, string.Empty,"`index` desc");
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = linksModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                linksModel.Insert(this.Post);
				Output("#","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string links_id)
        {
            if (this.IsPost)
            {
                linksModel.Update(this.Post);
                Output("#","编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["links"] = new LinksVO(links_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string links_id)
        {
            linksModel.Delete(links_id);
        }
		#endregion

		#region	_model
        private LinksModel _model = null;
        private LinksModel linksModel
        {
            get
            {
                if (_model == null)
                    _model = new LinksModel();
                return _model;
            }
        }
		#endregion
    }
}