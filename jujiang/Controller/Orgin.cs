﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 8:54:12
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Orgin : Sinbo.Controller
    {
		#region	Constructor
        public Orgin()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["orginList"] = orginModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = orginModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["orgin_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["orgin_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["orgin_id"], key), this.Post["orgin_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                orginModel.Insert(this.Post);
				this.Jump("jujiang/orgin/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string orgin_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["orgin_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["orgin_id"], key), this.Post["orgin_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                orginModel.Update(this.Post);
				this.Jump("jujiang/orgin/edit/"+this.Post["orgin_id"],"编辑成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["orgin"] = new OrginVO(orgin_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string orgin_id)
        {
            orginModel.Delete(orgin_id);
        }
		#endregion

		#region	_model
        private OrginModel _model = null;
        private OrginModel orginModel
        {
            get
            {
                if (_model == null)
                    _model = new OrginModel();
                return _model;
            }
        }
		#endregion
    }
}