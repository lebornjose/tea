﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 13:12:33
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Reg : Sinbo.Controller
    {
		#region	Constructor
        public Reg()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string active_id,string start)
        {
            int index=0;
			string where = string.Format ("actice_id ='{0}'", active_id);
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["regList"] = regModel.Select(index, 30,where, "`date` asc");
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
			view.Variable["total"] = regModel.Count(where);
            view.Render();
        }
		#endregion

		#region	add
		public void add(string active_id)
        {
            if (this.IsPost)
            {
				int date= Jabinfo.Help.Date.Now;
				ActiceVO activeVO = new ActiceVO (this.Post ["actice_id"]);
				if (activeVO.Date - 1440 > date) {    //活动开始的前12小时 不允许报名
					Output ("#", "活动开始的前12小时不允许报名");
					return;
				}
				this.Post ["date"] = date.ToString ();
                regModel.Insert(this.Post);
				Output("#","报名成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable ["actice_id"] = active_id;
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string reg_id)
        {
            if (this.IsPost)
            {
                regModel.Update(this.Post);
                Output("#","Saved success.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["reg"] = new RegVO(reg_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string reg_id)
        {
            regModel.Delete(reg_id);
        }
		#endregion

		#region	_model
        private RegModel _model = null;
        private RegModel regModel
        {
            get
            {
                if (_model == null)
                    _model = new RegModel();
                return _model;
            }
        }
		#endregion
    }
}