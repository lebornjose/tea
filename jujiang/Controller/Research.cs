﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Research : Sinbo.Controller
    {
		#region	Constructor
        public Research()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["researchList"] = researchModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = researchModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["research_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["research_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["research_id"], key), this.Post["research_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
				string content = Jabinfo.Help.Formate.HtmlClear(this.Post["content"]);
				if (content.Length < 200)
					this.Post["summary"] = content;
				else
					this.Post["summary"] = content.Substring(0, 200);
				this.Post ["date"] = Jabinfo.Help.Date.Now.ToString ();
                researchModel.Insert(this.Post);
				this.Jump("jujiang/research/home","添加成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string research_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["research_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["research_id"], key), this.Post["research_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
				string content = Jabinfo.Help.Formate.HtmlClear(this.Post["content"]);
				if (content.Length < 200)
					this.Post["summary"] = content;
				else
					this.Post["summary"] = content.Substring(0, 200);
                researchModel.Update(this.Post);
				Output("jujiang/resrarch/edit/"+this.Post["research_id"],"编辑成功.");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["research"] = new ResearchVO(research_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string research_id)
        {
            researchModel.Delete(research_id);
        }
		#endregion

		#region	_model
        private ResearchModel _model = null;
        private ResearchModel researchModel
        {
            get
            {
                if (_model == null)
                    _model = new ResearchModel();
                return _model;
            }
        }
		#endregion
    }
}