﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Teacher : Sinbo.Controller
    {
		#region	Constructor
        public Teacher()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["teacherList"] = teacherModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
			view.Functions.Add ("team", this.team);
			view.Variable ["type"] = Jabinfo.Help.Config.Get ("jujiang.type");
            view.Variable["total"] = teacherModel.Count(string.Empty);
            view.Render();
        }

		public string team(object [] args)
		{
			string team_id = Convert.ToString (args [0]);
			TeamVO teamVO = new TeamVO (team_id);
			return teamVO.Title;
		
		}
		#endregion

		public void search_win()
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["type"] = Jabinfo.Help.Config.Get("jujiang.type");
			view.Render ();
		}

		public void search(string start)
		{
			int index=0;
			string where = string.Empty;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			if (this.IsPost) {
				string name = this.Post ["name"];
				string type = this.Post ["type"];
				if (!string.IsNullOrEmpty (name))
					where = string.Format ("and name like '%{0}%'", name);
				if (!string.IsNullOrEmpty (type))
					where = string.Format ("{0} and type='{1}'", where, type);
				where = where.Substring (4);
				this.JabinfoContext.Session.Add ("teacher_search", where);
			} else {
				where = this.JabinfoContext.Session.Get ("teacher_search").ToString ();
			}
			JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["teacherList"] = teacherModel.Select(index, 30, where, "`date` desc");
			view.Functions.Add ("team", this.team);
			view.Variable ["type"] = Jabinfo.Help.Config.Get ("jujiang.type");
			view.Variable["index"] = index;
			view.Variable["size"] = 30;
			view.Variable["total"] = teacherModel.Count(where);
			view.Render();
		}

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
				this.Post["teacher_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["teacher_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["teacher_id"], key), this.Post["teacher_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
				this.Post ["date"] = Jabinfo.Help.Date.Now.ToString();
                teacherModel.Insert(this.Post);
				this.Jump("jujiang/teacher/home","添加成功.");
                return;
            }
			TeamModel teamModel = new TeamModel ();
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable ["type"] = Jabinfo.Help.Config.Get("jujiang.type");
			view.Variable ["teamList"] = teamModel.Select (string.Empty, string.Empty);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string teacher_id)
        {
            if (this.IsPost)
            {
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["teacher_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					foreach (string key in sizes.Keys)
					{
						string[] size = sizes[key].Split('x');
						Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["teacher_id"], key), this.Post["teacher_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
					}
				}
                teacherModel.Update(this.Post);
				this.Jump("jujiang/teacher/edit/"+this.Post["teacher_id"],"编辑成功.");
                return;
            }
			TeamModel teamModel = new TeamModel ();
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable ["type"] = Jabinfo.Help.Config.Get("jujiang.type");
			view.Variable ["teamList"] = teamModel.Select (string.Empty, string.Empty);
			view.Variable["teacher"] = new TeacherVO(teacher_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string teacher_id)
        {
            teacherModel.Delete(teacher_id);
        }
		#endregion

		#region	_model
        private TeacherModel _model = null;
        private TeacherModel teacherModel
        {
            get
            {
                if (_model == null)
                    _model = new TeacherModel();
                return _model;
            }
        }
		#endregion
    }
}