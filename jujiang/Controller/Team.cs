﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using Jabinfo.Jujiang.Model;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
    class Team : Sinbo.Controller
    {
		#region	Constructor
        public Team()
        {
            this.Access = Right.Administrator;
        }
		#endregion

		#region	home
        public void home(string start)
        {
            int index=0;
            if (!string.IsNullOrEmpty(start))
                index = Convert.ToInt32(start);
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Variable["teamList"] = teamModel.Select(index, 30, string.Empty, string.Empty);
			view.Variable["index"] = index;
            view.Variable["size"] = 30;
            view.Variable["total"] = teamModel.Count(string.Empty);
            view.Render();
        }
		#endregion

		#region	add
        public void add()
        {
            if (this.IsPost)
            {
                teamModel.Insert(this.Post);
				Output("#","添加成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
            view.Render();
        }
		#endregion

		#region	edit
        public void edit(string team_id)
        {
            if (this.IsPost)
            {
                teamModel.Update(this.Post);
                Output("#","编辑成功");
                return;
            }
            JabinfoView view = new JabinfoView(this.JabinfoContext);
			view.Variable["team"] = new TeamVO(team_id);
            view.Render();
        }
		#endregion

		#region	remove
        public void remove(string team_id)
        {
            teamModel.Delete(team_id);
        }
		#endregion

		#region	_model
        private TeamModel _model = null;
        private TeamModel teamModel
        {
            get
            {
                if (_model == null)
                    _model = new TeamModel();
                return _model;
            }
        }
		#endregion
    }
}