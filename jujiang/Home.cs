﻿using System;
using System.Data;
using Jabinfo.Jujiang.Model;
using Jabinfo.Article.Model;
using Jabinfo.Article.VO;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang
{
	public class Home:Sinbo.Controller
	{
		public Home ()
		{
			this.Skin = true;
		}

		/// <summary>
		/// 首页
		/// </summary>
		public void index()
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Functions.Add ("time", this.time);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		public string[] time(object [] args)
		{
			int date = Convert.ToInt32 (args [0]);
			string time = Jabinfo.Help.Date.Formate (date);
			string[] t = time.Split ('-');
			return t;
		}

		/// <summary>
		/// 师资团队
		/// </summary>
		public void team(string start){
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			TeamModel teamModel = new TeamModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["teamList"] = teamModel.Select (index, 9, string.Empty, "`team_id` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 9;
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 教师目录
		/// </summary>
		/// <param name="start">Start.</param>
		public void teacher(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			TeacherModel teacherModel = new TeacherModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["teacherList"] = teacherModel.Select (index, 8, string.Format("`type`='2'"), "`date` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 8;
			view.Variable ["total"] = teacherModel.Count (string.Format("`type`='2'"));
			if (view.Cache ())
				return;
			view.Serialize ();
		}
			
		public void teacher_search(string start)
		{
			int index = 0;
			string where = string.Empty;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			if (this.IsPost) {
				string name = this.Post ["title"];
				if (!string.IsNullOrEmpty (name))
					where = string.Format ("`name` like '%{0}%'", name);
				this.JabinfoContext.Session.Add ("teacher_search", where);
			} else {
					where = this.JabinfoContext.Session.Get("teacher_search").ToString();
			}
			TeacherModel teacherModel = new TeacherModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["teacherList"] = teacherModel.Select (index, 8, where, "`date` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 8;
			view.Variable ["total"] = teacherModel.Count (where);
			if (view.Cache ())
				return;
			view.Serialize ();
		}
			
		public void reg_add(string active_id)
		{           
			RegModel regModel = new RegModel ();
			if (this.IsPost)
			{
				int date= Jabinfo.Help.Date.Now;
				ActiceVO activeVO = new ActiceVO (this.Post ["actice_id"]);
				if (activeVO.Date - 1440 > date) {    //活动开始的前12小时 不允许报名
					Output ("#", "活动开始的前12小时不允许报名");
					return;
				}
				//发送系统报名邮件
				string subject= string.Format ("{0}报名通知", activeVO.Title);
				int total = regModel.Count (string.Format ("actice_id='{0}'", this.Post["actice_id"]))+1;
				string email = this.Post ["email"];
				string content=string.Format("    {0}您好,您参加的{1}的报名我们已经收到,你的报名编号为0{2}。<br/>如有其它需要可联系夏老师： 13817305392。<br/>祝:工作顺利!",this.Post["name"],activeVO.Title,total);
			    Jabinfo.Help.Mail.Send(subject,content,email);
				this.Post ["date"] = date.ToString ();
				regModel.Insert(this.Post);
				Output("#","报名成功.");
				return;
			}
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["actice_id"] = active_id;
			view.Serialize ();
		}

		public void teacher_s(string team_id,string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			TeacherModel teacherModel = new TeacherModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["teacherList"] = teacherModel.Select (index, 8, string.Format("`team_id`='{0}'",team_id), "`date` desc");
			view.Functions.Add("teams",this.teams);
			view.Variable ["index"] = index;
			view.Variable ["team_id"] = team_id;
			view.Variable ["size"] = 8;
			view.Variable ["total"] = teacherModel.Count (string.Format("`team_id`='{0}'",team_id));
			if (view.Cache ())
				return;
			view.Serialize ();
		}
		public string teams(object [] args)
		{
			string team_id = Convert.ToString (args [0]);
			TeamVO teamVO = new TeamVO (team_id);
			return teamVO.Title;
		}
		/// <summary>
		/// 兼职导师
		/// </summary>
		/// <param name="start">Start.</param>
		public void teacher_jz(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			TeacherModel teacherModel = new TeacherModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["teacherList"] = teacherModel.Select (index, 8, string.Format("`type`='0'"), "`date` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 8;
			view.Variable ["total"] = teacherModel.Count (string.Format("`type`='0'"));
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 访问学者
		/// </summary>
		/// <param name="start">Start.</param>
		public void teacher_fw(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			TeacherModel teacherModel = new TeacherModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["teacherList"] = teacherModel.Select (index, 8, string.Format("`type`='1'"), "`date` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 8;
			view.Variable ["total"] = teacherModel.Count (string.Format("`type`='1'"));
			if (view.Cache ())
				return;
			view.Serialize ();
		}
		/// <summary>
		/// 教师详情介绍
		/// </summary>
		/// <param name="teacher_id">Teacher identifier.</param>
		public void teacher_detail(string teacher_id)
		{
			TeacherVO teacherVO = new TeacherVO (teacher_id);
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["detail"] = teacherVO;
			if (view.Cache ())
				return;
			view.Serialize (teacher_id);
		}

		public void xs_list(string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			ResearchModel reseachModel = new ResearchModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["reList"] = reseachModel.Select (index ,6,string.Empty, "`date` desc");
			view.Variable["index"] = index;
			view.Variable["size"] = 6;
			view.Variable ["total"] = reseachModel.Count (string.Empty);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 学术研究详情
		/// </summary>
		/// <param name="research_id">Research identifier.</param>
		public void xs_detail(string research_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["re"] = new ResearchVO (research_id);
			view.Functions.Add ("sub", this.sub);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		public string sub(object [] args)
		{
			string str = Convert.ToString (args [0]);
			str = str.Substring (0, 50);
			return str;
		}

		/// <summary>
		/// 学生风采
		/// </summary>
		public void deme(string start)
		{
			int index=0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			DemeanorModel demenorModel = new DemeanorModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["demeList"] = demenorModel.Select (index, 10, string.Empty, "`demeanor_id` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 10;
			view.Variable ["total"] = demenorModel.Count (string.Empty);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		public void deme_search(string start)
		{
			int index=0;
			string where = string.Empty;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			if (this.IsPost) {
				string name = this.Post ["title"];
				if (!string.IsNullOrEmpty (name))
						where = string.Format ("`name` like '%{0}%'", name);
				this.JabinfoContext.Session.Add ("teacher_search", where);
			} else {
					where = this.JabinfoContext.Session.Get("teacher_search").ToString();
			}
			DemeanorModel demenorModel = new DemeanorModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["demeList"] = demenorModel.Select (index, 10,where, "`demeanor_id` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 10;
			view.Variable ["total"] = demenorModel.Count (where);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 学校概况
		/// </summary>
		public void surve()
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 基础页面
		/// </summary>
		public void about(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 理事长寄语
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void member(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		public void page(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}
		/// <summary>
		/// 课程特色
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void course(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 基础课程
		/// </summary>
		public void less_jc(string start)
		{
			int index = 0;
			string where = string.Format ("`type`='1'");
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			CulumModel culumModel = new CulumModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["lessList"] = culumModel.Select (index, 12, where, "`index` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 12;
			view.Variable ["total"] = culumModel.Count (where);
			if (view.Cache ())
				return;
			view.Serialize ();
		}
		/// <summary>
		/// 专业基础课程
		/// </summary>
		/// <param name="start">Start.</param>
		public void less_zy(string start)
		{
			int index = 0;
			string where = string.Format ("`type`='2'");
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			CulumModel culumModel = new CulumModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["lessList"] = culumModel.Select (index, 12, where, "`index` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 12;
			view.Variable ["total"] = culumModel.Count (where);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		public void less_tg(string start)
		{
			int index = 0;
			string where = string.Format ("`type`='3'");
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			CulumModel culumModel = new CulumModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["lessList"] = culumModel.Select (index, 12, where, "`index` desc");
			view.Variable ["index"] = index;
			view.Variable ["size"] = 12;
			view.Variable ["total"] = culumModel.Count (where);
			if (view.Cache ())
				return;
			view.Serialize ();
		}
		/// <summary>
		/// 学院揭牌
		/// </summary>
		public void veild()
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 学院特色
		/// </summary>
		public void feature(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty(start))
				index = Convert.ToInt32(start);
			DistModel distModel = new DistModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["distList"] = distModel.Select (index, 8, string.Empty, string.Empty);
			if(view.Cache())
				return;
			view.Serialize();
		}

		/// <summary>
		/// 顾问名单
		/// </summary>
		/// <param name="start">Start.</param>
		public void consult(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			ConsultModel consultModel = new ConsultModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["consultList"] = consultModel.Select (index, 30, string.Empty, string.Empty);
			view.Variable ["index"] = index;
			view.Variable ["size"] = 30;
			view.Variable ["total"] = consultModel.Count (string.Empty);
			if (view.Cache ())
				return;
			view.Serialize ();
		}
		/// <summary>
		/// 组织机构
		/// </summary>
		/// <param name="start">Start.</param>
		public void orgin(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			OrginModel orginModel = new OrginModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["orginList"] = orginModel.Select (index, 8, string.Empty, string.Empty);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 理事简介
		/// </summary>
		public void director(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			DirectorModel directorModel = new DirectorModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["direList"] = directorModel.Select (index, 8, string.Empty, "`index` desc");
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 招生
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void student(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 学生事务
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void trans(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 奖励政策
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void reward(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 活动中心
		/// </summary>
		/// <param name="start">Start.</param>
		public void active(string start)
		{
			int index = 0;
			if (!string.IsNullOrEmpty (start))
				index = Convert.ToInt32 (start);
			ActiceModel activeModel = new ActiceModel ();
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["activeList"] = activeModel.Select (index, 5, string.Empty, "`date` desc");
			view.Functions.Add ("reging", this.reging);
			view.Variable ["index"] = index;
			view.Variable ["size"] = 5;
			view.Variable ["total"] = activeModel.Count (string.Empty);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		public string reging(object [] args)
		{
			string active_id = Convert.ToString (args [0]);
			RegModel regModel = new RegModel ();
			int count = regModel.Count (string.Format ("actice_id='{0}'",active_id));
			if (count >= 20)
				return string.Format ("报名人数: {0}人", count);
			else {
				return null;
			}
		}

		/// <summary>
		/// 活动详情
		/// </summary>
		/// <param name="active_id">Active identifier.</param>
		public void active_detail(string active_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["detail"] = new ActiceVO (active_id);
			if (view.Cache ())
				return;
			view.Serialize ();
		}

		/// <summary>
		/// 海外实训
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void abr(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 常见问题解答
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void problem(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}
			
		/// <summary>
		/// 联系我们
		/// </summary>
		/// <param name="about_id">About identifier.</param>
		public void contact(string about_id)
		{
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			view.Variable ["about"] = new AboutVO (about_id);
			if (view.Cache ())
				return;
			view.Serialize (about_id);
		}

		/// <summary>
		/// 在线报名
		/// </summary>
		public void enlist()
		{
			EnlistModel enlistModel = new EnlistModel ();
			JabinfoKeyValue other = new JabinfoKeyValue ();
			if (this.IsPost) {
				this.Post["list_id"] = Jabinfo.Help.Basic.JabId;
				if (this.Files["image"] != null && this.Files["image"].ContentLength > 10)
				{
					Jabinfo.Help.Image.Save(this.Post["list_id"], this.Files["image"]);
					JabinfoKeyValue sizes = Jabinfo.Help.Config.Get("article.photosize");//切图保存
					try {
						foreach (string key in sizes.Keys)
						{
							string[] size = sizes[key].Split('x');
							Jabinfo.Help.Image.Resize(string.Format("{0}_{1}", this.Post["list_id"], key), this.Post["list_id"], Convert.ToInt32(size[0]), Convert.ToInt32(size[1]));
						}
					} catch (Exception ex) {
						this.Jump ("jujiang/home/emlist", "图片过大或格式不正确");
						return;
					}
				}
				other ["unit"] = this.Post ["unit"];      //所在单位
				other ["feature"] = this.Post ["feature"];  //政治面貌
				other ["coll"] = this.Post ["coll"];      // 毕业院校
				other ["mobile"] = this.Post ["mobile"];
				other ["email"] = this.Post ["email"];
				this.Post ["address"] = this.Post ["s_province"] + this.Post ["s_city"] + this.Post ["s_county"];
				this.Post ["birthday"] = Jabinfo.Help.Date.StringToDate (this.Post ["birthday"]).ToString();
				this.Post ["other"] = other.ToString ();
				this.Post ["addtime"] = Jabinfo.Help.Date.Now.ToString ();
				enlistModel.Insert (this.Post);
				this.Jump ("jujiang/home/enlist", "报名成功");
				return;
			}
			JabinfoView view = new JabinfoView (this.JabinfoContext);
			if (view.Cache ())
				return;
			view.Serialize ();
		}
	}
}

