﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class AboutModel : Sinbo.Model<AboutVO>
    {
		#region constructor
        public AboutModel()
            : base("jujiang", "about")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AboutVO Get(string about_id)
        {
            Object cache = CacheGet(about_id);
            if (cache != null)
                return (AboutVO)cache;
            AboutVO aboutVO = new AboutVO(about_id);
            if(aboutVO.About_id==null)
                return null;
            CacheSet(about_id, aboutVO);
            return aboutVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override AboutVO Get(DataRow row)
        {
            return new AboutVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public AboutVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public AboutVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["about_id"] = Jabinfo.Help.Basic.JabId;
            AboutVO aboutVO = new AboutVO();
            aboutVO.Insert(data);
            return data["about_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            AboutVO aboutVO = new AboutVO();
            aboutVO.Update(data);
            return data["about_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string about_id)
        {
            IDelete().Where("about_id", about_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(AboutVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}