﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class AcadeModel : Sinbo.Model<AcadeVO>
    {
		#region constructor
        public AcadeModel()
            : base("jujiang", "acade")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AcadeVO Get(string acade_id)
        {
            Object cache = CacheGet(acade_id);
            if (cache != null)
                return (AcadeVO)cache;
            AcadeVO acadeVO = new AcadeVO(acade_id);
            if(acadeVO.Acade_id==null)
                return null;
            CacheSet(acade_id, acadeVO);
            return acadeVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override AcadeVO Get(DataRow row)
        {
            return new AcadeVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public AcadeVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public AcadeVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["acade_id"] = Jabinfo.Help.Basic.JabId;
            AcadeVO acadeVO = new AcadeVO();
            acadeVO.Insert(data);
            return data["acade_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            AcadeVO acadeVO = new AcadeVO();
            acadeVO.Update(data);
            return data["acade_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string acade_id)
        {
            IDelete().Where("acade_id", acade_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(AcadeVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}