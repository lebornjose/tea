﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class ActiceModel : Sinbo.Model<ActiceVO>
    {
		#region constructor
        public ActiceModel()
            : base("jujiang", "actice")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override ActiceVO Get(string active_id)
        {
            Object cache = CacheGet(active_id);
            if (cache != null)
                return (ActiceVO)cache;
            ActiceVO acticeVO = new ActiceVO(active_id);
            if(acticeVO.Active_id==null)
                return null;
            CacheSet(active_id, acticeVO);
            return acticeVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override ActiceVO Get(DataRow row)
        {
            return new ActiceVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public ActiceVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public ActiceVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            ActiceVO acticeVO = new ActiceVO();
            acticeVO.Insert(data);
            return data["active_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            ActiceVO acticeVO = new ActiceVO();
            acticeVO.Update(data);
            return data["active_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string active_id)
        {
            IDelete().Where("active_id", active_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(ActiceVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}