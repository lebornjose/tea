﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 18:49:22
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class Adv_listModel : Sinbo.Model<Adv_listVO>
    {
		#region constructor
        public Adv_listModel()
            : base("jujiang", "adv_list")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override Adv_listVO Get(string list_id)
        {
            Object cache = CacheGet(list_id);
            if (cache != null)
                return (Adv_listVO)cache;
            Adv_listVO adv_listVO = new Adv_listVO(list_id);
            if(adv_listVO.List_id==null)
                return null;
            CacheSet(list_id, adv_listVO);
            return adv_listVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override Adv_listVO Get(DataRow row)
        {
            return new Adv_listVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public Adv_listVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public Adv_listVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["list_id"] = Jabinfo.Help.Basic.JabId;
            Adv_listVO adv_listVO = new Adv_listVO();
            adv_listVO.Insert(data);
            return data["list_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            Adv_listVO adv_listVO = new Adv_listVO();
            adv_listVO.Update(data);
            return data["list_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string list_id)
        {
            IDelete().Where("list_id", list_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(Adv_listVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}