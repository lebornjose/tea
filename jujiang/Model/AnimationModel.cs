﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class AnimationModel : Sinbo.Model<AnimationVO>
    {
		#region constructor
        public AnimationModel()
            : base("jujiang", "animation")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override AnimationVO Get(string animation_id)
        {
            Object cache = CacheGet(animation_id);
            if (cache != null)
                return (AnimationVO)cache;
            AnimationVO animationVO = new AnimationVO(animation_id);
            if(animationVO.Animation_id==null)
                return null;
            CacheSet(animation_id, animationVO);
            return animationVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override AnimationVO Get(DataRow row)
        {
            return new AnimationVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public AnimationVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public AnimationVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            AnimationVO animationVO = new AnimationVO();
            animationVO.Insert(data);
            return data["animation_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            AnimationVO animationVO = new AnimationVO();
            animationVO.Update(data);
            return data["animation_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string animation_id)
        {
            IDelete().Where("animation_id", animation_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(AnimationVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}