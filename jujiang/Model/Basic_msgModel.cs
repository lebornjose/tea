﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 9:00:22
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class Basic_msgModel : Sinbo.Model<Basic_msgVO>
    {
		#region constructor
        public Basic_msgModel()
            : base("jujiang", "basic_msg")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override Basic_msgVO Get(string basic_id)
        {
            Object cache = CacheGet(basic_id);
            if (cache != null)
                return (Basic_msgVO)cache;
            Basic_msgVO basic_msgVO = new Basic_msgVO(basic_id);
            if(basic_msgVO.Basic_id==null)
                return null;
            CacheSet(basic_id, basic_msgVO);
            return basic_msgVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override Basic_msgVO Get(DataRow row)
        {
            return new Basic_msgVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public Basic_msgVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public Basic_msgVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["basic_id"] = Jabinfo.Help.Basic.JabId;
            Basic_msgVO basic_msgVO = new Basic_msgVO();
            basic_msgVO.Insert(data);
            return data["basic_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            Basic_msgVO basic_msgVO = new Basic_msgVO();
            basic_msgVO.Update(data);
            return data["basic_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string basic_id)
        {
            IDelete().Where("basic_id", basic_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(Basic_msgVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}