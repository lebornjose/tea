﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class ConsultModel : Sinbo.Model<ConsultVO>
    {
		#region constructor
        public ConsultModel()
            : base("jujiang", "consult")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override ConsultVO Get(string consult_id)
        {
            Object cache = CacheGet(consult_id);
            if (cache != null)
                return (ConsultVO)cache;
            ConsultVO consultVO = new ConsultVO(consult_id);
            if(consultVO.Consult_id==null)
                return null;
            CacheSet(consult_id, consultVO);
            return consultVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override ConsultVO Get(DataRow row)
        {
            return new ConsultVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public ConsultVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public ConsultVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["consult_id"] = Jabinfo.Help.Basic.JabId;
            ConsultVO consultVO = new ConsultVO();
            consultVO.Insert(data);
            return data["consult_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            ConsultVO consultVO = new ConsultVO();
            consultVO.Update(data);
            return data["consult_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string consult_id)
        {
            IDelete().Where("consult_id", consult_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(ConsultVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}