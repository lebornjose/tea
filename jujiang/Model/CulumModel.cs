﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class CulumModel : Sinbo.Model<CulumVO>
    {
		#region constructor
        public CulumModel()
            : base("jujiang", "culum")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override CulumVO Get(string Column_id)
        {
            Object cache = CacheGet(Column_id);
            if (cache != null)
                return (CulumVO)cache;
            CulumVO culumVO = new CulumVO(Column_id);
            if(culumVO.Column_id==null)
                return null;
            CacheSet(Column_id, culumVO);
            return culumVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override CulumVO Get(DataRow row)
        {
            return new CulumVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public CulumVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public CulumVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["Column_id"] = Jabinfo.Help.Basic.JabId;
            CulumVO culumVO = new CulumVO();
            culumVO.Insert(data);
            return data["Column_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            CulumVO culumVO = new CulumVO();
            culumVO.Update(data);
            return data["Column_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string Column_id)
        {
            IDelete().Where("Column_id", Column_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(CulumVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}