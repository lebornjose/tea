﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class DemeanorModel : Sinbo.Model<DemeanorVO>
    {
		#region constructor
        public DemeanorModel()
            : base("jujiang", "demeanor")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override DemeanorVO Get(string demeanor_id)
        {
            Object cache = CacheGet(demeanor_id);
            if (cache != null)
                return (DemeanorVO)cache;
            DemeanorVO demeanorVO = new DemeanorVO(demeanor_id);
            if(demeanorVO.Demeanor_id==null)
                return null;
            CacheSet(demeanor_id, demeanorVO);
            return demeanorVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override DemeanorVO Get(DataRow row)
        {
            return new DemeanorVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public DemeanorVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public DemeanorVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            DemeanorVO demeanorVO = new DemeanorVO();
            demeanorVO.Insert(data);
            return data["demeanor_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            DemeanorVO demeanorVO = new DemeanorVO();
            demeanorVO.Update(data);
            return data["demeanor_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string demeanor_id)
        {
            IDelete().Where("demeanor_id", demeanor_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(DemeanorVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}