﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class DirectorModel : Sinbo.Model<DirectorVO>
    {
		#region constructor
        public DirectorModel()
            : base("jujiang", "director")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override DirectorVO Get(string director_id)
        {
            Object cache = CacheGet(director_id);
            if (cache != null)
                return (DirectorVO)cache;
            DirectorVO directorVO = new DirectorVO(director_id);
            if(directorVO.Director_id==null)
                return null;
            CacheSet(director_id, directorVO);
            return directorVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override DirectorVO Get(DataRow row)
        {
            return new DirectorVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public DirectorVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public DirectorVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            DirectorVO directorVO = new DirectorVO();
            directorVO.Insert(data);
            return data["director_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            DirectorVO directorVO = new DirectorVO();
            directorVO.Update(data);
            return data["director_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string director_id)
        {
            IDelete().Where("director_id", director_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(DirectorVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}