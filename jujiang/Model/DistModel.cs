﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 8:50:30
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class DistModel : Sinbo.Model<DistVO>
    {
		#region constructor
        public DistModel()
            : base("jujiang", "dist")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override DistVO Get(string dist_id)
        {
            Object cache = CacheGet(dist_id);
            if (cache != null)
                return (DistVO)cache;
            DistVO distVO = new DistVO(dist_id);
            if(distVO.Dist_id==null)
                return null;
            CacheSet(dist_id, distVO);
            return distVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override DistVO Get(DataRow row)
        {
            return new DistVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public DistVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public DistVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            DistVO distVO = new DistVO();
            distVO.Insert(data);
            return data["dist_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            DistVO distVO = new DistVO();
            distVO.Update(data);
            return data["dist_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string dist_id)
        {
            IDelete().Where("dist_id", dist_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(DistVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}