﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/8/29 17:05:28
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class EnlistModel : Sinbo.Model<EnlistVO>
    {
		#region constructor
        public EnlistModel()
            : base("jujiang", "enlist")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override EnlistVO Get(string list_id)
        {
            Object cache = CacheGet(list_id);
            if (cache != null)
                return (EnlistVO)cache;
            EnlistVO enlistVO = new EnlistVO(list_id);
            if(enlistVO.List_id==null)
                return null;
            CacheSet(list_id, enlistVO);
            return enlistVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override EnlistVO Get(DataRow row)
        {
            return new EnlistVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public EnlistVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public EnlistVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            EnlistVO enlistVO = new EnlistVO();
            enlistVO.Insert(data);
            return data["list_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            EnlistVO enlistVO = new EnlistVO();
            enlistVO.Update(data);
            return data["list_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string list_id)
        {
            IDelete().Where("list_id", list_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(EnlistVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}