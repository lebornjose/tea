﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class ItemModel : Sinbo.Model<ItemVO>
    {
		#region constructor
        public ItemModel()
            : base("jujiang", "item")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override ItemVO Get(string item_id)
        {
            Object cache = CacheGet(item_id);
            if (cache != null)
                return (ItemVO)cache;
            ItemVO itemVO = new ItemVO(item_id);
            if(itemVO.Item_id==null)
                return null;
            CacheSet(item_id, itemVO);
            return itemVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override ItemVO Get(DataRow row)
        {
            return new ItemVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public ItemVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public ItemVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["item_id"] = Jabinfo.Help.Basic.JabId;
            ItemVO itemVO = new ItemVO();
            itemVO.Insert(data);
            return data["item_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            ItemVO itemVO = new ItemVO();
            itemVO.Update(data);
            return data["item_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string item_id)
        {
            IDelete().Where("item_id", item_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(ItemVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}