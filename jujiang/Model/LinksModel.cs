﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class LinksModel : Sinbo.Model<LinksVO>
    {
		#region constructor
        public LinksModel()
            : base("jujiang", "links")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override LinksVO Get(string links_id)
        {
            Object cache = CacheGet(links_id);
            if (cache != null)
                return (LinksVO)cache;
            LinksVO linksVO = new LinksVO(links_id);
            if(linksVO.Links_id==null)
                return null;
            CacheSet(links_id, linksVO);
            return linksVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override LinksVO Get(DataRow row)
        {
            return new LinksVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public LinksVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public LinksVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["links_id"] = Jabinfo.Help.Basic.JabId;
            LinksVO linksVO = new LinksVO();
            linksVO.Insert(data);
            return data["links_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            LinksVO linksVO = new LinksVO();
            linksVO.Update(data);
            return data["links_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string links_id)
        {
            IDelete().Where("links_id", links_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(LinksVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}