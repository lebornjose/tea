﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 8:54:12
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class OrginModel : Sinbo.Model<OrginVO>
    {
		#region constructor
        public OrginModel()
            : base("jujiang", "orgin")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override OrginVO Get(string orgin_id)
        {
            Object cache = CacheGet(orgin_id);
            if (cache != null)
                return (OrginVO)cache;
            OrginVO orginVO = new OrginVO(orgin_id);
            if(orginVO.Orgin_id==null)
                return null;
            CacheSet(orgin_id, orginVO);
            return orginVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override OrginVO Get(DataRow row)
        {
            return new OrginVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public OrginVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public OrginVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            OrginVO orginVO = new OrginVO();
            orginVO.Insert(data);
            return data["orgin_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            OrginVO orginVO = new OrginVO();
            orginVO.Update(data);
            return data["orgin_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string orgin_id)
        {
            IDelete().Where("orgin_id", orgin_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(OrginVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}