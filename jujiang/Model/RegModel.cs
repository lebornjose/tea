﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 13:12:33
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class RegModel : Sinbo.Model<RegVO>
    {
		#region constructor
        public RegModel()
            : base("jujiang", "reg")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override RegVO Get(string reg_id)
        {
            Object cache = CacheGet(reg_id);
            if (cache != null)
                return (RegVO)cache;
            RegVO regVO = new RegVO(reg_id);
            if(regVO.Reg_id==null)
                return null;
            CacheSet(reg_id, regVO);
            return regVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override RegVO Get(DataRow row)
        {
            return new RegVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public RegVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public RegVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["reg_id"] = Jabinfo.Help.Basic.JabId;
            RegVO regVO = new RegVO();
            regVO.Insert(data);
            return data["reg_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            RegVO regVO = new RegVO();
            regVO.Update(data);
            return data["reg_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string reg_id)
        {
            IDelete().Where("reg_id", reg_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(RegVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}