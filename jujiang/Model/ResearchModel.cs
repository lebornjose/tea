﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class ResearchModel : Sinbo.Model<ResearchVO>
    {
		#region constructor
        public ResearchModel()
            : base("jujiang", "research")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override ResearchVO Get(string research_id)
        {
            Object cache = CacheGet(research_id);
            if (cache != null)
                return (ResearchVO)cache;
            ResearchVO researchVO = new ResearchVO(research_id);
            if(researchVO.Research_id==null)
                return null;
            CacheSet(research_id, researchVO);
            return researchVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override ResearchVO Get(DataRow row)
        {
            return new ResearchVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public ResearchVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public ResearchVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            ResearchVO researchVO = new ResearchVO();
            researchVO.Insert(data);
            return data["research_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            ResearchVO researchVO = new ResearchVO();
            researchVO.Update(data);
            return data["research_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string research_id)
        {
            IDelete().Where("research_id", research_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(ResearchVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}