﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class TeacherModel : Sinbo.Model<TeacherVO>
    {
		#region constructor
        public TeacherModel()
            : base("jujiang", "teacher")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override TeacherVO Get(string teacher_id)
        {
            Object cache = CacheGet(teacher_id);
            if (cache != null)
                return (TeacherVO)cache;
            TeacherVO teacherVO = new TeacherVO(teacher_id);
            if(teacherVO.Teacher_id==null)
                return null;
            CacheSet(teacher_id, teacherVO);
            return teacherVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override TeacherVO Get(DataRow row)
        {
            return new TeacherVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public TeacherVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public TeacherVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            TeacherVO teacherVO = new TeacherVO();
            teacherVO.Insert(data);
            return data["teacher_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            TeacherVO teacherVO = new TeacherVO();
            teacherVO.Update(data);
            return data["teacher_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string teacher_id)
        {
            IDelete().Where("teacher_id", teacher_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(TeacherVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}