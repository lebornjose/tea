﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;
using Jabinfo.Jujiang.VO;

namespace Jabinfo.Jujiang.Model
{
    class TeamModel : Sinbo.Model<TeamVO>
    {
		#region constructor
        public TeamModel()
            : base("jujiang", "team")
        {
        }
		#endregion

		#region Get
		
        /// <summary>
        /// 获取一条记录
        ///</summary>
        public override TeamVO Get(string team_id)
        {
            Object cache = CacheGet(team_id);
            if (cache != null)
                return (TeamVO)cache;
            TeamVO teamVO = new TeamVO(team_id);
            if(teamVO.Team_id==null)
                return null;
            CacheSet(team_id, teamVO);
            return teamVO;
        }

		/// <summary>
        /// 获取一条记录
        ///</summary>
		public override TeamVO Get(DataRow row)
        {
            return new TeamVO(row);
        }
		#endregion

		#region Select
        /// <summary>
        /// 查询数据
        ///</summary>
        public TeamVO[] Select(string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Table()
            );
        }
        /// <summary>
        /// 分页查询数据
        ///</summary>
        /// <param name="index">页面索引</param>
        /// <param name="length">分页长度</param>
        /// <returns></returns>
        public TeamVO[] Select(int index, int length ,string where ,string order)
        {
			return Collection(
				ISelect("*").Where(where).Order(order).Limit(index, length).Table()
            );
        }
		#endregion

		#region Count
        /// <summary>
        /// 统计数据
        ///</summary>
        public int Count(string where)
        {
            return ISelect().Where(where).Count();
        }
		#endregion

		#region Insert
        /// <summary>
        /// 插入数据
        ///</summary>
        public string Insert(JabinfoKeyValue data)
        {
            data["team_id"] = Jabinfo.Help.Basic.JabId;
            TeamVO teamVO = new TeamVO();
            teamVO.Insert(data);
            return data["team_id"];
        }
		#endregion

		#region Update
        /// <summary>
        /// 更新数据
        ///</summary>
        public string Update(JabinfoKeyValue data)
        {
            TeamVO teamVO = new TeamVO();
            teamVO.Update(data);
            return data["team_id"];
        }
		#endregion

		#region Delete
        /// <summary>
        /// 删除数据
        ///</summary>
        public void Delete(string team_id)
        {
            IDelete().Where("team_id", team_id, DataType.Char, 32).Excute();
        }
		#endregion

		#region ToJson
        public string ToJson(TeamVO[] list)
        {
            if (list == null || list.Length == 0)
                return "[]";
            int length = list.Length;
            string[] result = new string[length];
            for (int i = 0; i < length; i++)
            {
                result[i] = list[i].ToJson();
            }
            return string.Format("[{0}]", string.Join(",", result));
        }
        #endregion
    }
}