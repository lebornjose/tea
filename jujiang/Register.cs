﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;

namespace Jabinfo.Jujiang
{
    /// <summary>
    /// 控制器注册
    /// </summary>
    public class Register
    {
        private static Register _Instance = null;
        public static Register Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new Register();
                return _Instance;
            }
        }
		private Register() { }
		

		public object animation { get { return new Animation(); } }

		public object teacher { get { return new Teacher(); } }

		public object team { get { return new Team(); } }

		public object research { get { return new Research(); } }

		public object demeanor { get { return new Demeanor(); } }

		public object links { get { return new Links(); } }

		public object about { get { return new About(); } }

		public object item { get { return new Item(); } }

		public object director { get { return new Director(); } }

		public object consult { get { return new Consult(); } }

		public object culum { get { return new Culum(); } }

		public object acade { get { return new Acade(); } }

		public object actice { get { return new Actice(); } }

		public object dist{get{return new Dist (); }}

		public object orgin{get{return new Orgin (); }}

		public object basic_msg{get{return new Basic_msg (); }}

		public object reg{get{return new Reg ();}}

		public object advertise{get{return new Advertise (); }}

		public object adv_list{get{return new Adv_list ();}}

		public object home{get{return new Home (); }}

		public object enlist{get{return new Enlist ();}}

		public API api { get{ return API.Instance; } }
    }
}