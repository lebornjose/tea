﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:34:03
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// aboutVO
	///	</summary>
	[Serializable]
	public class AboutVO
	{
		#region	Property

		private String  _About_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  About_id
		{
			get{ return _About_id; }
			set{ _About_id = value; }
		}


		private String  _Title;
		/// <summary>
		/// 标题
		///</summary>
		public String  Title
		{
			get{ return _Title; }
			set{ _Title = value; }
		}


		private String  _Summry;
		/// <summary>
		/// 简介
		///</summary>
		public String  Summry
		{
			get{ return _Summry; }
			set{ _Summry = value; }
		}


		private String  _Content;
		/// <summary>
		/// 内容
		///</summary>
		public String  Content
		{
			get{ return _Content; }
			set{ _Content = value; }
		}


		private String  _Type;
		/// <summary>
		/// 类型
		///</summary>
		public String  Type
		{
			get{ return _Type; }
			set{ _Type = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// about
		/// </summary>
		public AboutVO(){}

		/// <summary>
		/// about
		/// </summary>
		/// <param name="about_id">Primary Key</param>
		public AboutVO(string about_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("about").Where("about_id",about_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// about
		/// </summary>
		/// <param name="about_id">Primary Key</param>
		public AboutVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.About_id = dataReader["about_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Summry = dataReader["summry"] as string;
				this.Content = dataReader["content"] as string;
				this.Type = dataReader["type"] as string;
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// about Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("about");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("about");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("about_id", data["about_id"],DataType.Char, 24).
				Value("title", data["title"],DataType.Varchar, 30).
				Value("summry", data["summry"],DataType.Varchar, 50).
				Value("content", data["content"],DataType.Text).
				Value("type", data["type"],DataType.Char, 2).
				Excute();
		}

		/// <summary>
		/// about Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("about").
				Set("title", data["title"],DataType.Varchar, 30).
				Set("summry", data["summry"],DataType.Varchar, 50).
				Set("content", data["content"],DataType.Text).
				Set("type", data["type"],DataType.Char, 2).
				Where("about_id", data["about_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.About_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("about_id", this.About_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("summry", this.Summry);
			json.AddProperty("content", this.Content);
			json.AddProperty("type", this.Type);
			return json.Result;
		}
	}
}