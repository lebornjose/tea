﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// acadeVO
    ///	</summary>
    [Serializable]
    public class AcadeVO
    {
        #region	Property

        private String  _Acade_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Acade_id
        {
            get{ return _Acade_id; }
            set{ _Acade_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 标题
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Summary;
        /// <summary>
        /// 简介
        ///</summary>
        public String  Summary
        {
            get{ return _Summary; }
            set{ _Summary = value; }
        }
		

        private String  _Content;
        /// <summary>
        /// 详情
        ///</summary>
        public String  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }
		

        private Int32  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// acade
        /// </summary>
        public AcadeVO(){}
	
        /// <summary>
        /// acade
        /// </summary>
        /// <param name="acade_id">Primary Key</param>
        public AcadeVO(string acade_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("acade").Where("acade_id",acade_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// acade
        /// </summary>
        /// <param name="acade_id">Primary Key</param>
        public AcadeVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Acade_id = dataReader["acade_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Summary = dataReader["summary"] as string;
				this.Content = dataReader["content"] as string;
				this.Index = Convert.ToInt32(dataReader["index"]);
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// acade Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("acade");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("acade");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("acade_id", data["acade_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 30).
            Value("summary", data["summary"],DataType.Varchar, 100).
            Value("content", data["content"],DataType.Text).
            Value("index", data["index"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// acade Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("acade").
            Set("title", data["title"],DataType.Varchar, 30).
            Set("summary", data["summary"],DataType.Varchar, 100).
            Set("content", data["content"],DataType.Text).
            Set("index", data["index"],DataType.Int).
            Where("acade_id", data["acade_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Acade_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("acade_id", this.Acade_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("summary", this.Summary);
			json.AddProperty("content", this.Content);
			json.AddProperty("index", this.Index);
			return json.Result;
        }
    }
}