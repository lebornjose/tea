﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/8/6 14:35:08
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// acticeVO
	///	</summary>
	[Serializable]
	public class ActiceVO
	{
		#region	Property

		private String  _Active_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  Active_id
		{
			get{ return _Active_id; }
			set{ _Active_id = value; }
		}


		private String  _Title;
		/// <summary>
		/// 标题
		///</summary>
		public String  Title
		{
			get{ return _Title; }
			set{ _Title = value; }
		}


		private String  _Unit;
		/// <summary>
		/// 举办单位
		///</summary>
		public String  Unit
		{
			get{ return _Unit; }
			set{ _Unit = value; }
		}


		private String  _Address;
		/// <summary>
		/// 举办地点
		///</summary>
		public String  Address
		{
			get{ return _Address; }
			set{ _Address = value; }
		}


		private String  _Price;
		/// <summary>
		/// 相关费用
		///</summary>
		public String  Price
		{
			get{ return _Price; }
			set{ _Price = value; }
		}


		private String  _Num;
		/// <summary>
		/// 活动人数
		///</summary>
		public String  Num
		{
			get{ return _Num; }
			set{ _Num = value; }
		}


		private Int32  _Starttime;
		/// <summary>
		/// 举办时间
		///</summary>
		public Int32  Starttime
		{
			get{ return _Starttime; }
			set{ _Starttime = value; }
		}


		private String  _Summary;
		/// <summary>
		/// 简介
		///</summary>
		public String  Summary
		{
			get{ return _Summary; }
			set{ _Summary = value; }
		}


		private String  _Content;
		/// <summary>
		/// 详情
		///</summary>
		public String  Content
		{
			get{ return _Content; }
			set{ _Content = value; }
		}


		private String  _Status;
		/// <summary>
		/// 0.进行中,1.已结束
		///</summary>
		public String  Status
		{
			get{ return _Status; }
			set{ _Status = value; }
		}


		private Int32  _Date;
		/// <summary>
		/// 添加时间
		///</summary>
		public Int32  Date
		{
			get{ return _Date; }
			set{ _Date = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// actice
		/// </summary>
		public ActiceVO(){}

		/// <summary>
		/// actice
		/// </summary>
		/// <param name="active_id">Primary Key</param>
		public ActiceVO(string active_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("actice").Where("active_id",active_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// actice
		/// </summary>
		/// <param name="active_id">Primary Key</param>
		public ActiceVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.Active_id = dataReader["active_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Unit = dataReader["unit"] as string;
				this.Address = dataReader["address"] as string;
				this.Price = dataReader["price"] as string;
				this.Num = dataReader["num"] as string;
				this.Starttime = Convert.ToInt32(dataReader["starttime"]);
				this.Summary = dataReader["summary"] as string;
				this.Content = dataReader["content"] as string;
				this.Status = dataReader["status"] as string;
				this.Date = Convert.ToInt32(dataReader["date"]);
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// actice Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("actice");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("actice");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("active_id", data["active_id"],DataType.Char, 24).
				Value("title", data["title"],DataType.Varchar, 30).
				Value("unit", data["unit"],DataType.Varchar, 50).
				Value("address", data["address"],DataType.Varchar, 200).
				Value("price", data["price"],DataType.Varchar, 50).
				Value("num", data["num"],DataType.Varchar, 10).
				Value("starttime", data["starttime"],DataType.Int).
				Value("summary", data["summary"],DataType.Varchar, 100).
				Value("content", data["content"],DataType.Text).
				Value("status", data["status"],DataType.Char, 1).
				Value("date", data["date"],DataType.Int).
				Excute();
		}

		/// <summary>
		/// actice Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("actice").
				Set("title", data["title"],DataType.Varchar, 30).
				Set("unit", data["unit"],DataType.Varchar, 50).
				Set("address", data["address"],DataType.Varchar, 200).
				Set("price", data["price"],DataType.Varchar, 50).
				Set("num", data["num"],DataType.Varchar, 10).
				Set("starttime", data["starttime"],DataType.Int).
				Set("summary", data["summary"],DataType.Varchar, 100).
				Set("content", data["content"],DataType.Text).
				Set("status", data["status"],DataType.Char, 1).
				Set("date", data["date"],DataType.Int).
				Where("active_id", data["active_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.Active_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("active_id", this.Active_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("unit", this.Unit);
			json.AddProperty("address", this.Address);
			json.AddProperty("price", this.Price);
			json.AddProperty("num", this.Num);
			json.AddTimeProperty("starttime", this.Starttime);
			json.AddProperty("summary", this.Summary);
			json.AddProperty("content", this.Content);
			json.AddProperty("status", this.Status);
			json.AddProperty("date", this.Date);
			return json.Result;
		}
	}
}