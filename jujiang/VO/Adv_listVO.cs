﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/31 10:42:23
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// adv_listVO
	///	</summary>
	[Serializable]
	public class Adv_listVO
	{
		#region	Property

		private String  _List_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  List_id
		{
			get{ return _List_id; }
			set{ _List_id = value; }
		}


		private String  _Name;
		/// <summary>
		/// 姓名
		///</summary>
		public String  Name
		{
			get{ return _Name; }
			set{ _Name = value; }
		}


		private String  _Mobile;
		/// <summary>
		/// 电话
		///</summary>
		public String  Mobile
		{
			get{ return _Mobile; }
			set{ _Mobile = value; }
		}


		private String  _Email;
		/// <summary>
		/// 邮箱
		///</summary>
		public String  Email
		{
			get{ return _Email; }
			set{ _Email = value; }
		}


		private Int32  _Date;
		/// <summary>
		/// 报名时间
		///</summary>
		public Int32  Date
		{
			get{ return _Date; }
			set{ _Date = value; }
		}


		private String  _Advertise_id;
		/// <summary>
		/// 招聘编号
		///</summary>
		public String  Advertise_id
		{
			get{ return _Advertise_id; }
			set{ _Advertise_id = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// adv_list
		/// </summary>
		public Adv_listVO(){}

		/// <summary>
		/// adv_list
		/// </summary>
		/// <param name="list_id">Primary Key</param>
		public Adv_listVO(string list_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("adv_list").Where("list_id",list_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// adv_list
		/// </summary>
		/// <param name="list_id">Primary Key</param>
		public Adv_listVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.List_id = dataReader["list_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Mobile = dataReader["mobile"] as string;
				this.Email = dataReader["email"] as string;
				this.Date = Convert.ToInt32(dataReader["date"]);
				this.Advertise_id = dataReader["advertise_id"] as string;
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// adv_list Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("adv_list");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("adv_list");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("list_id", data["list_id"],DataType.Char, 24).
				Value("name", data["name"],DataType.Varchar, 30).
				Value("mobile", data["mobile"],DataType.Varchar, 30).
				Value("email", data["email"],DataType.Varchar, 30).
				Value("date", data["date"],DataType.Int).
				Value("advertise_id", data["advertise_id"],DataType.Char, 24).
				Excute();
		}

		/// <summary>
		/// adv_list Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("adv_list").
				Set("name", data["name"],DataType.Varchar, 30).
				Set("mobile", data["mobile"],DataType.Varchar, 30).
				Set("email", data["email"],DataType.Varchar, 30).
				Set("date", data["date"],DataType.Int).
				Set("advertise_id", data["advertise_id"],DataType.Char, 24).
				Where("list_id", data["list_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.List_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("list_id", this.List_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("mobile", this.Mobile);
			json.AddProperty("email", this.Email);
			json.AddProperty("date", this.Date);
			json.AddIDProperty("advertise_id", this.Advertise_id);
			return json.Result;
		}
	}
}