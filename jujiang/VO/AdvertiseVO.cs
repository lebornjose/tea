﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/31 9:10:34
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// advertiseVO
	///	</summary>
	[Serializable]
	public class AdvertiseVO
	{
		#region	Property

		private String  _Advertise_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  Advertise_id
		{
			get{ return _Advertise_id; }
			set{ _Advertise_id = value; }
		}


		private String  _Title;
		/// <summary>
		/// 标题
		///</summary>
		public String  Title
		{
			get{ return _Title; }
			set{ _Title = value; }
		}


		private String  _Job;
		/// <summary>
		/// 岗位名称
		///</summary>
		public String  Job
		{
			get{ return _Job; }
			set{ _Job = value; }
		}


		private Double  _People;
		/// <summary>
		/// 招聘人数
		///</summary>
		public Double  People
		{
			get{ return _People; }
			set{ _People = value; }
		}


		private String  _Detail;
		/// <summary>
		/// 详细信息
		///</summary>
		public String  Detail
		{
			get{ return _Detail; }
			set{ _Detail = value; }
		}


		private String  _Status;
		/// <summary>
		/// 1.进行中2.以结束
		///</summary>
		public String  Status
		{
			get{ return _Status; }
			set{ _Status = value; }
		}


		private Int32  _Date;
		/// <summary>
		/// 发布如期
		///</summary>
		public Int32  Date
		{
			get{ return _Date; }
			set{ _Date = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// advertise
		/// </summary>
		public AdvertiseVO(){}

		/// <summary>
		/// advertise
		/// </summary>
		/// <param name="advertise_id">Primary Key</param>
		public AdvertiseVO(string advertise_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("advertise").Where("advertise_id",advertise_id,DataType.Varchar, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// advertise
		/// </summary>
		/// <param name="advertise_id">Primary Key</param>
		public AdvertiseVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.Advertise_id = dataReader["advertise_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Job = dataReader["job"] as string;
				this.People = Convert.ToDouble(dataReader["people"]);
				this.Detail = dataReader["detail"] as string;
				this.Status = dataReader["status"] as string;
				this.Date = Convert.ToInt32(dataReader["date"]);
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// advertise Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("advertise");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("advertise");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("advertise_id", data["advertise_id"],DataType.Varchar, 24).
				Value("title", data["title"],DataType.Varchar, 50).
				Value("job", data["job"],DataType.Varchar, 50).
				Value("people", data["people"],DataType.Double).
				Value("detail", data["detail"],DataType.Text).
				Value("status", data["status"],DataType.Char, 1).
				Value("date", data["date"],DataType.Int).
				Excute();
		}

		/// <summary>
		/// advertise Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("advertise").
				Set("title", data["title"],DataType.Varchar, 50).
				Set("job", data["job"],DataType.Varchar, 50).
				Set("people", data["people"],DataType.Double).
				Set("detail", data["detail"],DataType.Text).
				Set("status", data["status"],DataType.Char, 1).
				Set("date", data["date"],DataType.Int).
				Where("advertise_id", data["advertise_id"],DataType.Varchar, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.Advertise_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddProperty("advertise_id", this.Advertise_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("job", this.Job);
			json.AddProperty("people", this.People.ToString());
			json.AddProperty("detail", this.Detail);
			json.AddProperty("status", this.Status);
			json.AddProperty("date", this.Date);
			return json.Result;
		}
	}
}