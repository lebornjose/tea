﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 9:00:22
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// basic_msgVO
    ///	</summary>
    [Serializable]
    public class Basic_msgVO
    {
        #region	Property

        private String  _Basic_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Basic_id
        {
            get{ return _Basic_id; }
            set{ _Basic_id = value; }
        }
		

        private String  _Name;
        /// <summary>
        /// 名称
        ///</summary>
        public String  Name
        {
            get{ return _Name; }
            set{ _Name = value; }
        }
		

        private String  _Mobile;
        /// <summary>
        /// 电话1
        ///</summary>
        public String  Mobile
        {
            get{ return _Mobile; }
            set{ _Mobile = value; }
        }
		

        private String  _Moble2;
        /// <summary>
        /// 电话2
        ///</summary>
        public String  Moble2
        {
            get{ return _Moble2; }
            set{ _Moble2 = value; }
        }
		

        private String  _Email;
        /// <summary>
        /// 邮箱
        ///</summary>
        public String  Email
        {
            get{ return _Email; }
            set{ _Email = value; }
        }
		

        private String  _Copyright;
        /// <summary>
        /// 版权号
        ///</summary>
        public String  Copyright
        {
            get{ return _Copyright; }
            set{ _Copyright = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// basic_msg
        /// </summary>
        public Basic_msgVO(){}
	
        /// <summary>
        /// basic_msg
        /// </summary>
        /// <param name="basic_id">Primary Key</param>
        public Basic_msgVO(string basic_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("basic_msg").Where("basic_id",basic_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// basic_msg
        /// </summary>
        /// <param name="basic_id">Primary Key</param>
        public Basic_msgVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Basic_id = dataReader["basic_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Mobile = dataReader["mobile"] as string;
				this.Moble2 = dataReader["moble2"] as string;
				this.Email = dataReader["email"] as string;
				this.Copyright = dataReader["copyright"] as string;
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// basic_msg Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("basic_msg");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("basic_msg");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("basic_id", data["basic_id"],DataType.Char, 24).
            Value("name", data["name"],DataType.Varchar, 50).
            Value("mobile", data["mobile"],DataType.Varchar, 30).
            Value("moble2", data["moble2"],DataType.Varchar, 30).
            Value("email", data["email"],DataType.Varchar, 30).
            Value("copyright", data["copyright"],DataType.Varchar, 50).
            Excute();
        }

        /// <summary>
        /// basic_msg Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("basic_msg").
            Set("name", data["name"],DataType.Varchar, 50).
            Set("mobile", data["mobile"],DataType.Varchar, 30).
            Set("moble2", data["moble2"],DataType.Varchar, 30).
            Set("email", data["email"],DataType.Varchar, 30).
            Set("copyright", data["copyright"],DataType.Varchar, 50).
            Where("basic_id", data["basic_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Basic_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("basic_id", this.Basic_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("mobile", this.Mobile);
			json.AddProperty("moble2", this.Moble2);
			json.AddProperty("email", this.Email);
			json.AddProperty("copyright", this.Copyright);
			return json.Result;
        }
    }
}