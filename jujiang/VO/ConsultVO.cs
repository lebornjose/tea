﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 12:57:54
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// consultVO
	///	</summary>
	[Serializable]
	public class ConsultVO
	{
		#region	Property

		private String  _Consult_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  Consult_id
		{
			get{ return _Consult_id; }
			set{ _Consult_id = value; }
		}


		private String  _Name;
		/// <summary>
		/// 姓名
		///</summary>
		public String  Name
		{
			get{ return _Name; }
			set{ _Name = value; }
		}


		private String  _Unit;
		/// <summary>
		/// 所在单位
		///</summary>
		public String  Unit
		{
			get{ return _Unit; }
			set{ _Unit = value; }
		}


		private String  _Duty;
		/// <summary>
		/// 职位
		///</summary>
		public String  Duty
		{
			get{ return _Duty; }
			set{ _Duty = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// consult
		/// </summary>
		public ConsultVO(){}

		/// <summary>
		/// consult
		/// </summary>
		/// <param name="consult_id">Primary Key</param>
		public ConsultVO(string consult_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("consult").Where("consult_id",consult_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// consult
		/// </summary>
		/// <param name="consult_id">Primary Key</param>
		public ConsultVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.Consult_id = dataReader["consult_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Unit = dataReader["unit"] as string;
				this.Duty = dataReader["duty"] as string;
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// consult Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("consult");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("consult");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("consult_id", data["consult_id"],DataType.Char, 24).
				Value("name", data["name"],DataType.Varchar, 30).
				Value("unit", data["unit"],DataType.Varchar, 50).
				Value("duty", data["duty"],DataType.Varchar, 30).
				Excute();
		}

		/// <summary>
		/// consult Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("consult").
				Set("name", data["name"],DataType.Varchar, 30).
				Set("unit", data["unit"],DataType.Varchar, 50).
				Set("duty", data["duty"],DataType.Varchar, 30).
				Where("consult_id", data["consult_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.Consult_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("consult_id", this.Consult_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("unit", this.Unit);
			json.AddProperty("duty", this.Duty);
			return json.Result;
		}
	}
}