﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// culumVO
    ///	</summary>
    [Serializable]
    public class CulumVO
    {
        #region	Property

        private String  _Column_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Column_id
        {
            get{ return _Column_id; }
            set{ _Column_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 标题
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Content;
        /// <summary>
        /// 内容
        ///</summary>
        public String  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }
		

        private String  _Type;
        /// <summary>
        /// 类别1.基础课程2.专业课程3.专业提高
        ///</summary>
        public String  Type
        {
            get{ return _Type; }
            set{ _Type = value; }
        }
		

        private Int32  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// culum
        /// </summary>
        public CulumVO(){}
	
        /// <summary>
        /// culum
        /// </summary>
        /// <param name="Column_id">Primary Key</param>
        public CulumVO(string Column_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("culum").Where("Column_id",Column_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// culum
        /// </summary>
        /// <param name="Column_id">Primary Key</param>
        public CulumVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Column_id = dataReader["Column_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Content = dataReader["content"] as string;
				this.Type = dataReader["type"] as string;
				this.Index = Convert.ToInt32(dataReader["index"]);
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// culum Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("culum");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("culum");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("Column_id", data["Column_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 30).
            Value("content", data["content"],DataType.Varchar, 200).
            Value("type", data["type"],DataType.Char, 1).
            Value("index", data["index"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// culum Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("culum").
            Set("title", data["title"],DataType.Varchar, 30).
            Set("content", data["content"],DataType.Varchar, 200).
            Set("type", data["type"],DataType.Char, 1).
            Set("index", data["index"],DataType.Int).
            Where("Column_id", data["Column_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Column_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("Column_id", this.Column_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("content", this.Content);
			json.AddProperty("type", this.Type);
			json.AddProperty("index", this.Index);
			return json.Result;
        }
    }
}