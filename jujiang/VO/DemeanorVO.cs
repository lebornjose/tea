﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 15:23:21
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// demeanorVO
	///	</summary>
	[Serializable]
	public class DemeanorVO
	{
		#region	Property

		private String  _Demeanor_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  Demeanor_id
		{
			get{ return _Demeanor_id; }
			set{ _Demeanor_id = value; }
		}


		private String  _Name;
		/// <summary>
		/// 学员姓名
		///</summary>
		public String  Name
		{
			get{ return _Name; }
			set{ _Name = value; }
		}


		private String  _Saying;
		/// <summary>
		/// 风采语录
		///</summary>
		public String  Saying
		{
			get{ return _Saying; }
			set{ _Saying = value; }
		}


		private String  _Note;
		/// <summary>
		/// 个人简介
		///</summary>
		public String  Note
		{
			get{ return _Note; }
			set{ _Note = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// demeanor
		/// </summary>
		public DemeanorVO(){}

		/// <summary>
		/// demeanor
		/// </summary>
		/// <param name="demeanor_id">Primary Key</param>
		public DemeanorVO(string demeanor_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("demeanor").Where("demeanor_id",demeanor_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// demeanor
		/// </summary>
		/// <param name="demeanor_id">Primary Key</param>
		public DemeanorVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.Demeanor_id = dataReader["demeanor_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Saying = dataReader["saying"] as string;
				this.Note = dataReader["note"] as string;
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// demeanor Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("demeanor");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("demeanor");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("demeanor_id", data["demeanor_id"],DataType.Char, 24).
				Value("name", data["name"],DataType.Varchar, 30).
				Value("saying", data["saying"],DataType.Varchar, 50).
				Value("note", data["note"],DataType.Text).
				Excute();
		}

		/// <summary>
		/// demeanor Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("demeanor").
				Set("name", data["name"],DataType.Varchar, 30).
				Set("saying", data["saying"],DataType.Varchar, 50).
				Set("note", data["note"],DataType.Text).
				Where("demeanor_id", data["demeanor_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.Demeanor_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("demeanor_id", this.Demeanor_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("saying", this.Saying);
			json.AddProperty("note", this.Note);
			return json.Result;
		}
	}
}