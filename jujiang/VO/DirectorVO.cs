﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// directorVO
    ///	</summary>
    [Serializable]
    public class DirectorVO
    {
        #region	Property

        private String  _Director_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Director_id
        {
            get{ return _Director_id; }
            set{ _Director_id = value; }
        }
		

        private String  _Name;
        /// <summary>
        /// 姓名
        ///</summary>
        public String  Name
        {
            get{ return _Name; }
            set{ _Name = value; }
        }
		

        private String  _Duty;
        /// <summary>
        /// 职务
        ///</summary>
        public String  Duty
        {
            get{ return _Duty; }
            set{ _Duty = value; }
        }
		

        private String  _Content;
        /// <summary>
        /// note
        ///</summary>
        public String  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }
		

        private Int32  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// director
        /// </summary>
        public DirectorVO(){}
	
        /// <summary>
        /// director
        /// </summary>
        /// <param name="director_id">Primary Key</param>
        public DirectorVO(string director_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("director").Where("director_id",director_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// director
        /// </summary>
        /// <param name="director_id">Primary Key</param>
        public DirectorVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Director_id = dataReader["director_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Duty = dataReader["duty"] as string;
				this.Content = dataReader["content"] as string;
				this.Index = Convert.ToInt32(dataReader["index"]);
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// director Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("director");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("director");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("director_id", data["director_id"],DataType.Char, 24).
            Value("name", data["name"],DataType.Varchar, 30).
            Value("duty", data["duty"],DataType.Varchar, 50).
            Value("content", data["content"],DataType.Text).
            Value("index", data["index"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// director Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("director").
            Set("name", data["name"],DataType.Varchar, 30).
            Set("duty", data["duty"],DataType.Varchar, 50).
            Set("content", data["content"],DataType.Text).
            Set("index", data["index"],DataType.Int).
            Where("director_id", data["director_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Director_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("director_id", this.Director_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("duty", this.Duty);
			json.AddProperty("content", this.Content);
			json.AddProperty("index", this.Index);
			return json.Result;
        }
    }
}