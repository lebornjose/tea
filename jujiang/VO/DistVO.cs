﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 8:50:30
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// distVO
    ///	</summary>
    [Serializable]
    public class DistVO
    {
        #region	Property

        private String  _Dist_id;
        /// <summary>
        /// 变好
        ///</summary>
        public String  Dist_id
        {
            get{ return _Dist_id; }
            set{ _Dist_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 标题
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Content;
        /// <summary>
        /// 内容
        ///</summary>
        public String  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// dist
        /// </summary>
        public DistVO(){}
	
        /// <summary>
        /// dist
        /// </summary>
        /// <param name="dist_id">Primary Key</param>
        public DistVO(string dist_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("dist").Where("dist_id",dist_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// dist
        /// </summary>
        /// <param name="dist_id">Primary Key</param>
        public DistVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Dist_id = dataReader["dist_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Content = dataReader["content"] as string;
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// dist Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("dist");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("dist");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("dist_id", data["dist_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 50).
            Value("content", data["content"],DataType.Varchar, 300).
            Excute();
        }

        /// <summary>
        /// dist Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("dist").
            Set("title", data["title"],DataType.Varchar, 50).
            Set("content", data["content"],DataType.Varchar, 300).
            Where("dist_id", data["dist_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Dist_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("dist_id", this.Dist_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("content", this.Content);
			return json.Result;
        }
    }
}