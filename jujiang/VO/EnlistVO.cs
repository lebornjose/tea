﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/8/29 18:03:42
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// enlistVO
	///	</summary>
	[Serializable]
	public class EnlistVO
	{
		#region	Property

		private String  _List_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  List_id
		{
			get{ return _List_id; }
			set{ _List_id = value; }
		}


		private String  _Realname;
		/// <summary>
		/// 姓名
		///</summary>
		public String  Realname
		{
			get{ return _Realname; }
			set{ _Realname = value; }
		}


		private String  _Sex;
		/// <summary>
		/// 性别
		///</summary>
		public String  Sex
		{
			get{ return _Sex; }
			set{ _Sex = value; }
		}


		private Int32  _Birthday;
		/// <summary>
		/// 出生日期
		///</summary>
		public Int32  Birthday
		{
			get{ return _Birthday; }
			set{ _Birthday = value; }
		}


		private Double  _Age;
		/// <summary>
		/// 年龄
		///</summary>
		public Double  Age
		{
			get{ return _Age; }
			set{ _Age = value; }
		}


		private String  _Address;
		/// <summary>
		/// 户口所在地
		///</summary>
		public String  Address
		{
			get{ return _Address; }
			set{ _Address = value; }
		}


		private String  _Other;
		/// <summary>
		/// 其他选项
		///</summary>
		public String  Other
		{
			get{ return _Other; }
			set{ _Other = value; }
		}
		public JabinfoKeyValue OtherKV{ get { return Jabinfo.Help.Basic.ToJKV(_Other); } }

		private String  _Edu;
		/// <summary>
		/// 教育背景
		///</summary>
		public String  Edu
		{
			get{ return _Edu; }
			set{ _Edu = value; }
		}


		private String  _Spec;
		/// <summary>
		/// 专业课程
		///</summary>
		public String  Spec
		{
			get{ return _Spec; }
			set{ _Spec = value; }
		}


		private String  _Ability;
		/// <summary>
		/// 个人能力
		///</summary>
		public String  Ability
		{
			get{ return _Ability; }
			set{ _Ability = value; }
		}


		private String  _Exper;
		/// <summary>
		/// 工作经历
		///</summary>
		public String  Exper
		{
			get{ return _Exper; }
			set{ _Exper = value; }
		}


		private String  _Language;
		/// <summary>
		/// 语言能力
		///</summary>
		public String  Language
		{
			get{ return _Language; }
			set{ _Language = value; }
		}


		private String  _Apprai;
		/// <summary>
		/// 自我评介
		///</summary>
		public String  Apprai
		{
			get{ return _Apprai; }
			set{ _Apprai = value; }
		}


		private String  _Reward;
		/// <summary>
		/// 证书与奖励
		///</summary>
		public String  Reward
		{
			get{ return _Reward; }
			set{ _Reward = value; }
		}


		private Int32  _Addtime;
		/// <summary>
		/// 报名时间
		///</summary>
		public Int32  Addtime
		{
			get{ return _Addtime; }
			set{ _Addtime = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// enlist
		/// </summary>
		public EnlistVO(){}

		/// <summary>
		/// enlist
		/// </summary>
		/// <param name="list_id">Primary Key</param>
		public EnlistVO(string list_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("enlist").Where("list_id",list_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// enlist
		/// </summary>
		/// <param name="list_id">Primary Key</param>
		public EnlistVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.List_id = dataReader["list_id"] as string;
				this.Realname = dataReader["realname"] as string;
				this.Sex = dataReader["sex"] as string;
				this.Birthday = Convert.ToInt32(dataReader["birthday"]);
				this.Age = Convert.ToDouble(dataReader["age"]);
				this.Address = dataReader["address"] as string;
				this.Other = dataReader["other"] as string;
				this.Edu = dataReader["edu"] as string;
				this.Spec = dataReader["spec"] as string;
				this.Ability = dataReader["ability"] as string;
				this.Exper = dataReader["exper"] as string;
				this.Language = dataReader["language"] as string;
				this.Apprai = dataReader["apprai"] as string;
				this.Reward = dataReader["reward"] as string;
				this.Addtime = Convert.ToInt32(dataReader["addtime"]);
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// enlist Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("enlist");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("enlist");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("list_id", data["list_id"],DataType.Char, 24).
				Value("realname", data["realname"],DataType.Varchar, 30).
				Value("sex", data["sex"],DataType.Char, 1).
				Value("birthday", data["birthday"],DataType.Int).
				Value("age", data["age"],DataType.Double).
				Value("address", data["address"],DataType.Varchar, 100).
				Value("other", data["other"],DataType.Varchar, 1000).
				Value("edu", data["edu"],DataType.Text).
				Value("spec", data["spec"],DataType.Text).
				Value("ability", data["ability"],DataType.Text).
				Value("exper", data["exper"],DataType.Text).
				Value("language", data["language"],DataType.Text).
				Value("apprai", data["apprai"],DataType.Text).
				Value("reward", data["reward"],DataType.Text).
				Value("addtime", data["addtime"],DataType.Int).
				Excute();
		}

		/// <summary>
		/// enlist Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("enlist").
				Set("realname", data["realname"],DataType.Varchar, 30).
				Set("sex", data["sex"],DataType.Char, 1).
				Set("birthday", data["birthday"],DataType.Int).
				Set("age", data["age"],DataType.Double).
				Set("address", data["address"],DataType.Varchar, 100).
				Set("other", data["other"],DataType.Varchar, 1000).
				Set("edu", data["edu"],DataType.Text).
				Set("spec", data["spec"],DataType.Text).
				Set("ability", data["ability"],DataType.Text).
				Set("exper", data["exper"],DataType.Text).
				Set("language", data["language"],DataType.Text).
				Set("apprai", data["apprai"],DataType.Text).
				Set("reward", data["reward"],DataType.Text).
				Set("addtime", data["addtime"],DataType.Int).
				Where("list_id", data["list_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.List_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("list_id", this.List_id);
			json.AddProperty("realname", this.Realname);
			json.AddProperty("sex", this.Sex);
			json.AddProperty("birthday", this.Birthday);
			json.AddProperty("age", this.Age.ToString());
			json.AddProperty("address", this.Address);
			json.AddObjectProperty("other", this.Other);
			json.AddProperty("edu", this.Edu);
			json.AddProperty("spec", this.Spec);
			json.AddProperty("ability", this.Ability);
			json.AddProperty("exper", this.Exper);
			json.AddProperty("language", this.Language);
			json.AddProperty("apprai", this.Apprai);
			json.AddProperty("reward", this.Reward);
			json.AddTimeProperty("addtime", this.Addtime);
			return json.Result;
		}
	}
}