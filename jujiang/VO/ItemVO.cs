﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// itemVO
    ///	</summary>
    [Serializable]
    public class ItemVO
    {
        #region	Property

        private String  _Item_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Item_id
        {
            get{ return _Item_id; }
            set{ _Item_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 标题
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Content;
        /// <summary>
        /// 内容
        ///</summary>
        public String  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }
		

        private Int32  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// item
        /// </summary>
        public ItemVO(){}
	
        /// <summary>
        /// item
        /// </summary>
        /// <param name="item_id">Primary Key</param>
        public ItemVO(string item_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("item").Where("item_id",item_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// item
        /// </summary>
        /// <param name="item_id">Primary Key</param>
        public ItemVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Item_id = dataReader["item_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Content = dataReader["content"] as string;
				this.Index = Convert.ToInt32(dataReader["index"]);
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// item Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("item");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("item");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("item_id", data["item_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 30).
            Value("content", data["content"],DataType.Text).
            Value("index", data["index"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// item Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("item").
            Set("title", data["title"],DataType.Varchar, 30).
            Set("content", data["content"],DataType.Text).
            Set("index", data["index"],DataType.Int).
            Where("item_id", data["item_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Item_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("item_id", this.Item_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("content", this.Content);
			json.AddProperty("index", this.Index);
			return json.Result;
        }
    }
}