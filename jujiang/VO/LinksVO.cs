﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// linksVO
    ///	</summary>
    [Serializable]
    public class LinksVO
    {
        #region	Property

        private String  _Links_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Links_id
        {
            get{ return _Links_id; }
            set{ _Links_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 标题
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Url;
        /// <summary>
        /// 链接地址
        ///</summary>
        public String  Url
        {
            get{ return _Url; }
            set{ _Url = value; }
        }
		

        private Int32  _Index;
        /// <summary>
        /// 排序
        ///</summary>
        public Int32  Index
        {
            get{ return _Index; }
            set{ _Index = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// links
        /// </summary>
        public LinksVO(){}
	
        /// <summary>
        /// links
        /// </summary>
        /// <param name="links_id">Primary Key</param>
        public LinksVO(string links_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("links").Where("links_id",links_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// links
        /// </summary>
        /// <param name="links_id">Primary Key</param>
        public LinksVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Links_id = dataReader["links_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Url = dataReader["url"] as string;
				this.Index = Convert.ToInt32(dataReader["index"]);
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// links Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("links");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("links");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("links_id", data["links_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 30).
            Value("url", data["url"],DataType.Varchar, 100).
            Value("index", data["index"],DataType.Int).
            Excute();
        }

        /// <summary>
        /// links Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("links").
            Set("title", data["title"],DataType.Varchar, 30).
            Set("url", data["url"],DataType.Varchar, 100).
            Set("index", data["index"],DataType.Int).
            Where("links_id", data["links_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Links_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("links_id", this.Links_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("url", this.Url);
			json.AddProperty("index", this.Index);
			return json.Result;
        }
    }
}