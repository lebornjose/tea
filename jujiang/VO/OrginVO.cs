﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/25 8:54:12
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// orginVO
    ///	</summary>
    [Serializable]
    public class OrginVO
    {
        #region	Property

        private String  _Orgin_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Orgin_id
        {
            get{ return _Orgin_id; }
            set{ _Orgin_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 名称
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Content;
        /// <summary>
        /// 简介
        ///</summary>
        public String  Content
        {
            get{ return _Content; }
            set{ _Content = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// orgin
        /// </summary>
        public OrginVO(){}
	
        /// <summary>
        /// orgin
        /// </summary>
        /// <param name="orgin_id">Primary Key</param>
        public OrginVO(string orgin_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("orgin").Where("orgin_id",orgin_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// orgin
        /// </summary>
        /// <param name="orgin_id">Primary Key</param>
        public OrginVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Orgin_id = dataReader["orgin_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Content = dataReader["content"] as string;
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// orgin Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("orgin");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("orgin");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("orgin_id", data["orgin_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 50).
            Value("content", data["content"],DataType.Varchar, 300).
            Excute();
        }

        /// <summary>
        /// orgin Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("orgin").
            Set("title", data["title"],DataType.Varchar, 50).
            Set("content", data["content"],DataType.Varchar, 300).
            Where("orgin_id", data["orgin_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Orgin_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("orgin_id", this.Orgin_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("content", this.Content);
			return json.Result;
        }
    }
}