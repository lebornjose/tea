﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 13:12:33
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// regVO
    ///	</summary>
    [Serializable]
    public class RegVO
    {
        #region	Property

        private String  _Reg_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Reg_id
        {
            get{ return _Reg_id; }
            set{ _Reg_id = value; }
        }
		

        private String  _Name;
        /// <summary>
        /// 姓名
        ///</summary>
        public String  Name
        {
            get{ return _Name; }
            set{ _Name = value; }
        }
		

        private String  _Unit;
        /// <summary>
        /// 职务
        ///</summary>
        public String  Unit
        {
            get{ return _Unit; }
            set{ _Unit = value; }
        }
		

        private String  _Mobile;
        /// <summary>
        /// 电话
        ///</summary>
        public String  Mobile
        {
            get{ return _Mobile; }
            set{ _Mobile = value; }
        }
		

        private String  _Email;
        /// <summary>
        /// 邮箱
        ///</summary>
        public String  Email
        {
            get{ return _Email; }
            set{ _Email = value; }
        }
		

        private Int32  _Date;
        /// <summary>
        /// 报名时间
        ///</summary>
        public Int32  Date
        {
            get{ return _Date; }
            set{ _Date = value; }
        }
		

        private String  _Actice_id;
        /// <summary>
        /// 活动编号
        ///</summary>
        public String  Actice_id
        {
            get{ return _Actice_id; }
            set{ _Actice_id = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// reg
        /// </summary>
        public RegVO(){}
	
        /// <summary>
        /// reg
        /// </summary>
        /// <param name="reg_id">Primary Key</param>
        public RegVO(string reg_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("reg").Where("reg_id",reg_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// reg
        /// </summary>
        /// <param name="reg_id">Primary Key</param>
        public RegVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Reg_id = dataReader["reg_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Unit = dataReader["unit"] as string;
				this.Mobile = dataReader["mobile"] as string;
				this.Email = dataReader["email"] as string;
				this.Date = Convert.ToInt32(dataReader["date"]);
				this.Actice_id = dataReader["actice_id"] as string;
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// reg Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("reg");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("reg");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("reg_id", data["reg_id"],DataType.Char, 24).
            Value("name", data["name"],DataType.Varchar, 30).
            Value("unit", data["unit"],DataType.Varchar, 50).
            Value("mobile", data["mobile"],DataType.Varchar, 20).
            Value("email", data["email"],DataType.Varchar, 20).
            Value("date", data["date"],DataType.Int).
            Value("actice_id", data["actice_id"],DataType.Char, 24).
            Excute();
        }

        /// <summary>
        /// reg Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("reg").
            Set("name", data["name"],DataType.Varchar, 30).
            Set("unit", data["unit"],DataType.Varchar, 50).
            Set("mobile", data["mobile"],DataType.Varchar, 20).
            Set("email", data["email"],DataType.Varchar, 20).
            Set("date", data["date"],DataType.Int).
            Set("actice_id", data["actice_id"],DataType.Char, 24).
            Where("reg_id", data["reg_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Reg_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("reg_id", this.Reg_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("unit", this.Unit);
			json.AddProperty("mobile", this.Mobile);
			json.AddProperty("email", this.Email);
			json.AddProperty("date", this.Date);
			json.AddIDProperty("actice_id", this.Actice_id);
			return json.Result;
        }
    }
}