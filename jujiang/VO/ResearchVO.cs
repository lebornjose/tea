﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/30 15:29:08
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// researchVO
	///	</summary>
	[Serializable]
	public class ResearchVO
	{
		#region	Property

		private String  _Research_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  Research_id
		{
			get{ return _Research_id; }
			set{ _Research_id = value; }
		}


		private String  _Title;
		/// <summary>
		/// 标题
		///</summary>
		public String  Title
		{
			get{ return _Title; }
			set{ _Title = value; }
		}


		private String  _Summary;
		/// <summary>
		/// 简介
		///</summary>
		public String  Summary
		{
			get{ return _Summary; }
			set{ _Summary = value; }
		}


		private String  _Content;
		/// <summary>
		/// 详情
		///</summary>
		public String  Content
		{
			get{ return _Content; }
			set{ _Content = value; }
		}


		private Int32  _Date;
		/// <summary>
		/// 发布时间
		///</summary>
		public Int32  Date
		{
			get{ return _Date; }
			set{ _Date = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// research
		/// </summary>
		public ResearchVO(){}

		/// <summary>
		/// research
		/// </summary>
		/// <param name="research_id">Primary Key</param>
		public ResearchVO(string research_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("research").Where("research_id",research_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// research
		/// </summary>
		/// <param name="research_id">Primary Key</param>
		public ResearchVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.Research_id = dataReader["research_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Summary = dataReader["summary"] as string;
				this.Content = dataReader["content"] as string;
				this.Date = Convert.ToInt32(dataReader["date"]);
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// research Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("research");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("research");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("research_id", data["research_id"],DataType.Char, 24).
				Value("title", data["title"],DataType.Varchar, 30).
				Value("summary", data["summary"],DataType.Varchar, 200).
				Value("content", data["content"],DataType.Text).
				Value("date", data["date"],DataType.Int).
				Excute();
		}

		/// <summary>
		/// research Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("research").
				Set("title", data["title"],DataType.Varchar, 30).
				Set("summary", data["summary"],DataType.Varchar, 200).
				Set("content", data["content"],DataType.Text).
				Set("date", data["date"],DataType.Int).
				Where("research_id", data["research_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.Research_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("research_id", this.Research_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("summary", this.Summary);
			json.AddProperty("content", this.Content);
			json.AddProperty("date", this.Date);
			return json.Result;
		}
	}
}