﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/8/7 11:00:47
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
	/// <summary>
	/// teacherVO
	///	</summary>
	[Serializable]
	public class TeacherVO
	{
		#region	Property

		private String  _Teacher_id;
		/// <summary>
		/// 编号
		///</summary>
		public String  Teacher_id
		{
			get{ return _Teacher_id; }
			set{ _Teacher_id = value; }
		}


		private String  _Name;
		/// <summary>
		/// 姓名
		///</summary>
		public String  Name
		{
			get{ return _Name; }
			set{ _Name = value; }
		}


		private String  _Position;
		/// <summary>
		/// 职务
		///</summary>
		public String  Position
		{
			get{ return _Position; }
			set{ _Position = value; }
		}


		private String  _Exper;
		/// <summary>
		/// 业务专长
		///</summary>
		public String  Exper
		{
			get{ return _Exper; }
			set{ _Exper = value; }
		}


		private String  _Team_id;
		/// <summary>
		/// 精英团队
		///</summary>
		public String  Team_id
		{
			get{ return _Team_id; }
			set{ _Team_id = value; }
		}


		private String  _Content;
		/// <summary>
		/// 详情
		///</summary>
		public String  Content
		{
			get{ return _Content; }
			set{ _Content = value; }
		}


		private String  _Type;
		/// <summary>
		/// 类型
		///</summary>
		public String  Type
		{
			get{ return _Type; }
			set{ _Type = value; }
		}


		private Int32  _Date;
		/// <summary>
		/// 添加时间
		///</summary>
		public Int32  Date
		{
			get{ return _Date; }
			set{ _Date = value; }
		}


		#endregion

		#region Constructor
		/// <summary>
		/// teacher
		/// </summary>
		public TeacherVO(){}

		/// <summary>
		/// teacher
		/// </summary>
		/// <param name="teacher_id">Primary Key</param>
		public TeacherVO(string teacher_id)
		{
			DataRow dataReader = null;
			try
			{
				dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("teacher").Where("teacher_id",teacher_id,DataType.Char, 24).Row();
			}
			catch (Exception e)
			{
				this.Alter();
				throw e;
			}
			if (dataReader != null)
				this._Init(dataReader);
		}

		/// <summary>
		/// teacher
		/// </summary>
		/// <param name="teacher_id">Primary Key</param>
		public TeacherVO(DataRow dataReader)
		{
			_Init(dataReader);
		}

		private void _Init(DataRow dataReader)
		{
			try
			{
				this.Teacher_id = dataReader["teacher_id"] as string;
				this.Name = dataReader["name"] as string;
				this.Position = dataReader["position"] as string;
				this.Exper = dataReader["exper"] as string;
				this.Team_id = dataReader["team_id"] as string;
				this.Content = dataReader["content"] as string;
				this.Type = dataReader["type"] as string;
				this.Date = Convert.ToInt32(dataReader["date"]);
			}
			catch (Exception e)
			{
				if (e.Source.ToString() == "System.Data")
				{
					this.Alter();
					return;
				}
				throw e;
			}
		}
		#endregion

		#region Insert,Update
		/// <summary>
		/// teacher Insert
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Insert(JabinfoKeyValue data)
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("teacher");
			int result = _Push(data, i);
			if (result == -2)
			{
				this.Alter();
			}
			return result;
		}

		public void Alter()
		{
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("teacher");
			i = i.Alter();
			_Push(new JabinfoKeyValue(), i);
		}

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
		{
			return Insert.
				Value("teacher_id", data["teacher_id"],DataType.Char, 24).
				Value("name", data["name"],DataType.Varchar, 30).
				Value("position", data["position"],DataType.Varchar, 50).
				Value("exper", data["exper"],DataType.Varchar, 80).
				Value("team_id", data["team_id"],DataType.Char, 24).
				Value("content", data["content"],DataType.Text).
				Value("type", data["type"],DataType.Char, 1).
				Value("date", data["date"],DataType.Int).
				Excute();
		}

		/// <summary>
		/// teacher Update
		/// </summary>
		/// <param name="data">JabinfoKeyValue</param>
		/// <returns></returns>
		public int Update(JabinfoKeyValue data)
		{
			return JabinfoSQL.Instance("jujiang").Update("teacher").
				Set("name", data["name"],DataType.Varchar, 30).
				Set("position", data["position"],DataType.Varchar, 50).
				Set("exper", data["exper"],DataType.Varchar, 80).
				Set("team_id", data["team_id"],DataType.Char, 24).
				Set("content", data["content"],DataType.Text).
				Set("type", data["type"],DataType.Char, 1).
				Set("date", data["date"],DataType.Int).
				Where("teacher_id", data["teacher_id"],DataType.Char, 24).
				Excute();
		}
		#endregion

		/// <summary>
		/// ToJson
		/// </summary>
		public string ToJson()
		{
			if (this.Teacher_id == null)
			{
				return "null";
			}
			JabinfoJson json = new JabinfoJson();

			json.AddIDProperty("teacher_id", this.Teacher_id);
			json.AddProperty("name", this.Name);
			json.AddProperty("position", this.Position);
			json.AddProperty("exper", this.Exper);
			json.AddIDProperty("team_id", this.Team_id);
			json.AddProperty("content", this.Content);
			json.AddProperty("type", this.Type);
			json.AddProperty("date", this.Date);
			return json.Result;
		}
	}
}