﻿/*
 * Created:  By Jabinfo.Team
 * Contact:  http://www.jabinfo.com
 * Author :  YunzhiSoft
 * Date   :  2014/7/24 17:09:50
 */
using System;
using System.Data;

namespace Jabinfo.Jujiang.VO
{
    /// <summary>
    /// teamVO
    ///	</summary>
    [Serializable]
    public class TeamVO
    {
        #region	Property

        private String  _Team_id;
        /// <summary>
        /// 编号
        ///</summary>
        public String  Team_id
        {
            get{ return _Team_id; }
            set{ _Team_id = value; }
        }
		

        private String  _Title;
        /// <summary>
        /// 名称
        ///</summary>
        public String  Title
        {
            get{ return _Title; }
            set{ _Title = value; }
        }
		

        private String  _Detail;
        /// <summary>
        /// 详情
        ///</summary>
        public String  Detail
        {
            get{ return _Detail; }
            set{ _Detail = value; }
        }
		

        #endregion

		#region Constructor
        /// <summary>
        /// team
        /// </summary>
        public TeamVO(){}
	
        /// <summary>
        /// team
        /// </summary>
        /// <param name="team_id">Primary Key</param>
        public TeamVO(string team_id)
        {
			DataRow dataReader = null;
            try
            {
                dataReader = JabinfoSQL.Instance("jujiang").Select("*").From("team").Where("team_id",team_id,DataType.Char, 24).Row();
            }
            catch (Exception e)
            {
                this.Alter();
				throw e;
            }
            if (dataReader != null)
                this._Init(dataReader);
        }

		/// <summary>
        /// team
        /// </summary>
        /// <param name="team_id">Primary Key</param>
        public TeamVO(DataRow dataReader)
        {
			_Init(dataReader);
        }

        private void _Init(DataRow dataReader)
        {
			try
            {
				this.Team_id = dataReader["team_id"] as string;
				this.Title = dataReader["title"] as string;
				this.Detail = dataReader["detail"] as string;
			}
            catch (Exception e)
            {
                if (e.Source.ToString() == "System.Data")
                {
                    this.Alter();
                    return;
                }
                throw e;
            }
        }
		#endregion

		#region Insert,Update
        /// <summary>
        /// team Insert
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Insert(JabinfoKeyValue data)
        {
			Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("team");
            int result = _Push(data, i);
            if (result == -2)
            {
                this.Alter();
            }
			return result;
        }

		public void Alter()
        {
            Sinbo.ICmdInsert i = JabinfoSQL.Instance("jujiang").Insert("team");
            i = i.Alter();
            _Push(new JabinfoKeyValue(), i);
        }

		private int _Push(JabinfoKeyValue data, Sinbo.ICmdInsert Insert)
        {
            return Insert.
            Value("team_id", data["team_id"],DataType.Char, 24).
            Value("title", data["title"],DataType.Varchar, 30).
            Value("detail", data["detail"],DataType.Text).
            Excute();
        }

        /// <summary>
        /// team Update
        /// </summary>
        /// <param name="data">JabinfoKeyValue</param>
        /// <returns></returns>
        public int Update(JabinfoKeyValue data)
        {
            return JabinfoSQL.Instance("jujiang").Update("team").
            Set("title", data["title"],DataType.Varchar, 30).
            Set("detail", data["detail"],DataType.Text).
            Where("team_id", data["team_id"],DataType.Char, 24).
            Excute();
        }
		#endregion

		/// <summary>
        /// ToJson
        /// </summary>
        public string ToJson()
        {
			if (this.Team_id == null)
            {
                return "null";
            }
            JabinfoJson json = new JabinfoJson();
			
			json.AddIDProperty("team_id", this.Team_id);
			json.AddProperty("title", this.Title);
			json.AddProperty("detail", this.Detail);
			return json.Result;
        }
    }
}